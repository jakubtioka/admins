# Administration

This project provide administration interface for Crafts service.

## Introduction

This project is based on Nuxt 3 framework combained with few other useful libraries as Vuetify and more. Nuxt's goal is to make web development intuitive and performant with a great developer experience in mind, learn more in the ["What is Nuxt?"](https://v3.nuxtjs.org/guide/concepts/introduction/) section of our documentation.

## Prerequisites

Navigate to the project directory and run the following command to start the development server:

```bash
cd admin/
```

The **API project** has to be running before you can use the administration interface.

Make sure to install the dependencies using the following command:

```bash
npm install
```

## Development

```bash
npm run dev
```

## GraphQL Types Generator

After that some changes were made in GraphQL queries and mutations regeneration of the GraphQL types is needed.

```bash
npm run generate:gql
```

If you are actively developing and changing the GraphQL queries, you can use the following command to generate the types continuously.

```bash
npm run generate:gql -- -watch
```

## Versioning

This project uses [npm-version](https://docs.npmjs.com/cli/v7/commands/npm-version) to manage the versioning.

```bash
npm version <major|minor|patch>
```

## Links and Tools

This project is based on Nuxt 3 framework combained with few other useful libraries as Vuetify and more.

- [Nuxt](https://nuxtjs.org) - The Hybrid Vue Framework
- [Vuetify](https://next.vuetifyjs.com/en/) is a Vue UI Library with beautifully handcrafted Material Components
- [urql](https://formidable.com/open-source/urql/docs/) is a highly customizable and versatile GraphQL client with which you add on features like normalized caching as you grow
- [GraphQL Code Generator](https://www.graphql-code-generator.com/docs/guides/vue) is a plugin-based tool that helps you get the best out of your GraphQL stack
