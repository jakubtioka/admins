import {
	ref,
	getStorage,
	getMetadata,
	uploadBytes,
	list,
	getDownloadURL,
	StorageReference,
	deleteObject
} from 'firebase/storage'

export const useFirebaseStorage = () => {
	const normalizePath = (path: string) => path
		.split(' ')
		.join('-')
		.normalize('NFD')
		.replace(/[\u0300-\u036f]/g, '')
		.replace(/[^\w.]|_/g, '')
		.toLowerCase()

	const filePrefix = () => {
		const now = new Date()
		return now.toISOString().slice(0, 10) + '-' + now.toISOString().slice(11, 23).replace(/[^\w]|_/g, '')
	}

	const getPath = (path: string, folder?: string) => [
		folder,
		`${filePrefix()}-${normalizePath(path)}`
	].join('/')

	const getStorageRef = (s: string | StorageReference) => typeof (s) === 'string' ? ref(getStorage(), s) : s

	return {
		// getFileRef: getStorageRef,

		getFileMetadata: (path: string | StorageReference) => getMetadata(getStorageRef(path)),

		getFileUrl: (path: string | StorageReference) => getDownloadURL(getStorageRef(path)),

		listFiles: (path: string | StorageReference) => list(getStorageRef(path)),

		uploadFile: (file: File, folder?: string) => uploadBytes(getStorageRef(getPath(file.name, folder)), file),

		deleteFile: (path: string | StorageReference) => deleteObject(getStorageRef(path)),
	}
}
