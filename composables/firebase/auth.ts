//https://firebase.google.com/docs/auth/web/start
import {
	User,
	getAuth,
	createUserWithEmailAndPassword,
	signInWithEmailAndPassword,
	onAuthStateChanged,
	sendPasswordResetEmail,
	signOut,
	updateProfile,
	updateEmail,
} from 'firebase/auth'

export const useFirebaseAuth = () => {
	const loggedUser = useState<User | null | undefined>('firebaseUser', () => undefined)

	const updateUser = (user: User | null) => loggedUser.value = user

	const initUser = () => onAuthStateChanged(getAuth(), (user) => updateUser(user))

	const registerUser = (email: string, password: string) => createUserWithEmailAndPassword(
		getAuth(),
		email,
		password
	).then(({ user }) => updateUser(user))

	const loginUser = (email: string, password: string) => signInWithEmailAndPassword(
		getAuth(),
		email,
		password
	).then(({ user }) => updateUser(user))

	const logoutUser = () => signOut(getAuth()).then(() => updateUser(null))

	const resetUserPassword = (email: string) => sendPasswordResetEmail(getAuth(), email)

	const updateUserProfile = (data: { displayName?: string | null, photoURL?: string | null }) =>
		loggedUser.value ? updateProfile(loggedUser.value, data) : Promise.reject()

	const updateUserEmail = (email: string) =>
		loggedUser.value ? updateEmail(loggedUser.value, email) : Promise.reject()

	return {
		initUser,
		loggedUser,
		loginUser,
		logoutUser,
		registerUser,
		resetUserPassword,
		updateUserProfile,
		updateUserEmail,
	}
}
