export const useLayout = () => {
	const showSideNav = useState('showNav', () => true)
	const expandSideNav = useState('showNavMobile', () => true)

	return {
		showSideNav,
		expandSideNav,
	}
}
