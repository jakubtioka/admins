import { Installation_Bool_Exp, Installation_Order_By, useInstallationsQuery } from '@gql/generated'

export const useInstallations = () => {
	const page = ref<number>(1)
	const limit = ref<number>(10)
	const orderBy = ref<Installation_Order_By>({})
	const where = ref<Installation_Bool_Exp>({})

	const variables = computed(() => ({
		where: where.value,
		orderBy: orderBy.value,
		limit: limit.value,
		offset: (page.value - 1) * limit.value,
	}))

	const query = useInstallationsQuery({ variables })
	const { data, error, fetching } = query
	const items = computed(() => data.value?.installations || [])
	const count = computed(() => data.value?.installationsAggregate.aggregate?.count || 0)

	return {
		page,
		limit,
		orderBy,
		where,
		error,
		fetching,
		items,
		count,
	}
}
