import { Message } from '@interfaces/global'

export const useMessages = () => {
	const messages = useState<Array<Message>>('messages', () => [])


	const message  = (text: string ) => {
		messages.value.push({ text })
	}
	
	return { messages, message }
}
