import { useQuery } from '@urql/vue'
import { GetEshopSelectDocument, GetEshopSelectQuery } from '@gql/generated'

// TODO add param for filtering users by role
export const useEntityEshopSelect = () => {
	const selectQuery = useQuery<GetEshopSelectQuery>({ query: GetEshopSelectDocument })

	return {
		selectQuery,
	}
}
