import { Order_By } from '@gql/generated'
import merge from 'lodash/merge'
import set from 'lodash/set'
import { Column } from '../abstract'

type ColumnField = 'id' | '' | 'firstName' | 'lastName' | 'email' | 'phone' | 'address'
  | 'companyName' | 'roleValue' | 'isActive' | 'in' | 'tin' | 'userServiceTypes'

export const useEntityUserState = (namespace: string = '') => {
	const statePrefix = 'users'
	const getStateKey = (key: string) => [namespace, statePrefix, key].join('-')

	// TODO: Init from cookie
  interface User { address: string; email: string; firstName: string; 
	lastName: string; firebaseId?: string; admin: string;}
  const initialColumns: Array<Column<ColumnField>> = [
  	{
  		field: 'id',
  		title: 'Id',
  		hidden: true,
  		key: true,
  	},
	  {
  		field: '',
  		title: '',
		  transform: () => '',
		  icon: ({ firebaseId }: User) => (getUserIcon(firebaseId))
  	},
  	{
  		field: '',
  		title: 'Jméno',
  		transform: ({ firstName, lastName }: User) => `${firstName} ${lastName}`,
  	},
  	{
  		field: 'companyName',
  		title: 'Společnost',
  		sort: null,
  	},
  	{
  		field: 'phone',
  		title: 'Telefon',
  		sort: null,
  	},
  	{
  		field: 'email',
  		title: 'Email',
  		sort: null,
  	},
	  {
  		field: 'userServiceTypes',
  		title: 'Specializace',
  		transform: (userServiceTypes) => userServiceTypes.map(item => item.serviceType.value).join(', ')
  	},
  	{
  		field: 'in',
  		title: 'IČ',
  		sort: null,
  	},
  	{
  		field: 'tin',
  		title: 'DIČ',
  		sort: null,
  	},
  	{
  		field: 'isActive',
  		title: 'Aktivní',
  		transform: (value: boolean) => value ? 'Aktivní' : 'Neaktivní',
  		sort: Order_By.Desc,
  	},
  	{
  		field: 'firstName',
  		title: 'Jméno',
  		virtual: true,
  		sort: null,
  	},
  	{
  		field: 'lastName',
  		title: 'Příjmení',
  		virtual: true,
  		sort: Order_By.Asc,
  	},
  ]
  const columns = useState(getStateKey('columns'), () => initialColumns)
  const orderBy = computed(() => merge({}, ...columns.value.map(i => set({}, i.field, i.sort))))

  const getUserIcon = (firebaseId? : string) => {
  	return firebaseId ? 'mdi-account-cog' : 'mdi-account-wrench-outline'
  }

  // TODO: Init from cookie
  const selected = useState<Array<string>>(getStateKey('selected'), () => [])

  // TODO: Init from cookie
  const page = useState<number>(getStateKey('page'), () => 1)

  return {
  	columns,
  	selected,
  	page,
  	orderBy,
  }
}
