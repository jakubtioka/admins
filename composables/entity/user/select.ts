import { useQuery } from '@urql/vue'
import { 
	GetUserSelectDocument, GetUserSelectQuery, 
	GetUserCompanySelectDocument, GetUserCompanySelectQuery 
} from '@gql/generated'

export const useEntityUserSelect = () => {
	const selectQuery = useQuery<GetUserSelectQuery>({ query: GetUserSelectDocument })

	return {
		selectQuery,
	}
}

export const useEntityUserCompanySelect = () => {
	const selectCompanyQuery = useQuery<GetUserCompanySelectQuery>({ query: GetUserCompanySelectDocument })

	return {
		selectCompanyQuery
	}
}
