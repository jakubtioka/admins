import { User_Bool_Exp, User_Order_By, useUsersQuery } from '@gql/generated'

export const useEntityUserList = () => {
	const page = ref<number>(1)
	const limit = ref<number>(10)
	const orderBy = ref<User_Order_By>({})
	const where = ref<User_Bool_Exp>({})

	const variables = computed(() => ({
		where: where.value,
		orderBy: orderBy.value,
		limit: limit.value,
		offset: (page.value - 1) * limit.value,
	}))

	const query = useUsersQuery({ variables })
	const { data, error, fetching } = query
	const items = computed(() => data.value?.users || [])
	const count = computed(() => data.value?.usersAgregate.aggregate?.count || 0)

	return {
		page,
		limit,
		orderBy,
		where,
		error,
		fetching,
		items,
		count,
		// changeSorting: (sortBy: string, direction: Order_By) => {
		// 	orderBy.value[sortBy] = direction
		// },
		// filterList: (value) => {
		// 	where.value.lastName = value?.lastName? { _ilike:`%${value.lastName}%`} : undefined
		// 	where.value.email = value?.email? { _ilike:`%${value.email}%`} : undefined
		// 	where.value.address = value?.address? { _ilike:`%${value.address}%`} : undefined
		// 	where.value.isActive = value?.isActive !== undefined? { _eq: value.isActive} : undefined
		// 	where.value.roleValue = value.roleValue?.length? { _in: value.roleValue} : undefined
		// },
	}
}

