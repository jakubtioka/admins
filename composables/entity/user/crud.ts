import {
	useCreateUserMutation,
	useDeleteUserMutation,
	useUpdateUserMutation,
	CreateUserMutationVariables,
	DeleteUserMutationVariables,
	UpdateUserMutationVariables,
	UserFragment,
	UpdateUserByPkMutationVariables,
	useUpdateUserByPkMutation,
} from '@gql/generated'


export const useEntityUserCRUD = () => {
	const translates = useTranslates()
	const { message } = useMessages()
	const error = ref()
	const updateMutation = useUpdateUserMutation()
	const createMutation = useCreateUserMutation()
	const deleteMutation = useDeleteUserMutation()
	const updateByPKMutation = useUpdateUserByPkMutation()

	return {
		error,
		updateItem: async (input: UserFragment) => {
			error.value = undefined

			const { id, role, userServiceTypes, availabilities, userZipCodes, ...rest } = input
			const userInput = rest
			const userServiceTypesInput = userServiceTypes.map(({ serviceTypeValue, userId }) => ({
				serviceTypeValue, userId
			})) ?? []


			if (input.id) {
				const variables: UpdateUserMutationVariables = {
					id,
					userInput,
					userServiceTypesInput
				}

				updateMutation.executeMutation(variables).then((response) => {
					if (response.error) {
						error.value = response.error
						message(translates.user.messageCreateFailed)
						return
					}

					message(translates.user.messageUpdateSuccess)

				}).catch((error) => {
					error.value = error

					message(translates.global.messageConnectionFailed)
				})
			} else {
				const variables: CreateUserMutationVariables = {
					userInput: {
						...userInput,
						userServiceTypes: {
							data: userServiceTypesInput
						}
					}
				}

				await createMutation.executeMutation(variables).then((response) => {
					if (response.error) {
						error.value = response.error
						message(translates.user.messageCreateFailed)
						return
					}

					message(translates.user.messageCreateSuccess)
				}).catch((error) => {
					error.value = error

					message(translates.global.messageConnectionFailed)
				})
			}
		},
		deleteItem: async (id: string) => {
			const variables: DeleteUserMutationVariables = {
				id
			}

			await deleteMutation.executeMutation(variables).then((response) => {
				if (response.error) {
					error.value = response.error
					message(translates.user.messageDeleteFailed)
					return
				}

				message(translates.user.messageDeleteSuccess)
			}).catch((error) => {
				error.value = error

				message(translates.global.messageConnectionFailed)
			})
		},
		updateUserByPK: async (input: UserFragment) => {
			error.value = undefined

			const { id, role, userServiceTypes, availabilities, firebaseId, ...rest } = input
			const userInput = { firebaseId }


			if (input.id) {
				const variables: UpdateUserByPkMutationVariables = {
					id,
					userInput
				}

				updateByPKMutation.executeMutation(variables).then((response) => {
					if (response.error) {
						error.value = response.error
						message(translates.user.messageAdminUpdateFailed)
						return
					}

					message(translates.user.messageAdminUpdateSuccess)

				}).catch((error) => {
					error.value = error

					message(translates.global.messageConnectionFailed)
				})
			}
		}
	}
}
