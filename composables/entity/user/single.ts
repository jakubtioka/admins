import { Ref } from 'vue'
import { useQuery } from '@urql/vue'
import { GetUserDocument, GetUserQuery, UserFragment, Role_Enum } from '@gql/generated'

export const useEntityUserSingle = (id: Ref<string | undefined>) => {
	const itemQuery = useQuery<GetUserQuery>({
		query: GetUserDocument,
		variables: ref({ id }),
		pause: computed(() => !id.value),
	})
	const itemDefaults = {
		id: undefined,
		isActive: true,
		firstName: '',
		lastName: '',
		login: '',
		email: '',
		roleValue: Role_Enum.Serviceman,
		userServiceTypes: [],
		// TODO
		role: { value: Role_Enum.Serviceman, description: '' },
		availabilities: [],
		userZipCodes: [],
	} as UserFragment

	return {
		itemDefaults,
		itemQuery,
	}
}
