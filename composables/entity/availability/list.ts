import dayjs from 'dayjs'
import {
	GetInstallationAvailabilityQueryVariables,
	GetServiceTypeQueryVariables,
	useGetInstallationAvailabilityQuery,
	useGetServiceTypeQuery,
} from '@gql/generated'

export const useAvailability = (autoFetch: boolean = false) => {
	const shouldFetch = ref<boolean>(autoFetch)
	const from = ref(dayjs())
	const to = ref(dayjs())
	const eshopId = ref<string>('')
	const eshopCategoryIds = ref<Array<string>>([])
	const installationId = ref<string>()
	const zipCode = ref<string | undefined>()
	const userIds = ref<Array<string>>([])

	const serviceTypeVariables = computed((): GetServiceTypeQueryVariables => {
		return {
			where: {
				servicedCategories: {
					_and: [
						{ ...(eshopId.value && { eshopId: {  _eq: eshopId } }) },
						{ ...( eshopCategoryIds.value.length && { eshopCategoryId: { _in: eshopCategoryIds.value } }) },
					]
				}
			}
		}
	})
	
	const serviceTypeQuery = useGetServiceTypeQuery({
		variables: serviceTypeVariables,
		pause: computed(() => !shouldFetch.value),
	})

	const serviceTypesValues = computed((): Array<string> => {
		return serviceTypeQuery.data.value?.serviceType.map((serviceType) => serviceType.value) || []
	})
	
	const availabilityVariables = computed((): GetInstallationAvailabilityQueryVariables => {
		return {
			where: {
				...(from.value && { availableFrom: { _gte: from.value } }),
				...(to.value && { availableTo: { _lte: to.value } }),
				...(
					zipCode.value
					&& { availabilityZipCodes: { zipCodeId: { _eq: zipCode.value } } }
				),
				...(
					serviceTypesValues.value.length
					&& { user: { userServiceTypes: { serviceType: { value: { _in: serviceTypesValues.value } } } } }
				),
				...(
					installationId.value && {
						_or: [
							{ installationId: { _is_null: true } },
							{ installationId: { _eq: installationId.value } }
						]
					}
				),
				...(
					userIds.value.length > 0 ? { userId: { _in: userIds.value } } : { userId: {} }
				)
			}
		}
	})

	const availabilityQuery = useGetInstallationAvailabilityQuery({
		variables: availabilityVariables,
		pause: computed(() => !shouldFetch.value),
	})

	return {
		from,
		to,
		zipCode,
		installationId,
		eshopId,
		eshopCategoryIds,
		shouldFetch,
		availabilityQuery,
		userIds
	}
}
