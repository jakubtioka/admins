import {
	Availability_Insert_Input,
	useCreateAvailabilityMutation,
	useUpdateAvailabilityMutation,
} from '@gql/generated'

export const useEntityAvailabilityCRUD = () => {
	const translates = useTranslates()
	const { message } = useMessages()
	const updateMutation = useUpdateAvailabilityMutation()
	const createMutation = useCreateAvailabilityMutation()
	const error = ref()

	return {
		error,
		updateAvailability: async (installId, availabilityIds) => {
			error.value = undefined

			if (availabilityIds) {
				const variables = { installId, availabilityIds }
				await updateMutation.executeMutation(variables).then((response) => {
					if (response.error) {
						error.value = response.error
						message(translates.installation.messageUpdateFailed)
						return
					}
					message(translates.installation.messageUpdateSuccess)
				}).catch((error) => {
					error.value = error
					message(translates.global.messageConnectionFailed)
				})
			} 
		},
		insertAvailability: async (availabilityInput: Array<Availability_Insert_Input>) => {
			error.value = undefined
			
			if (availabilityInput.length > 0) {
				const variables = { availabilityInput }

				await createMutation.executeMutation(variables).then((response) => {
					if (response.error) {
						error.value = response.error
						message(translates.availability.availabilityCreateFailed)
						return
					}
					message(translates.availability.availabilityCreateSuccess)
				}).catch((error) => {
					error.value = error
					message(translates.global.messageConnectionFailed)
				})
			} 
		}
	}
}
