
import { useQuery } from '@urql/vue'
import { GetRoleSelectDocument, GetRoleSelectQuery } from '@gql/generated'

export const useEntityRoleSelect = () => {
	const selectQuery = useQuery<GetRoleSelectQuery>({ query: GetRoleSelectDocument })

	return {
		selectQuery,
	}
}
