import { Order_By } from '@gql/generated'

export enum ActionName { Create, Delete, Edit, Duplicate }
export interface Action {
	name: ActionName,
	title: string
	icon: string
}

export interface ColumnOptions {
	title: string
	order?: number
	key?: boolean
	hidden?: boolean
	virtual?: boolean
	transform?(input: any): any
	sort?: SortableValue
	icon?(input: any): any
}
export type SortableValue = Order_By | null
export type Column<T = string> = { field: T } & ColumnOptions
