import { GetInstallationStateSelectDocument, GetInstallationStateSelectQuery } from '@gql/generated'
import { useQuery } from '@urql/vue'


export const useEntityInstallationStateSelect = () => {
	const selectQuery = useQuery<GetInstallationStateSelectQuery>({ query: GetInstallationStateSelectDocument })

	return {
		selectQuery,
	}
}
