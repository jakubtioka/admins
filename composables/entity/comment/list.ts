import { useSubscribeCommentSubscription } from '@gql/generated'

export const useCommentList = () => {
	const installationId = ref<string | undefined>()
	const commentVariables = { installationId }

	const commentQuery = useSubscribeCommentSubscription({
		variables: commentVariables
	})

	return {
		installationId,
		commentQuery,
	}
}
