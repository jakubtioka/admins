import {
	CommentFragment,
	CreateCommentMutationVariables,
	UpdateCommentMutationVariables,
	useCreateCommentMutation,
	useUpdateCommentMutation,
} from '@gql/generated'


export const useEntityCommentCRUD = () => {
	const translates = useTranslates()
	const { message } = useMessages()
	const error = ref()
	const updateMutation = useUpdateCommentMutation()
	const createMutation = useCreateCommentMutation()

	return {
		error,
		updateCommentItem: async (input: CommentFragment, installationId:string) => {
			error.value = undefined

			const { id, user, ...rest } = input
			const commentInput = rest

			if (input.id) {
				const { id } = input
				const variables: UpdateCommentMutationVariables = { id, commentInput: input }

				await updateMutation.executeMutation(variables).then((response) => {
					if (response.error) {
						error.value = response.error
						message(translates.comment.commentUpdateFailed)
						return
					}

					message(translates.comment.commentUpdateSuccess)
				}).catch((error) => {
					error.value = error

					message(translates.global.messageConnectionFailed)
				})
			} else {	
				const variables: CreateCommentMutationVariables = { 
					commentInput: {
						...commentInput,
						commentEntities: {
							data: [{ installationId }]
						}
					}
				}

				await createMutation.executeMutation(variables).then((response) => {
					if (response.error) {
						error.value = response.error
						message(translates.comment.commentCreateFailed)
						return
					}

					message(translates.comment.commentCreateSuccess)
				}).catch((error) => {
					error.value = error

					message(translates.global.messageConnectionFailed)
				})
			}
		},
	}
}
