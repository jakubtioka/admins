import { useGetAllZipCodesQuery, useGetAssignedZipCodesQuery } from '@gql/generated'

export const useZipCodesList = () => {
	const userId = ref<string | undefined>()
	const allZipCodesQuery = useGetAllZipCodesQuery()

	const assignedZipCodeListVariables = ref({ userId })

	const assignedZipCodesQuery = useGetAssignedZipCodesQuery({
		variables: assignedZipCodeListVariables,
		// pause: computed(() => !shouldFetch.value),
	})

	return {
		userId,
		allZipCodesQuery,
		assignedZipCodesQuery,
	}
}
