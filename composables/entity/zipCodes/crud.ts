import {
	useCreateUserZipCodesMutation, 
	UserZipCodes_Insert_Input
} from '@gql/generated'

export const useEntityUserZipCodeCRUD = () => {
	const translates = useTranslates()
	const { message } = useMessages()
	const createMutation = useCreateUserZipCodesMutation()
	const error = ref()

	return {
		error,
		// eslint-disable-next-line max-len
		updateUserZipCode: async (userId: string, zipCodeInput: UserZipCodes_Insert_Input | Array<UserZipCodes_Insert_Input>) => {
			error.value = undefined
			if (userId) {
				const variables = { userId, zipCodeInput }

				await createMutation.executeMutation(variables).then((response) => {
					if (response.error) {
						error.value = response.error
						message(translates.user.messageUpdateFailed)
						return
					}
					message(translates.user.messageUpdateSuccess)
				}).catch((error) => {
					error.value = error
					message(translates.global.messageConnectionFailed)
				})
			} 
		}
	}
}
