import { Ref } from 'vue'
import { useQuery } from '@urql/vue'
import {
	GetInstallationDocument,
	GetInstallationQuery,
	InstallationFragment,
	InstallationState_Enum
} from '@gql/generated'

export const useEntityInstallationSingle = (id: Ref<string | undefined>) => {
	const itemQuery = useQuery<GetInstallationQuery>({
		query: GetInstallationDocument,
		variables: ref({ id }),
		pause: computed(() => !id.value),
	})
	const itemDefaults = {
		availabilities: [],
		id: undefined,
		orderId: '',
		eshop: {
			name: '',
			id: '',
		},
		eshopId: undefined,
		deliveries: [],
		items: [],
		date: new Date(),
		customer: {
			firstName: '',
			lastName: '',
			email: '',
			phone: ''
		},
		shippingAddress: {
			street: '',
			city: '',
			zipCode: '',
			state: ''
		},
		selectedDateTime: {
			from: '',
			to: '',
		},
		createdAt: undefined,
		updatedAt: undefined,
		stateValue: InstallationState_Enum.New,
		installationComments: []
	}  as InstallationFragment

	return {
		itemDefaults,
		itemQuery,
	}
}
