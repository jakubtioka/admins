import {
	InstallationFragment,
	DeleteInstallationMutationVariables,
	UpdateInstallationMutationVariables,
	useDeleteInstallationMutation,
	useUpdateInstallationMutation,
	useCreateInstallationMutation,
	CreateInstallationMutationVariables,
	Installation_Insert_Input,
} from '@gql/generated'


export const useEntityInstallationCRUD = () => {
	const translates = useTranslates()
	const { message } = useMessages()
	const error = ref()
	const updateMutation = useUpdateInstallationMutation()
	const createMutation = useCreateInstallationMutation()
	const deleteMutation = useDeleteInstallationMutation()

	return {
		error,
		updateItem: async (input: InstallationFragment) => {
			error.value = undefined
			
			if (input.id) {
				const { id, availabilities, installationComments, eshop, ...rest } = input
				const variables: UpdateInstallationMutationVariables = { id, installationInput: rest }

				await updateMutation.executeMutation(variables).then((response) => {
					if (response.error) {
						error.value = response.error
						message(translates.installation.messageUpdateFailed)
						return
					}

					message(translates.installation.messageUpdateSuccess)
				}).catch((error) => {
					error.value = error

					message(translates.global.messageConnectionFailed)
				})
			} else {
				const { id, availabilities, installationComments, eshop, ...rest } = input
				const installationInput: Installation_Insert_Input = {
					...rest,
				}
				const variables: CreateInstallationMutationVariables = { installationInput }

				await createMutation.executeMutation(variables).then((response) => {
					if (response.error) {
						error.value = response.error
						message(translates.installation.messageCreateFailed)
						return
					}

					message(translates.installation.messageCreateSuccess)
				}).catch((error) => {
					error.value = error

					message(translates.global.messageConnectionFailed)
				})
			}
		},
		deleteItem: async (id: string) => {
			const variables: DeleteInstallationMutationVariables = {
				id
			}

			await deleteMutation.executeMutation(variables).then((response) => {
				if (response.error) {
					error.value = response.error
					message(translates.installation.messageDeleteFailed)
					return
				}

				message(translates.installation.messageDeleteSuccess)
			}).catch((error) => {
				error.value = error

				message(translates.global.messageConnectionFailed)
			})
		},
	}
}
