import { useQuery } from '@urql/vue'
import {
	FilterInstallationsDocument,
	FilterInstallationsQuery,
	Installation_Bool_Exp,
	Installation_Order_By,
	Order_By,
} from '@gql/generated'

export const useEntityInstallationList = () => {
	const page = ref<number>(1)
	const limit = ref<number>(10)
	const itemsCount = ref(1)
	const offset = computed(() => (page.value - 1) * limit.value)
	const orderBy = ref<Installation_Order_By>({})
	const where = ref<Installation_Bool_Exp>({})

	const filterVariables = ref({
		orderBy,
		where,
		limit, 
		offset
	})

	const filtrationQuery = useQuery<FilterInstallationsQuery>({
		query: FilterInstallationsDocument,
		variables: filterVariables,
	})
	filtrationQuery.then(
		({ data }) =>
			(itemsCount.value = data.value?.installation_aggregate.aggregate?.count || 1)
	)
	return {
		page,
		limit,
		itemsCount,
		orderBy,
		changeSorting: (sortBy: string, direction: Order_By) => {
			orderBy.value[sortBy] = direction
		},
		filterList: (value) => {
			where.value.eshopId = value.eshopId?.length
				? { _in: value.eshopId }
				: undefined
			where.value.customer = value.customer
				? { _cast: { String: { _ilike: `%${value.customer}%` } } }
				: undefined
			where.value.shippingAddress = value.address
				? { _cast: { String: { _ilike: `%${value.address}%` } } }
				: undefined
		},
		filtrationQuery,
	}
}
