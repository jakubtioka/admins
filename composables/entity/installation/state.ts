import merge from 'lodash/merge'
import set from 'lodash/set'
import { InstallationState_Enum, Order_By } from '@gql/generated'
import { Column } from '../abstract'
import dayjs from 'dayjs'
import cs from 'dayjs/locale/cs'
dayjs.locale(cs)

const { format } = useTranslates()

type ColumnField = 'id' | 'orderId' | 'pinCode' | 'customer' | 'createdAt' | 'updatedAt'
	| 'installationState' | 'eshop.name'

export const useEntityInstallationState = (namespace: string = '') => {
	const statePrefix = 'installations'
	const getStateKey = (key: string) => [namespace, statePrefix, key].join('-')

	// TODO: Init from cookie
	interface Customer { firstName: string, lastName: string, email: string, phone: string }
	interface State { title: string, value: string }
	const transformDateString = (string: string) => string ? dayjs(string).format(format.dateWithTime) : ''
	const initialColumns: Array<Column<ColumnField>> = [
		{
			field: 'id',
			title: 'Id instalace',
			hidden: true,
			key: true,
		},
		{
			field: 'orderId',
			title: 'Číslo objednávky',
			order: 2,
			sort: null,
		},
		{
			field: 'eshop.name',
			title: 'e-Shop',
			sort: null,
		},
		{
			field: 'customer',
			title: 'Zákazník',
			transform: ({ firstName, lastName }: Customer) => `${firstName} ${lastName}`,
			sort: null,
		},
		{
			field: 'createdAt',
			title: 'Vytvořeno',
			transform: transformDateString,
			sort: Order_By.Desc,
		},
		{
			field: 'pinCode',
			title: 'PIN',
			transform: (pin: string) => pin.replace(/(.{1}).*(.{1})/, '$1' + '#'.repeat(pin.length - 2) + '$2'),
		},
		{
			field: 'updatedAt',
			title: 'Upraveno',
			hidden: true,
			transform: transformDateString,
		},
		{
			field: 'installationState',
			title: 'Stav',
			order: 1,
			transform: ({ title }: State) => title,
			icon: ({ value }: State) => getIconByState(value)
		},
	]
	const columns = useState(getStateKey('columns'), () => initialColumns)
	const orderBy = computed(() => merge({}, ...columns.value.map(i => set({}, i.field, i.sort))))


	const getIconByState = (state) => {
		const iconMapper = {
			[InstallationState_Enum.New]: 'mdi-new-box',
			[InstallationState_Enum.Open]: 'mdi-flag-outline',
			[InstallationState_Enum.Assigned]: 'mdi-account-hard-hat',
			[InstallationState_Enum.InProgress]: 'mdi-progress-wrench',
			[InstallationState_Enum.InReview]: 'mdi-eye',
			[InstallationState_Enum.Approved]: 'mdi-check-bold',
			[InstallationState_Enum.Rejected]: 'mdi-close-circle',
			[InstallationState_Enum.Cancelled]: 'mdi-cancel',
			[InstallationState_Enum.Reopen]:'mdi-repeat',
			[InstallationState_Enum.Closed]: 'mdi-lock',
			[InstallationState_Enum.Done]: 'mdi-check-decagram',
		}
		return iconMapper[state] || 'mdi-help-circle'
	}
	
	// TODO: Init from cookie
	const selected = useState<Array<string>>(getStateKey('selected'), () => [])

	// TODO: Init from cookie
	const page = useState<number>(getStateKey('page'), () => 1)

	// TODO: const filtering = useState(`${statePrefix}-filtering`, () => [])

	return {
		columns,
		selected,
		page,
		orderBy,
	}
}
