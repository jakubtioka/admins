
import { useQuery } from '@urql/vue'
import { GetServiceTypeSelectDocument, GetServiceTypeSelectQuery } from '@gql/generated'

export const useEntityServiceTypeSelect = () => {
	const selectQuery = useQuery<GetServiceTypeSelectQuery>({
		query: GetServiceTypeSelectDocument
	})

	return {
		selectQuery,
	}
}
