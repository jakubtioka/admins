import { version } from '../package.json'

export const useUtils = () => {
	return {
		version,
	}
}

// TODO: useString { normalizeZipCode }
export const normalizeZipCode = (zipcode: string) => {
	return zipcode.trim().split(' ').join('')
}
