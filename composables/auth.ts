export const useAuth = () => {
	const {
		loggedUser,
		loginUser: firebaseLoginUser,
		logoutUser: firebaseLogoutUser,
		registerUser: firebaseRegisterUser, // TODO: Implement
		resetUserPassword: firebaseResetUserPassword,
		updateUserProfile: firebaseUpdateUserProfile,
		updateUserEmail: firebaseUpdateUserEmail,
	} = useFirebaseAuth()
	const { message } = useMessages()

	const loginUser = (email: string, password: string) => {
		firebaseLoginUser(email, password)
			.then(() => {
				if (loggedUser.value) {
					message('Přihlášení proběhlo úspěšně.')
				} else {
					message('Přihlášení se nezdařilo.')
				}
			})
			.catch(() => {
				message('Přihlášení se nezdařilo.')
			})
	}

	const resetUserPassword = (email: string) => {
		firebaseResetUserPassword(email)
			.then(() => {
				message(`Na e-mail ${email} jsme Vám odeslali odkaz pro obnovu hesla.`)
			})
			.catch(() => {
				message('Nepodařilo se odeslat e-mail pro obnovu hesla.')
			})
	}
  
	const logoutUser = () => {
		firebaseLogoutUser().then(() => {
			message('Odhlášení proběhlo úspěšně.')
		}).catch(() => {
			message('Odhlášení se nezdařilo.')
		})
	}

	const registerUser = (email: string, password: string) => {
		firebaseRegisterUser(email, password)
			.then(() => {
				message('Registrace proběhla úspěšně.')
			})
			.catch(() => {
				message('Registrace se nezdařila.')
			})
	}

	const updateUserProfile = (data: { displayName?: string | null, photoURL?: string | null }) => {
		firebaseUpdateUserProfile(data).then(() => {
			message('Profil byl úspěšně aktualizován.')
		}).catch(() => {
			message('Aktualizace profilu se nezdařila.')
		})
	}

	const updateUserEmail = (email: string) => {
		firebaseUpdateUserEmail(email).then(() => {
			message('E-mail byl úspěšně aktualizován.')
		}).catch(() => {
			message('Aktualizace e-mailu se nezdařila.')
		})
	}

	return {
		loggedUser,
		loginUser,
		logoutUser,
		registerUser,
		resetUserPassword,
		updateUserProfile,
		updateUserEmail,
	}
}
