import { gql } from '@urql/core'

export const Installation = gql`
  fragment Installation on installation {
    id
    customer
    deliveries
    eshopId
    eshop {
      id
      name
    }
    items
    orderId
    pinCode
    shippingAddress
    servicemanId
    updatedAt
    createdAt
    stateValue
    selectedDateTime
    date
    availabilities {
      availableFrom
      availableTo
      userId
      id
      installationId
    }
    installationComments: commentEntities {
      comment {
        text
        updatedAt
        id
        createdAt
        user {
          firstName
          email
          lastName
        }
      }
    }
  }
`

export const GetInstallation = gql`
  query GetInstallation($id: uuid!) {
    installation: installation_by_pk(id: $id) {
      ...Installation
      eshop {
        name
      }
    }
  }
`

// TODO: Delete
export const FilterInstallations = gql`
  query FilterInstallations(
    $orderBy: [installation_order_by!]
    $limit: Int = 10
    $offset: Int = 0
    $where: installation_bool_exp
  ) {
    installations: installation(
      limit: $limit,
      offset: $offset,
      order_by: $orderBy,
      where: $where
    ) {
      ...Installation
      installationState {
        value
        title
      }
      user {
        id
      }
      eshop {
        name
      }
    }
    installation_aggregate {
      aggregate {
        count
      }
    }
  }
`

export const Installations = gql`
  query Installations(
    $where: installation_bool_exp
    $orderBy: [installation_order_by!]
    $limit: Int = 10
    $offset: Int = 0
  ) {
    installations: installation(
      where: $where,
      limit: $limit,
      offset: $offset,
      order_by: $orderBy
    ) {
      ...Installation
      eshop {
        name
      }
      installationState {
        value
        title
      }
    }
    installationsAggregate: installation_aggregate(
      where: $where,
    ) {
      aggregate {
        count
      }
    }
  }
`
