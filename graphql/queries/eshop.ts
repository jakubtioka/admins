import { gql } from '@urql/core'

export const EshopSelect = gql`
  fragment EshopSelect on eshop {
    id
    name
    servicedCategories {
      categoryName,
      eshopCategoryId,
      serviceType {
        value,
        description
      }
    }
  }
`

export const GetEshopSelect = gql`
  query GetEshopSelect {
    eshops: eshop {
      ...EshopSelect
    }
  }

  ${EshopSelect}
`
