import { gql } from '@urql/core'

export const Availability = gql`
  fragment Availability on availability {
    availableFrom
    availableTo
    user {
      firstName
      lastName
      email
      address
      comment
      companyName
      id
      in
      phone
    }
    id
    installationId
  }
`

export const GetAvailability = gql`
  query GetAvailability($id: uuid!) {
    availability: availability_by_pk(id: $id) {
      ...Availability
    }
  }
`

export const GetInstallationAvailability = gql`
  query GetInstallationAvailability($where: availability_bool_exp = {}) {
    availability(
      order_by: { availableFrom: asc }
      where: $where
    ) {
      availableFrom
      availableTo
      user {
        firstName
        lastName
        email
        address
        comment
        companyName
        id
        in
        phone
      }
      id
      installationId
    }
  }
`

export const GetServiceType = gql`
  query GetServiceType($where: serviceType_bool_exp = {}) {
    serviceType(where: $where) {
      value
      description
    }
  }
`
