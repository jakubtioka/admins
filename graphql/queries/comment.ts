import { gql } from '@urql/core'

export const Comment = gql`
  fragment Comment on comment {
    id
    text
    createdAt
    updatedAt
    userId
    user {
      lastName
      firstName
      email
    }
  }
`
export const GetComments = gql`
    query GetComments($installationId: uuid!) {
        comment(where: {
            commentEntities: { installationId: { _eq: $installationId } }
        }) {
        text
        user {
            lastName
            firstName
            email
            id
        }
        updatedAt
        createdAt
        userId
        }
    }
`
