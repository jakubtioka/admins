import { gql } from '@urql/core'

export const RoleSelect = gql`
  fragment RoleSelect on role {
    value
    description
  }
`

export const GetRoleSelect = gql`
  query GetRoleSelect {
    roles: role {
        ...RoleSelect
    }
  }
`
