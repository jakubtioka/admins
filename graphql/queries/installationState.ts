import { gql } from '@urql/vue'

export const InstallationStateSelect = gql`
  fragment InstallationStateSelect on installationState {
    value
    title
  }
`

export const GetInstallationStateSelect = gql`
query GetInstallationStateSelect {
    installationStates: installationState {
      ...InstallationStateSelect
  }
}
`
