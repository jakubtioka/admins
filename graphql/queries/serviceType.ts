import { gql } from '@urql/core'

export const ServiceTypeSelect = gql`
  fragment ServiceTypeSelect on serviceType {
    value
    description
  }
`

export const GetServiceTypeSelect = gql`
  query GetServiceTypeSelect {
    serviceTypes: serviceType {
        ...ServiceTypeSelect
    }
  }
`
