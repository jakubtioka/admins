import { gql } from '@urql/core'

export const zipCode = gql`
  fragment Zipcode on zipCodes {
    city
    district
    id
    region
  }
`


export const GetAllZipCodes = gql`
    query GetAllZipCodes {
        zipCodes {
            city
            district
            id
            region
        }
    }
`

export const GetAssignedZipCodes = gql`
  query GetAssignedZipCodes($userId: uuid!) {
    zipCodes(where: {userZipCodes: {userId: {_eq: $userId}}}) {
      city
      district
      id
      region
    }
  }
`
