import { gql } from '@urql/core'

export const UserSelect = gql`
  fragment UserSelect on user {
    id
    firstName
    lastName
    companyName
  }
`

export const GetUserSelect = gql`
  query GetUserSelect {
    users: user {
      ...UserSelect
    }
  }
`

export const GetUserCompanySelect = gql`
  query GetUserCompanySelect {
    user(distinct_on: companyName) {
      companyName
    }
  }
`

export const User = gql`
  fragment User on user {
    id
    firebaseId
    firstName
    lastName
    login
    password
    address
    email
    companyName
    email
    phone
    comment
    roleValue
    isActive
    in
    tin
    userServiceTypes {
      userId
      serviceTypeValue
      serviceType {
        value
        description
      }
    }
    role {
      value
      description
    }
    availabilities {
      availableFrom
      availableTo
    }
    userZipCodes {
      zipCodeId
    }
  }
`

export const GetUser = gql`
  query GetUser($id: uuid!) {
    user: user_by_pk(id: $id) {
      ...User
    }
  }
`

export const Users = gql`
  query Users(
    $where: user_bool_exp
    $orderBy: [user_order_by!]
    $limit: Int = 10
    $offset: Int = 0
  ) {
    users: user(
      where: $where
      order_by: $orderBy
      limit: $limit
      offset: $offset
    ) {
      ...User
    }
    usersAgregate: user_aggregate(
      where: $where
    ) {
      aggregate {
        count
      }
    }
  }
`
