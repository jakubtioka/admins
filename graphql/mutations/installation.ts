import { gql } from '@urql/core'

export const UpdateInstallation = gql`
  mutation UpdateInstallation($id: uuid, $installationInput: installation_set_input) {
    installation: update_installation( where: { id: { _eq: $id } }, _set: $installationInput ) {
      returning {
          id
      }
    }
  }
`

export const CreateInstallation =  gql`
  mutation CreateInstallation($installationInput: installation_insert_input!) {
    installation: insert_installation_one(object: $installationInput) {
      id
    }
  }
`

export const DeleteInstallation = gql`
  mutation DeleteInstallation($id: uuid!) {
    installation: delete_installation_by_pk(id: $id) {
      id
    }
  }
`
