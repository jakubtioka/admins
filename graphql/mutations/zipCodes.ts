import { gql } from '@urql/vue'

export const CreateUserZipCodes = gql`
    mutation CreateUserZipCodes($userId: uuid!, $zipCodeInput: [userZipCodes_insert_input!]!) {
        delete_userZipCodes(where: {userId: {_eq: $userId}}) {
            returning {
                id
            }
        }
        insert_userZipCodes(objects: $zipCodeInput) {
            returning {
                id
            }
        }
    }
`
