import { gql } from '@urql/core'

export const CreateComment =  gql`
mutation CreateComment($commentInput: comment_insert_input!) {
  comment: insert_comment_one(object: $commentInput) {
    id
  }
}
`
export const UpdateComment = gql`
  mutation UpdateComment($id: uuid, $commentInput: comment_set_input) {
    comment: update_comment( where: { id: { _eq: $id } }, _set: $commentInput ) {
      returning {
          id
      }
    }
  }
`
export const DeleteComment = gql`
  mutation DeleteComment($id: uuid!) {
    comment: delete_comment_by_pk(id: $id) {
      id
    }
  }
`
