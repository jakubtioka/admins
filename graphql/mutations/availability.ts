import { gql } from '@urql/vue'

export const UpdateAvailability = gql`
    mutation UpdateAvailability($installId: uuid!,$availabilityIds: [uuid!]!) {
        resetIds: update_availability (
            where: { installationId: { _eq: $installId } }, 
            _set: { installationId: null }
        )   
            {
                returning { id }
            }
        update_availability(
            where: { id: { _in: $availabilityIds } }, 
            _set: { installationId: $installId } 
        )   
            {
                returning { id }
            }
    }
`

export const CreateAvailability = gql`
    mutation CreateAvailability($availabilityInput: [availability_insert_input!]!) {
        insert_availability(objects: $availabilityInput) {
            returning { id }
        }
    }
`


