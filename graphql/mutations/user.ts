import { gql } from '@urql/core'

export const CreateUser =  gql`
  mutation CreateUser($userInput: user_insert_input!) {
    user: insert_user_one(object: $userInput) {
      id
    }
  }
`

export const UpdateUser = gql`
  mutation UpdateUser(
    $id: uuid!,
    $userInput: user_set_input,
    $userServiceTypesInput: [userServiceTypes_insert_input!]!
  ) {
    delete_userServiceTypes(where: {userId: {_eq: $id}}) {     
      returning { serviceTypeValue }
    }
    insert_userServiceTypes(
      objects: $userServiceTypesInput,
      on_conflict: {
        constraint: userServiceTypes_pkey,
        update_columns: serviceTypeValue
      }
    ) {
      returning { serviceTypeValue }
    }
    user: update_user_by_pk(pk_columns: {id: $id}, _set: $userInput) {
      id
    }
  }
`

export const DeleteUser = gql`
  mutation DeleteUser($id: uuid!) {
    user: delete_user_by_pk(id: $id) {
      id
    }
  }
`

export const updateUserByPK = gql`
  mutation updateUserByPK($id: uuid!,$userInput: user_set_input!) {
    update_user_by_pk(
      pk_columns: { id: $id }, 
      _set: $userInput
    ) {
      firebaseId
      id
    }
  }
`
