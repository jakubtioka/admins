import { gql } from '@urql/core'

export const Comment = gql`
    subscription subscribeComment($installationId: uuid!) {
        comment(where: { commentEntities: { installationId: { _eq: $installationId } } }) 
        {
            id
            text
            createdAt
            updatedAt
            user {
                firstName
                lastName
            }
        }
    }
`
