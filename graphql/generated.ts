import gql from 'graphql-tag';
import * as Urql from '@urql/vue';
export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
export type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  bpchar: any;
  jsonb: any;
  timestamptz: any;
  uuid: any;
};

/** Boolean expression to compare columns of type "Boolean". All fields are combined with logical 'AND'. */
export type Boolean_Comparison_Exp = {
  _eq?: InputMaybe<Scalars['Boolean']>;
  _gt?: InputMaybe<Scalars['Boolean']>;
  _gte?: InputMaybe<Scalars['Boolean']>;
  _in?: InputMaybe<Array<Scalars['Boolean']>>;
  _is_null?: InputMaybe<Scalars['Boolean']>;
  _lt?: InputMaybe<Scalars['Boolean']>;
  _lte?: InputMaybe<Scalars['Boolean']>;
  _neq?: InputMaybe<Scalars['Boolean']>;
  _nin?: InputMaybe<Array<Scalars['Boolean']>>;
};

/** Boolean expression to compare columns of type "Float". All fields are combined with logical 'AND'. */
export type Float_Comparison_Exp = {
  _eq?: InputMaybe<Scalars['Float']>;
  _gt?: InputMaybe<Scalars['Float']>;
  _gte?: InputMaybe<Scalars['Float']>;
  _in?: InputMaybe<Array<Scalars['Float']>>;
  _is_null?: InputMaybe<Scalars['Boolean']>;
  _lt?: InputMaybe<Scalars['Float']>;
  _lte?: InputMaybe<Scalars['Float']>;
  _neq?: InputMaybe<Scalars['Float']>;
  _nin?: InputMaybe<Array<Scalars['Float']>>;
};

/** Boolean expression to compare columns of type "String". All fields are combined with logical 'AND'. */
export type String_Comparison_Exp = {
  _eq?: InputMaybe<Scalars['String']>;
  _gt?: InputMaybe<Scalars['String']>;
  _gte?: InputMaybe<Scalars['String']>;
  /** does the column match the given case-insensitive pattern */
  _ilike?: InputMaybe<Scalars['String']>;
  _in?: InputMaybe<Array<Scalars['String']>>;
  /** does the column match the given POSIX regular expression, case insensitive */
  _iregex?: InputMaybe<Scalars['String']>;
  _is_null?: InputMaybe<Scalars['Boolean']>;
  /** does the column match the given pattern */
  _like?: InputMaybe<Scalars['String']>;
  _lt?: InputMaybe<Scalars['String']>;
  _lte?: InputMaybe<Scalars['String']>;
  _neq?: InputMaybe<Scalars['String']>;
  /** does the column NOT match the given case-insensitive pattern */
  _nilike?: InputMaybe<Scalars['String']>;
  _nin?: InputMaybe<Array<Scalars['String']>>;
  /** does the column NOT match the given POSIX regular expression, case insensitive */
  _niregex?: InputMaybe<Scalars['String']>;
  /** does the column NOT match the given pattern */
  _nlike?: InputMaybe<Scalars['String']>;
  /** does the column NOT match the given POSIX regular expression, case sensitive */
  _nregex?: InputMaybe<Scalars['String']>;
  /** does the column NOT match the given SQL regular expression */
  _nsimilar?: InputMaybe<Scalars['String']>;
  /** does the column match the given POSIX regular expression, case sensitive */
  _regex?: InputMaybe<Scalars['String']>;
  /** does the column match the given SQL regular expression */
  _similar?: InputMaybe<Scalars['String']>;
};

/** columns and relationships of "availability" */
export type Availability = {
  __typename?: 'availability';
  /** An array relationship */
  availabilityZipCodes: Array<AvailabilityZipCodes>;
  /** An aggregate relationship */
  availabilityZipCodes_aggregate: AvailabilityZipCodes_Aggregate;
  availableFrom?: Maybe<Scalars['timestamptz']>;
  availableTo?: Maybe<Scalars['timestamptz']>;
  comment?: Maybe<Scalars['String']>;
  id: Scalars['uuid'];
  /** An object relationship */
  installation?: Maybe<Installation>;
  installationId?: Maybe<Scalars['uuid']>;
  /** An object relationship */
  user: User;
  userId: Scalars['uuid'];
};


/** columns and relationships of "availability" */
export type AvailabilityAvailabilityZipCodesArgs = {
  distinct_on?: InputMaybe<Array<AvailabilityZipCodes_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<AvailabilityZipCodes_Order_By>>;
  where?: InputMaybe<AvailabilityZipCodes_Bool_Exp>;
};


/** columns and relationships of "availability" */
export type AvailabilityAvailabilityZipCodes_AggregateArgs = {
  distinct_on?: InputMaybe<Array<AvailabilityZipCodes_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<AvailabilityZipCodes_Order_By>>;
  where?: InputMaybe<AvailabilityZipCodes_Bool_Exp>;
};

/** connection table between availability and postCodes */
export type AvailabilityZipCodes = {
  __typename?: 'availabilityZipCodes';
  /** An object relationship */
  availability: Availability;
  availabilityId: Scalars['uuid'];
  id: Scalars['uuid'];
  /** An object relationship */
  zipCode: ZipCodes;
  zipCodeId: Scalars['String'];
};

/** aggregated selection of "availabilityZipCodes" */
export type AvailabilityZipCodes_Aggregate = {
  __typename?: 'availabilityZipCodes_aggregate';
  aggregate?: Maybe<AvailabilityZipCodes_Aggregate_Fields>;
  nodes: Array<AvailabilityZipCodes>;
};

/** aggregate fields of "availabilityZipCodes" */
export type AvailabilityZipCodes_Aggregate_Fields = {
  __typename?: 'availabilityZipCodes_aggregate_fields';
  count: Scalars['Int'];
  max?: Maybe<AvailabilityZipCodes_Max_Fields>;
  min?: Maybe<AvailabilityZipCodes_Min_Fields>;
};


/** aggregate fields of "availabilityZipCodes" */
export type AvailabilityZipCodes_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<AvailabilityZipCodes_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "availabilityZipCodes" */
export type AvailabilityZipCodes_Aggregate_Order_By = {
  count?: InputMaybe<Order_By>;
  max?: InputMaybe<AvailabilityZipCodes_Max_Order_By>;
  min?: InputMaybe<AvailabilityZipCodes_Min_Order_By>;
};

/** input type for inserting array relation for remote table "availabilityZipCodes" */
export type AvailabilityZipCodes_Arr_Rel_Insert_Input = {
  data: Array<AvailabilityZipCodes_Insert_Input>;
  /** upsert condition */
  on_conflict?: InputMaybe<AvailabilityZipCodes_On_Conflict>;
};

/** Boolean expression to filter rows from the table "availabilityZipCodes". All fields are combined with a logical 'AND'. */
export type AvailabilityZipCodes_Bool_Exp = {
  _and?: InputMaybe<Array<AvailabilityZipCodes_Bool_Exp>>;
  _not?: InputMaybe<AvailabilityZipCodes_Bool_Exp>;
  _or?: InputMaybe<Array<AvailabilityZipCodes_Bool_Exp>>;
  availability?: InputMaybe<Availability_Bool_Exp>;
  availabilityId?: InputMaybe<Uuid_Comparison_Exp>;
  id?: InputMaybe<Uuid_Comparison_Exp>;
  zipCode?: InputMaybe<ZipCodes_Bool_Exp>;
  zipCodeId?: InputMaybe<String_Comparison_Exp>;
};

/** unique or primary key constraints on table "availabilityZipCodes" */
export enum AvailabilityZipCodes_Constraint {
  /** unique or primary key constraint */
  AvailabilityZipCodesPkey = 'availabilityZipCodes_pkey',
  /** unique or primary key constraint */
  AvailabilityZipCodesZipCodeIdAvailabilityIdKey = 'availabilityZipCodes_zipCodeId_availabilityId_key'
}

/** input type for inserting data into table "availabilityZipCodes" */
export type AvailabilityZipCodes_Insert_Input = {
  availability?: InputMaybe<Availability_Obj_Rel_Insert_Input>;
  availabilityId?: InputMaybe<Scalars['uuid']>;
  id?: InputMaybe<Scalars['uuid']>;
  zipCode?: InputMaybe<ZipCodes_Obj_Rel_Insert_Input>;
  zipCodeId?: InputMaybe<Scalars['String']>;
};

/** aggregate max on columns */
export type AvailabilityZipCodes_Max_Fields = {
  __typename?: 'availabilityZipCodes_max_fields';
  availabilityId?: Maybe<Scalars['uuid']>;
  id?: Maybe<Scalars['uuid']>;
  zipCodeId?: Maybe<Scalars['String']>;
};

/** order by max() on columns of table "availabilityZipCodes" */
export type AvailabilityZipCodes_Max_Order_By = {
  availabilityId?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  zipCodeId?: InputMaybe<Order_By>;
};

/** aggregate min on columns */
export type AvailabilityZipCodes_Min_Fields = {
  __typename?: 'availabilityZipCodes_min_fields';
  availabilityId?: Maybe<Scalars['uuid']>;
  id?: Maybe<Scalars['uuid']>;
  zipCodeId?: Maybe<Scalars['String']>;
};

/** order by min() on columns of table "availabilityZipCodes" */
export type AvailabilityZipCodes_Min_Order_By = {
  availabilityId?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  zipCodeId?: InputMaybe<Order_By>;
};

/** response of any mutation on the table "availabilityZipCodes" */
export type AvailabilityZipCodes_Mutation_Response = {
  __typename?: 'availabilityZipCodes_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int'];
  /** data from the rows affected by the mutation */
  returning: Array<AvailabilityZipCodes>;
};

/** on_conflict condition type for table "availabilityZipCodes" */
export type AvailabilityZipCodes_On_Conflict = {
  constraint: AvailabilityZipCodes_Constraint;
  update_columns?: Array<AvailabilityZipCodes_Update_Column>;
  where?: InputMaybe<AvailabilityZipCodes_Bool_Exp>;
};

/** Ordering options when selecting data from "availabilityZipCodes". */
export type AvailabilityZipCodes_Order_By = {
  availability?: InputMaybe<Availability_Order_By>;
  availabilityId?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  zipCode?: InputMaybe<ZipCodes_Order_By>;
  zipCodeId?: InputMaybe<Order_By>;
};

/** primary key columns input for table: availabilityZipCodes */
export type AvailabilityZipCodes_Pk_Columns_Input = {
  id: Scalars['uuid'];
};

/** select columns of table "availabilityZipCodes" */
export enum AvailabilityZipCodes_Select_Column {
  /** column name */
  AvailabilityId = 'availabilityId',
  /** column name */
  Id = 'id',
  /** column name */
  ZipCodeId = 'zipCodeId'
}

/** input type for updating data in table "availabilityZipCodes" */
export type AvailabilityZipCodes_Set_Input = {
  availabilityId?: InputMaybe<Scalars['uuid']>;
  id?: InputMaybe<Scalars['uuid']>;
  zipCodeId?: InputMaybe<Scalars['String']>;
};

/** update columns of table "availabilityZipCodes" */
export enum AvailabilityZipCodes_Update_Column {
  /** column name */
  AvailabilityId = 'availabilityId',
  /** column name */
  Id = 'id',
  /** column name */
  ZipCodeId = 'zipCodeId'
}

/** aggregated selection of "availability" */
export type Availability_Aggregate = {
  __typename?: 'availability_aggregate';
  aggregate?: Maybe<Availability_Aggregate_Fields>;
  nodes: Array<Availability>;
};

/** aggregate fields of "availability" */
export type Availability_Aggregate_Fields = {
  __typename?: 'availability_aggregate_fields';
  count: Scalars['Int'];
  max?: Maybe<Availability_Max_Fields>;
  min?: Maybe<Availability_Min_Fields>;
};


/** aggregate fields of "availability" */
export type Availability_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<Availability_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "availability" */
export type Availability_Aggregate_Order_By = {
  count?: InputMaybe<Order_By>;
  max?: InputMaybe<Availability_Max_Order_By>;
  min?: InputMaybe<Availability_Min_Order_By>;
};

/** input type for inserting array relation for remote table "availability" */
export type Availability_Arr_Rel_Insert_Input = {
  data: Array<Availability_Insert_Input>;
  /** upsert condition */
  on_conflict?: InputMaybe<Availability_On_Conflict>;
};

/** Boolean expression to filter rows from the table "availability". All fields are combined with a logical 'AND'. */
export type Availability_Bool_Exp = {
  _and?: InputMaybe<Array<Availability_Bool_Exp>>;
  _not?: InputMaybe<Availability_Bool_Exp>;
  _or?: InputMaybe<Array<Availability_Bool_Exp>>;
  availabilityZipCodes?: InputMaybe<AvailabilityZipCodes_Bool_Exp>;
  availableFrom?: InputMaybe<Timestamptz_Comparison_Exp>;
  availableTo?: InputMaybe<Timestamptz_Comparison_Exp>;
  comment?: InputMaybe<String_Comparison_Exp>;
  id?: InputMaybe<Uuid_Comparison_Exp>;
  installation?: InputMaybe<Installation_Bool_Exp>;
  installationId?: InputMaybe<Uuid_Comparison_Exp>;
  user?: InputMaybe<User_Bool_Exp>;
  userId?: InputMaybe<Uuid_Comparison_Exp>;
};

/** unique or primary key constraints on table "availability" */
export enum Availability_Constraint {
  /** unique or primary key constraint */
  AvailabilityAvailabilityIdKey = 'availability_availabilityId_key',
  /** unique or primary key constraint */
  AvailabilityPkey = 'availability_pkey'
}

/** input type for inserting data into table "availability" */
export type Availability_Insert_Input = {
  availabilityZipCodes?: InputMaybe<AvailabilityZipCodes_Arr_Rel_Insert_Input>;
  availableFrom?: InputMaybe<Scalars['timestamptz']>;
  availableTo?: InputMaybe<Scalars['timestamptz']>;
  comment?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['uuid']>;
  installation?: InputMaybe<Installation_Obj_Rel_Insert_Input>;
  installationId?: InputMaybe<Scalars['uuid']>;
  user?: InputMaybe<User_Obj_Rel_Insert_Input>;
  userId?: InputMaybe<Scalars['uuid']>;
};

/** aggregate max on columns */
export type Availability_Max_Fields = {
  __typename?: 'availability_max_fields';
  availableFrom?: Maybe<Scalars['timestamptz']>;
  availableTo?: Maybe<Scalars['timestamptz']>;
  comment?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['uuid']>;
  installationId?: Maybe<Scalars['uuid']>;
  userId?: Maybe<Scalars['uuid']>;
};

/** order by max() on columns of table "availability" */
export type Availability_Max_Order_By = {
  availableFrom?: InputMaybe<Order_By>;
  availableTo?: InputMaybe<Order_By>;
  comment?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  installationId?: InputMaybe<Order_By>;
  userId?: InputMaybe<Order_By>;
};

/** aggregate min on columns */
export type Availability_Min_Fields = {
  __typename?: 'availability_min_fields';
  availableFrom?: Maybe<Scalars['timestamptz']>;
  availableTo?: Maybe<Scalars['timestamptz']>;
  comment?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['uuid']>;
  installationId?: Maybe<Scalars['uuid']>;
  userId?: Maybe<Scalars['uuid']>;
};

/** order by min() on columns of table "availability" */
export type Availability_Min_Order_By = {
  availableFrom?: InputMaybe<Order_By>;
  availableTo?: InputMaybe<Order_By>;
  comment?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  installationId?: InputMaybe<Order_By>;
  userId?: InputMaybe<Order_By>;
};

/** response of any mutation on the table "availability" */
export type Availability_Mutation_Response = {
  __typename?: 'availability_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int'];
  /** data from the rows affected by the mutation */
  returning: Array<Availability>;
};

/** input type for inserting object relation for remote table "availability" */
export type Availability_Obj_Rel_Insert_Input = {
  data: Availability_Insert_Input;
  /** upsert condition */
  on_conflict?: InputMaybe<Availability_On_Conflict>;
};

/** on_conflict condition type for table "availability" */
export type Availability_On_Conflict = {
  constraint: Availability_Constraint;
  update_columns?: Array<Availability_Update_Column>;
  where?: InputMaybe<Availability_Bool_Exp>;
};

/** Ordering options when selecting data from "availability". */
export type Availability_Order_By = {
  availabilityZipCodes_aggregate?: InputMaybe<AvailabilityZipCodes_Aggregate_Order_By>;
  availableFrom?: InputMaybe<Order_By>;
  availableTo?: InputMaybe<Order_By>;
  comment?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  installation?: InputMaybe<Installation_Order_By>;
  installationId?: InputMaybe<Order_By>;
  user?: InputMaybe<User_Order_By>;
  userId?: InputMaybe<Order_By>;
};

/** primary key columns input for table: availability */
export type Availability_Pk_Columns_Input = {
  id: Scalars['uuid'];
};

/** select columns of table "availability" */
export enum Availability_Select_Column {
  /** column name */
  AvailableFrom = 'availableFrom',
  /** column name */
  AvailableTo = 'availableTo',
  /** column name */
  Comment = 'comment',
  /** column name */
  Id = 'id',
  /** column name */
  InstallationId = 'installationId',
  /** column name */
  UserId = 'userId'
}

/** input type for updating data in table "availability" */
export type Availability_Set_Input = {
  availableFrom?: InputMaybe<Scalars['timestamptz']>;
  availableTo?: InputMaybe<Scalars['timestamptz']>;
  comment?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['uuid']>;
  installationId?: InputMaybe<Scalars['uuid']>;
  userId?: InputMaybe<Scalars['uuid']>;
};

/** update columns of table "availability" */
export enum Availability_Update_Column {
  /** column name */
  AvailableFrom = 'availableFrom',
  /** column name */
  AvailableTo = 'availableTo',
  /** column name */
  Comment = 'comment',
  /** column name */
  Id = 'id',
  /** column name */
  InstallationId = 'installationId',
  /** column name */
  UserId = 'userId'
}

/** Boolean expression to compare columns of type "bpchar". All fields are combined with logical 'AND'. */
export type Bpchar_Comparison_Exp = {
  _eq?: InputMaybe<Scalars['bpchar']>;
  _gt?: InputMaybe<Scalars['bpchar']>;
  _gte?: InputMaybe<Scalars['bpchar']>;
  /** does the column match the given case-insensitive pattern */
  _ilike?: InputMaybe<Scalars['bpchar']>;
  _in?: InputMaybe<Array<Scalars['bpchar']>>;
  /** does the column match the given POSIX regular expression, case insensitive */
  _iregex?: InputMaybe<Scalars['bpchar']>;
  _is_null?: InputMaybe<Scalars['Boolean']>;
  /** does the column match the given pattern */
  _like?: InputMaybe<Scalars['bpchar']>;
  _lt?: InputMaybe<Scalars['bpchar']>;
  _lte?: InputMaybe<Scalars['bpchar']>;
  _neq?: InputMaybe<Scalars['bpchar']>;
  /** does the column NOT match the given case-insensitive pattern */
  _nilike?: InputMaybe<Scalars['bpchar']>;
  _nin?: InputMaybe<Array<Scalars['bpchar']>>;
  /** does the column NOT match the given POSIX regular expression, case insensitive */
  _niregex?: InputMaybe<Scalars['bpchar']>;
  /** does the column NOT match the given pattern */
  _nlike?: InputMaybe<Scalars['bpchar']>;
  /** does the column NOT match the given POSIX regular expression, case sensitive */
  _nregex?: InputMaybe<Scalars['bpchar']>;
  /** does the column NOT match the given SQL regular expression */
  _nsimilar?: InputMaybe<Scalars['bpchar']>;
  /** does the column match the given POSIX regular expression, case sensitive */
  _regex?: InputMaybe<Scalars['bpchar']>;
  /** does the column match the given SQL regular expression */
  _similar?: InputMaybe<Scalars['bpchar']>;
};

/** columns and relationships of "comment" */
export type Comment = {
  __typename?: 'comment';
  /** An array relationship */
  commentEntities: Array<CommentEntity>;
  /** An aggregate relationship */
  commentEntities_aggregate: CommentEntity_Aggregate;
  createdAt?: Maybe<Scalars['timestamptz']>;
  id: Scalars['uuid'];
  text: Scalars['String'];
  updatedAt?: Maybe<Scalars['timestamptz']>;
  /** An object relationship */
  user?: Maybe<User>;
  userId?: Maybe<Scalars['uuid']>;
};


/** columns and relationships of "comment" */
export type CommentCommentEntitiesArgs = {
  distinct_on?: InputMaybe<Array<CommentEntity_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<CommentEntity_Order_By>>;
  where?: InputMaybe<CommentEntity_Bool_Exp>;
};


/** columns and relationships of "comment" */
export type CommentCommentEntities_AggregateArgs = {
  distinct_on?: InputMaybe<Array<CommentEntity_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<CommentEntity_Order_By>>;
  where?: InputMaybe<CommentEntity_Bool_Exp>;
};

/** connection table between comment and installation tables */
export type CommentEntity = {
  __typename?: 'commentEntity';
  /** An object relationship */
  comment: Comment;
  commentId: Scalars['uuid'];
  id: Scalars['uuid'];
  /** An object relationship */
  installation: Installation;
  installationId: Scalars['uuid'];
};

/** aggregated selection of "commentEntity" */
export type CommentEntity_Aggregate = {
  __typename?: 'commentEntity_aggregate';
  aggregate?: Maybe<CommentEntity_Aggregate_Fields>;
  nodes: Array<CommentEntity>;
};

/** aggregate fields of "commentEntity" */
export type CommentEntity_Aggregate_Fields = {
  __typename?: 'commentEntity_aggregate_fields';
  count: Scalars['Int'];
  max?: Maybe<CommentEntity_Max_Fields>;
  min?: Maybe<CommentEntity_Min_Fields>;
};


/** aggregate fields of "commentEntity" */
export type CommentEntity_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<CommentEntity_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "commentEntity" */
export type CommentEntity_Aggregate_Order_By = {
  count?: InputMaybe<Order_By>;
  max?: InputMaybe<CommentEntity_Max_Order_By>;
  min?: InputMaybe<CommentEntity_Min_Order_By>;
};

/** input type for inserting array relation for remote table "commentEntity" */
export type CommentEntity_Arr_Rel_Insert_Input = {
  data: Array<CommentEntity_Insert_Input>;
  /** upsert condition */
  on_conflict?: InputMaybe<CommentEntity_On_Conflict>;
};

/** Boolean expression to filter rows from the table "commentEntity". All fields are combined with a logical 'AND'. */
export type CommentEntity_Bool_Exp = {
  _and?: InputMaybe<Array<CommentEntity_Bool_Exp>>;
  _not?: InputMaybe<CommentEntity_Bool_Exp>;
  _or?: InputMaybe<Array<CommentEntity_Bool_Exp>>;
  comment?: InputMaybe<Comment_Bool_Exp>;
  commentId?: InputMaybe<Uuid_Comparison_Exp>;
  id?: InputMaybe<Uuid_Comparison_Exp>;
  installation?: InputMaybe<Installation_Bool_Exp>;
  installationId?: InputMaybe<Uuid_Comparison_Exp>;
};

/** unique or primary key constraints on table "commentEntity" */
export enum CommentEntity_Constraint {
  /** unique or primary key constraint */
  CommentEntityPkey = 'commentEntity_pkey'
}

/** input type for inserting data into table "commentEntity" */
export type CommentEntity_Insert_Input = {
  comment?: InputMaybe<Comment_Obj_Rel_Insert_Input>;
  commentId?: InputMaybe<Scalars['uuid']>;
  id?: InputMaybe<Scalars['uuid']>;
  installation?: InputMaybe<Installation_Obj_Rel_Insert_Input>;
  installationId?: InputMaybe<Scalars['uuid']>;
};

/** aggregate max on columns */
export type CommentEntity_Max_Fields = {
  __typename?: 'commentEntity_max_fields';
  commentId?: Maybe<Scalars['uuid']>;
  id?: Maybe<Scalars['uuid']>;
  installationId?: Maybe<Scalars['uuid']>;
};

/** order by max() on columns of table "commentEntity" */
export type CommentEntity_Max_Order_By = {
  commentId?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  installationId?: InputMaybe<Order_By>;
};

/** aggregate min on columns */
export type CommentEntity_Min_Fields = {
  __typename?: 'commentEntity_min_fields';
  commentId?: Maybe<Scalars['uuid']>;
  id?: Maybe<Scalars['uuid']>;
  installationId?: Maybe<Scalars['uuid']>;
};

/** order by min() on columns of table "commentEntity" */
export type CommentEntity_Min_Order_By = {
  commentId?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  installationId?: InputMaybe<Order_By>;
};

/** response of any mutation on the table "commentEntity" */
export type CommentEntity_Mutation_Response = {
  __typename?: 'commentEntity_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int'];
  /** data from the rows affected by the mutation */
  returning: Array<CommentEntity>;
};

/** on_conflict condition type for table "commentEntity" */
export type CommentEntity_On_Conflict = {
  constraint: CommentEntity_Constraint;
  update_columns?: Array<CommentEntity_Update_Column>;
  where?: InputMaybe<CommentEntity_Bool_Exp>;
};

/** Ordering options when selecting data from "commentEntity". */
export type CommentEntity_Order_By = {
  comment?: InputMaybe<Comment_Order_By>;
  commentId?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  installation?: InputMaybe<Installation_Order_By>;
  installationId?: InputMaybe<Order_By>;
};

/** primary key columns input for table: commentEntity */
export type CommentEntity_Pk_Columns_Input = {
  id: Scalars['uuid'];
};

/** select columns of table "commentEntity" */
export enum CommentEntity_Select_Column {
  /** column name */
  CommentId = 'commentId',
  /** column name */
  Id = 'id',
  /** column name */
  InstallationId = 'installationId'
}

/** input type for updating data in table "commentEntity" */
export type CommentEntity_Set_Input = {
  commentId?: InputMaybe<Scalars['uuid']>;
  id?: InputMaybe<Scalars['uuid']>;
  installationId?: InputMaybe<Scalars['uuid']>;
};

/** update columns of table "commentEntity" */
export enum CommentEntity_Update_Column {
  /** column name */
  CommentId = 'commentId',
  /** column name */
  Id = 'id',
  /** column name */
  InstallationId = 'installationId'
}

/** aggregated selection of "comment" */
export type Comment_Aggregate = {
  __typename?: 'comment_aggregate';
  aggregate?: Maybe<Comment_Aggregate_Fields>;
  nodes: Array<Comment>;
};

/** aggregate fields of "comment" */
export type Comment_Aggregate_Fields = {
  __typename?: 'comment_aggregate_fields';
  count: Scalars['Int'];
  max?: Maybe<Comment_Max_Fields>;
  min?: Maybe<Comment_Min_Fields>;
};


/** aggregate fields of "comment" */
export type Comment_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<Comment_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "comment" */
export type Comment_Aggregate_Order_By = {
  count?: InputMaybe<Order_By>;
  max?: InputMaybe<Comment_Max_Order_By>;
  min?: InputMaybe<Comment_Min_Order_By>;
};

/** input type for inserting array relation for remote table "comment" */
export type Comment_Arr_Rel_Insert_Input = {
  data: Array<Comment_Insert_Input>;
  /** upsert condition */
  on_conflict?: InputMaybe<Comment_On_Conflict>;
};

/** Boolean expression to filter rows from the table "comment". All fields are combined with a logical 'AND'. */
export type Comment_Bool_Exp = {
  _and?: InputMaybe<Array<Comment_Bool_Exp>>;
  _not?: InputMaybe<Comment_Bool_Exp>;
  _or?: InputMaybe<Array<Comment_Bool_Exp>>;
  commentEntities?: InputMaybe<CommentEntity_Bool_Exp>;
  createdAt?: InputMaybe<Timestamptz_Comparison_Exp>;
  id?: InputMaybe<Uuid_Comparison_Exp>;
  text?: InputMaybe<String_Comparison_Exp>;
  updatedAt?: InputMaybe<Timestamptz_Comparison_Exp>;
  user?: InputMaybe<User_Bool_Exp>;
  userId?: InputMaybe<Uuid_Comparison_Exp>;
};

/** unique or primary key constraints on table "comment" */
export enum Comment_Constraint {
  /** unique or primary key constraint */
  CommentPkey = 'comment_pkey'
}

/** input type for inserting data into table "comment" */
export type Comment_Insert_Input = {
  commentEntities?: InputMaybe<CommentEntity_Arr_Rel_Insert_Input>;
  createdAt?: InputMaybe<Scalars['timestamptz']>;
  id?: InputMaybe<Scalars['uuid']>;
  text?: InputMaybe<Scalars['String']>;
  updatedAt?: InputMaybe<Scalars['timestamptz']>;
  user?: InputMaybe<User_Obj_Rel_Insert_Input>;
  userId?: InputMaybe<Scalars['uuid']>;
};

/** aggregate max on columns */
export type Comment_Max_Fields = {
  __typename?: 'comment_max_fields';
  createdAt?: Maybe<Scalars['timestamptz']>;
  id?: Maybe<Scalars['uuid']>;
  text?: Maybe<Scalars['String']>;
  updatedAt?: Maybe<Scalars['timestamptz']>;
  userId?: Maybe<Scalars['uuid']>;
};

/** order by max() on columns of table "comment" */
export type Comment_Max_Order_By = {
  createdAt?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  text?: InputMaybe<Order_By>;
  updatedAt?: InputMaybe<Order_By>;
  userId?: InputMaybe<Order_By>;
};

/** aggregate min on columns */
export type Comment_Min_Fields = {
  __typename?: 'comment_min_fields';
  createdAt?: Maybe<Scalars['timestamptz']>;
  id?: Maybe<Scalars['uuid']>;
  text?: Maybe<Scalars['String']>;
  updatedAt?: Maybe<Scalars['timestamptz']>;
  userId?: Maybe<Scalars['uuid']>;
};

/** order by min() on columns of table "comment" */
export type Comment_Min_Order_By = {
  createdAt?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  text?: InputMaybe<Order_By>;
  updatedAt?: InputMaybe<Order_By>;
  userId?: InputMaybe<Order_By>;
};

/** response of any mutation on the table "comment" */
export type Comment_Mutation_Response = {
  __typename?: 'comment_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int'];
  /** data from the rows affected by the mutation */
  returning: Array<Comment>;
};

/** input type for inserting object relation for remote table "comment" */
export type Comment_Obj_Rel_Insert_Input = {
  data: Comment_Insert_Input;
  /** upsert condition */
  on_conflict?: InputMaybe<Comment_On_Conflict>;
};

/** on_conflict condition type for table "comment" */
export type Comment_On_Conflict = {
  constraint: Comment_Constraint;
  update_columns?: Array<Comment_Update_Column>;
  where?: InputMaybe<Comment_Bool_Exp>;
};

/** Ordering options when selecting data from "comment". */
export type Comment_Order_By = {
  commentEntities_aggregate?: InputMaybe<CommentEntity_Aggregate_Order_By>;
  createdAt?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  text?: InputMaybe<Order_By>;
  updatedAt?: InputMaybe<Order_By>;
  user?: InputMaybe<User_Order_By>;
  userId?: InputMaybe<Order_By>;
};

/** primary key columns input for table: comment */
export type Comment_Pk_Columns_Input = {
  id: Scalars['uuid'];
};

/** select columns of table "comment" */
export enum Comment_Select_Column {
  /** column name */
  CreatedAt = 'createdAt',
  /** column name */
  Id = 'id',
  /** column name */
  Text = 'text',
  /** column name */
  UpdatedAt = 'updatedAt',
  /** column name */
  UserId = 'userId'
}

/** input type for updating data in table "comment" */
export type Comment_Set_Input = {
  createdAt?: InputMaybe<Scalars['timestamptz']>;
  id?: InputMaybe<Scalars['uuid']>;
  text?: InputMaybe<Scalars['String']>;
  updatedAt?: InputMaybe<Scalars['timestamptz']>;
  userId?: InputMaybe<Scalars['uuid']>;
};

/** update columns of table "comment" */
export enum Comment_Update_Column {
  /** column name */
  CreatedAt = 'createdAt',
  /** column name */
  Id = 'id',
  /** column name */
  Text = 'text',
  /** column name */
  UpdatedAt = 'updatedAt',
  /** column name */
  UserId = 'userId'
}

/** columns and relationships of "eshop" */
export type Eshop = {
  __typename?: 'eshop';
  active: Scalars['Boolean'];
  id: Scalars['uuid'];
  /** An array relationship */
  installations: Array<Installation>;
  /** An aggregate relationship */
  installations_aggregate: Installation_Aggregate;
  logo?: Maybe<Scalars['String']>;
  name: Scalars['String'];
  /** An array relationship */
  servicedCategories: Array<ServicedCategory>;
  /** An aggregate relationship */
  servicedCategories_aggregate: ServicedCategory_Aggregate;
  url: Scalars['String'];
};


/** columns and relationships of "eshop" */
export type EshopInstallationsArgs = {
  distinct_on?: InputMaybe<Array<Installation_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Installation_Order_By>>;
  where?: InputMaybe<Installation_Bool_Exp>;
};


/** columns and relationships of "eshop" */
export type EshopInstallations_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Installation_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Installation_Order_By>>;
  where?: InputMaybe<Installation_Bool_Exp>;
};


/** columns and relationships of "eshop" */
export type EshopServicedCategoriesArgs = {
  distinct_on?: InputMaybe<Array<ServicedCategory_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<ServicedCategory_Order_By>>;
  where?: InputMaybe<ServicedCategory_Bool_Exp>;
};


/** columns and relationships of "eshop" */
export type EshopServicedCategories_AggregateArgs = {
  distinct_on?: InputMaybe<Array<ServicedCategory_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<ServicedCategory_Order_By>>;
  where?: InputMaybe<ServicedCategory_Bool_Exp>;
};

/** aggregated selection of "eshop" */
export type Eshop_Aggregate = {
  __typename?: 'eshop_aggregate';
  aggregate?: Maybe<Eshop_Aggregate_Fields>;
  nodes: Array<Eshop>;
};

/** aggregate fields of "eshop" */
export type Eshop_Aggregate_Fields = {
  __typename?: 'eshop_aggregate_fields';
  count: Scalars['Int'];
  max?: Maybe<Eshop_Max_Fields>;
  min?: Maybe<Eshop_Min_Fields>;
};


/** aggregate fields of "eshop" */
export type Eshop_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<Eshop_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']>;
};

/** Boolean expression to filter rows from the table "eshop". All fields are combined with a logical 'AND'. */
export type Eshop_Bool_Exp = {
  _and?: InputMaybe<Array<Eshop_Bool_Exp>>;
  _not?: InputMaybe<Eshop_Bool_Exp>;
  _or?: InputMaybe<Array<Eshop_Bool_Exp>>;
  active?: InputMaybe<Boolean_Comparison_Exp>;
  id?: InputMaybe<Uuid_Comparison_Exp>;
  installations?: InputMaybe<Installation_Bool_Exp>;
  logo?: InputMaybe<String_Comparison_Exp>;
  name?: InputMaybe<String_Comparison_Exp>;
  servicedCategories?: InputMaybe<ServicedCategory_Bool_Exp>;
  url?: InputMaybe<String_Comparison_Exp>;
};

/** unique or primary key constraints on table "eshop" */
export enum Eshop_Constraint {
  /** unique or primary key constraint */
  EshopPkey = 'eshop_pkey'
}

/** input type for inserting data into table "eshop" */
export type Eshop_Insert_Input = {
  active?: InputMaybe<Scalars['Boolean']>;
  id?: InputMaybe<Scalars['uuid']>;
  installations?: InputMaybe<Installation_Arr_Rel_Insert_Input>;
  logo?: InputMaybe<Scalars['String']>;
  name?: InputMaybe<Scalars['String']>;
  servicedCategories?: InputMaybe<ServicedCategory_Arr_Rel_Insert_Input>;
  url?: InputMaybe<Scalars['String']>;
};

/** aggregate max on columns */
export type Eshop_Max_Fields = {
  __typename?: 'eshop_max_fields';
  id?: Maybe<Scalars['uuid']>;
  logo?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  url?: Maybe<Scalars['String']>;
};

/** aggregate min on columns */
export type Eshop_Min_Fields = {
  __typename?: 'eshop_min_fields';
  id?: Maybe<Scalars['uuid']>;
  logo?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  url?: Maybe<Scalars['String']>;
};

/** response of any mutation on the table "eshop" */
export type Eshop_Mutation_Response = {
  __typename?: 'eshop_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int'];
  /** data from the rows affected by the mutation */
  returning: Array<Eshop>;
};

/** input type for inserting object relation for remote table "eshop" */
export type Eshop_Obj_Rel_Insert_Input = {
  data: Eshop_Insert_Input;
  /** upsert condition */
  on_conflict?: InputMaybe<Eshop_On_Conflict>;
};

/** on_conflict condition type for table "eshop" */
export type Eshop_On_Conflict = {
  constraint: Eshop_Constraint;
  update_columns?: Array<Eshop_Update_Column>;
  where?: InputMaybe<Eshop_Bool_Exp>;
};

/** Ordering options when selecting data from "eshop". */
export type Eshop_Order_By = {
  active?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  installations_aggregate?: InputMaybe<Installation_Aggregate_Order_By>;
  logo?: InputMaybe<Order_By>;
  name?: InputMaybe<Order_By>;
  servicedCategories_aggregate?: InputMaybe<ServicedCategory_Aggregate_Order_By>;
  url?: InputMaybe<Order_By>;
};

/** primary key columns input for table: eshop */
export type Eshop_Pk_Columns_Input = {
  id: Scalars['uuid'];
};

/** select columns of table "eshop" */
export enum Eshop_Select_Column {
  /** column name */
  Active = 'active',
  /** column name */
  Id = 'id',
  /** column name */
  Logo = 'logo',
  /** column name */
  Name = 'name',
  /** column name */
  Url = 'url'
}

/** input type for updating data in table "eshop" */
export type Eshop_Set_Input = {
  active?: InputMaybe<Scalars['Boolean']>;
  id?: InputMaybe<Scalars['uuid']>;
  logo?: InputMaybe<Scalars['String']>;
  name?: InputMaybe<Scalars['String']>;
  url?: InputMaybe<Scalars['String']>;
};

/** update columns of table "eshop" */
export enum Eshop_Update_Column {
  /** column name */
  Active = 'active',
  /** column name */
  Id = 'id',
  /** column name */
  Logo = 'logo',
  /** column name */
  Name = 'name',
  /** column name */
  Url = 'url'
}

/** columns and relationships of "installation" */
export type Installation = {
  __typename?: 'installation';
  /** An array relationship */
  availabilities: Array<Availability>;
  /** An aggregate relationship */
  availabilities_aggregate: Availability_Aggregate;
  /** An array relationship */
  commentEntities: Array<CommentEntity>;
  /** An aggregate relationship */
  commentEntities_aggregate: CommentEntity_Aggregate;
  createdAt?: Maybe<Scalars['timestamptz']>;
  customer: Scalars['jsonb'];
  date?: Maybe<Scalars['timestamptz']>;
  deliveries: Scalars['jsonb'];
  /** An object relationship */
  eshop: Eshop;
  eshopId: Scalars['uuid'];
  id: Scalars['uuid'];
  /** An array relationship */
  installationSessions: Array<InstallationSession>;
  /** An aggregate relationship */
  installationSessions_aggregate: InstallationSession_Aggregate;
  /** An object relationship */
  installationState: InstallationState;
  items: Scalars['jsonb'];
  note?: Maybe<Scalars['String']>;
  orderId: Scalars['String'];
  pinCode?: Maybe<Scalars['String']>;
  selectedDateTime?: Maybe<Scalars['jsonb']>;
  servicemanId?: Maybe<Scalars['uuid']>;
  shippingAddress: Scalars['jsonb'];
  stateValue: InstallationState_Enum;
  updatedAt?: Maybe<Scalars['timestamptz']>;
  /** An object relationship */
  user?: Maybe<User>;
};


/** columns and relationships of "installation" */
export type InstallationAvailabilitiesArgs = {
  distinct_on?: InputMaybe<Array<Availability_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Availability_Order_By>>;
  where?: InputMaybe<Availability_Bool_Exp>;
};


/** columns and relationships of "installation" */
export type InstallationAvailabilities_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Availability_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Availability_Order_By>>;
  where?: InputMaybe<Availability_Bool_Exp>;
};


/** columns and relationships of "installation" */
export type InstallationCommentEntitiesArgs = {
  distinct_on?: InputMaybe<Array<CommentEntity_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<CommentEntity_Order_By>>;
  where?: InputMaybe<CommentEntity_Bool_Exp>;
};


/** columns and relationships of "installation" */
export type InstallationCommentEntities_AggregateArgs = {
  distinct_on?: InputMaybe<Array<CommentEntity_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<CommentEntity_Order_By>>;
  where?: InputMaybe<CommentEntity_Bool_Exp>;
};


/** columns and relationships of "installation" */
export type InstallationCustomerArgs = {
  path?: InputMaybe<Scalars['String']>;
};


/** columns and relationships of "installation" */
export type InstallationDeliveriesArgs = {
  path?: InputMaybe<Scalars['String']>;
};


/** columns and relationships of "installation" */
export type InstallationInstallationSessionsArgs = {
  distinct_on?: InputMaybe<Array<InstallationSession_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<InstallationSession_Order_By>>;
  where?: InputMaybe<InstallationSession_Bool_Exp>;
};


/** columns and relationships of "installation" */
export type InstallationInstallationSessions_AggregateArgs = {
  distinct_on?: InputMaybe<Array<InstallationSession_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<InstallationSession_Order_By>>;
  where?: InputMaybe<InstallationSession_Bool_Exp>;
};


/** columns and relationships of "installation" */
export type InstallationItemsArgs = {
  path?: InputMaybe<Scalars['String']>;
};


/** columns and relationships of "installation" */
export type InstallationSelectedDateTimeArgs = {
  path?: InputMaybe<Scalars['String']>;
};


/** columns and relationships of "installation" */
export type InstallationShippingAddressArgs = {
  path?: InputMaybe<Scalars['String']>;
};

/** columns and relationships of "installationSession" */
export type InstallationSession = {
  __typename?: 'installationSession';
  id: Scalars['uuid'];
  /** An object relationship */
  installation: Installation;
  installationId: Scalars['uuid'];
  ipAddress?: Maybe<Scalars['String']>;
  pinCode?: Maybe<Scalars['String']>;
  validTo: Scalars['timestamptz'];
};

/** aggregated selection of "installationSession" */
export type InstallationSession_Aggregate = {
  __typename?: 'installationSession_aggregate';
  aggregate?: Maybe<InstallationSession_Aggregate_Fields>;
  nodes: Array<InstallationSession>;
};

/** aggregate fields of "installationSession" */
export type InstallationSession_Aggregate_Fields = {
  __typename?: 'installationSession_aggregate_fields';
  count: Scalars['Int'];
  max?: Maybe<InstallationSession_Max_Fields>;
  min?: Maybe<InstallationSession_Min_Fields>;
};


/** aggregate fields of "installationSession" */
export type InstallationSession_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<InstallationSession_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "installationSession" */
export type InstallationSession_Aggregate_Order_By = {
  count?: InputMaybe<Order_By>;
  max?: InputMaybe<InstallationSession_Max_Order_By>;
  min?: InputMaybe<InstallationSession_Min_Order_By>;
};

/** input type for inserting array relation for remote table "installationSession" */
export type InstallationSession_Arr_Rel_Insert_Input = {
  data: Array<InstallationSession_Insert_Input>;
  /** upsert condition */
  on_conflict?: InputMaybe<InstallationSession_On_Conflict>;
};

/** Boolean expression to filter rows from the table "installationSession". All fields are combined with a logical 'AND'. */
export type InstallationSession_Bool_Exp = {
  _and?: InputMaybe<Array<InstallationSession_Bool_Exp>>;
  _not?: InputMaybe<InstallationSession_Bool_Exp>;
  _or?: InputMaybe<Array<InstallationSession_Bool_Exp>>;
  id?: InputMaybe<Uuid_Comparison_Exp>;
  installation?: InputMaybe<Installation_Bool_Exp>;
  installationId?: InputMaybe<Uuid_Comparison_Exp>;
  ipAddress?: InputMaybe<String_Comparison_Exp>;
  pinCode?: InputMaybe<String_Comparison_Exp>;
  validTo?: InputMaybe<Timestamptz_Comparison_Exp>;
};

/** unique or primary key constraints on table "installationSession" */
export enum InstallationSession_Constraint {
  /** unique or primary key constraint */
  InstallationSessionPkey = 'installationSession_pkey'
}

/** input type for inserting data into table "installationSession" */
export type InstallationSession_Insert_Input = {
  id?: InputMaybe<Scalars['uuid']>;
  installation?: InputMaybe<Installation_Obj_Rel_Insert_Input>;
  installationId?: InputMaybe<Scalars['uuid']>;
  ipAddress?: InputMaybe<Scalars['String']>;
  pinCode?: InputMaybe<Scalars['String']>;
  validTo?: InputMaybe<Scalars['timestamptz']>;
};

/** aggregate max on columns */
export type InstallationSession_Max_Fields = {
  __typename?: 'installationSession_max_fields';
  id?: Maybe<Scalars['uuid']>;
  installationId?: Maybe<Scalars['uuid']>;
  ipAddress?: Maybe<Scalars['String']>;
  pinCode?: Maybe<Scalars['String']>;
  validTo?: Maybe<Scalars['timestamptz']>;
};

/** order by max() on columns of table "installationSession" */
export type InstallationSession_Max_Order_By = {
  id?: InputMaybe<Order_By>;
  installationId?: InputMaybe<Order_By>;
  ipAddress?: InputMaybe<Order_By>;
  pinCode?: InputMaybe<Order_By>;
  validTo?: InputMaybe<Order_By>;
};

/** aggregate min on columns */
export type InstallationSession_Min_Fields = {
  __typename?: 'installationSession_min_fields';
  id?: Maybe<Scalars['uuid']>;
  installationId?: Maybe<Scalars['uuid']>;
  ipAddress?: Maybe<Scalars['String']>;
  pinCode?: Maybe<Scalars['String']>;
  validTo?: Maybe<Scalars['timestamptz']>;
};

/** order by min() on columns of table "installationSession" */
export type InstallationSession_Min_Order_By = {
  id?: InputMaybe<Order_By>;
  installationId?: InputMaybe<Order_By>;
  ipAddress?: InputMaybe<Order_By>;
  pinCode?: InputMaybe<Order_By>;
  validTo?: InputMaybe<Order_By>;
};

/** response of any mutation on the table "installationSession" */
export type InstallationSession_Mutation_Response = {
  __typename?: 'installationSession_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int'];
  /** data from the rows affected by the mutation */
  returning: Array<InstallationSession>;
};

/** on_conflict condition type for table "installationSession" */
export type InstallationSession_On_Conflict = {
  constraint: InstallationSession_Constraint;
  update_columns?: Array<InstallationSession_Update_Column>;
  where?: InputMaybe<InstallationSession_Bool_Exp>;
};

/** Ordering options when selecting data from "installationSession". */
export type InstallationSession_Order_By = {
  id?: InputMaybe<Order_By>;
  installation?: InputMaybe<Installation_Order_By>;
  installationId?: InputMaybe<Order_By>;
  ipAddress?: InputMaybe<Order_By>;
  pinCode?: InputMaybe<Order_By>;
  validTo?: InputMaybe<Order_By>;
};

/** primary key columns input for table: installationSession */
export type InstallationSession_Pk_Columns_Input = {
  id: Scalars['uuid'];
};

/** select columns of table "installationSession" */
export enum InstallationSession_Select_Column {
  /** column name */
  Id = 'id',
  /** column name */
  InstallationId = 'installationId',
  /** column name */
  IpAddress = 'ipAddress',
  /** column name */
  PinCode = 'pinCode',
  /** column name */
  ValidTo = 'validTo'
}

/** input type for updating data in table "installationSession" */
export type InstallationSession_Set_Input = {
  id?: InputMaybe<Scalars['uuid']>;
  installationId?: InputMaybe<Scalars['uuid']>;
  ipAddress?: InputMaybe<Scalars['String']>;
  pinCode?: InputMaybe<Scalars['String']>;
  validTo?: InputMaybe<Scalars['timestamptz']>;
};

/** update columns of table "installationSession" */
export enum InstallationSession_Update_Column {
  /** column name */
  Id = 'id',
  /** column name */
  InstallationId = 'installationId',
  /** column name */
  IpAddress = 'ipAddress',
  /** column name */
  PinCode = 'pinCode',
  /** column name */
  ValidTo = 'validTo'
}

/** columns and relationships of "installationState" */
export type InstallationState = {
  __typename?: 'installationState';
  title: Scalars['String'];
  value: Scalars['String'];
};

/** aggregated selection of "installationState" */
export type InstallationState_Aggregate = {
  __typename?: 'installationState_aggregate';
  aggregate?: Maybe<InstallationState_Aggregate_Fields>;
  nodes: Array<InstallationState>;
};

/** aggregate fields of "installationState" */
export type InstallationState_Aggregate_Fields = {
  __typename?: 'installationState_aggregate_fields';
  count: Scalars['Int'];
  max?: Maybe<InstallationState_Max_Fields>;
  min?: Maybe<InstallationState_Min_Fields>;
};


/** aggregate fields of "installationState" */
export type InstallationState_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<InstallationState_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']>;
};

/** Boolean expression to filter rows from the table "installationState". All fields are combined with a logical 'AND'. */
export type InstallationState_Bool_Exp = {
  _and?: InputMaybe<Array<InstallationState_Bool_Exp>>;
  _not?: InputMaybe<InstallationState_Bool_Exp>;
  _or?: InputMaybe<Array<InstallationState_Bool_Exp>>;
  title?: InputMaybe<String_Comparison_Exp>;
  value?: InputMaybe<String_Comparison_Exp>;
};

/** unique or primary key constraints on table "installationState" */
export enum InstallationState_Constraint {
  /** unique or primary key constraint */
  InstallationStatePkey = 'installationState_pkey'
}

export enum InstallationState_Enum {
  /** Schválená */
  Approved = 'approved',
  /** Přiřazená */
  Assigned = 'assigned',
  /** Zrušená */
  Cancelled = 'cancelled',
  /** Uzavřená */
  Closed = 'closed',
  /** Dokončená */
  Done = 'done',
  /** V realizaci */
  InProgress = 'in_progress',
  /** Kontrolována */
  InReview = 'in_review',
  /** Nová */
  New = 'new',
  /** Připravená */
  Open = 'open',
  /** Zamítnutá */
  Rejected = 'rejected',
  /** Znovu otevřená */
  Reopen = 'reopen'
}

/** Boolean expression to compare columns of type "installationState_enum". All fields are combined with logical 'AND'. */
export type InstallationState_Enum_Comparison_Exp = {
  _eq?: InputMaybe<InstallationState_Enum>;
  _in?: InputMaybe<Array<InstallationState_Enum>>;
  _is_null?: InputMaybe<Scalars['Boolean']>;
  _neq?: InputMaybe<InstallationState_Enum>;
  _nin?: InputMaybe<Array<InstallationState_Enum>>;
};

/** input type for inserting data into table "installationState" */
export type InstallationState_Insert_Input = {
  title?: InputMaybe<Scalars['String']>;
  value?: InputMaybe<Scalars['String']>;
};

/** aggregate max on columns */
export type InstallationState_Max_Fields = {
  __typename?: 'installationState_max_fields';
  title?: Maybe<Scalars['String']>;
  value?: Maybe<Scalars['String']>;
};

/** aggregate min on columns */
export type InstallationState_Min_Fields = {
  __typename?: 'installationState_min_fields';
  title?: Maybe<Scalars['String']>;
  value?: Maybe<Scalars['String']>;
};

/** response of any mutation on the table "installationState" */
export type InstallationState_Mutation_Response = {
  __typename?: 'installationState_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int'];
  /** data from the rows affected by the mutation */
  returning: Array<InstallationState>;
};

/** input type for inserting object relation for remote table "installationState" */
export type InstallationState_Obj_Rel_Insert_Input = {
  data: InstallationState_Insert_Input;
  /** upsert condition */
  on_conflict?: InputMaybe<InstallationState_On_Conflict>;
};

/** on_conflict condition type for table "installationState" */
export type InstallationState_On_Conflict = {
  constraint: InstallationState_Constraint;
  update_columns?: Array<InstallationState_Update_Column>;
  where?: InputMaybe<InstallationState_Bool_Exp>;
};

/** Ordering options when selecting data from "installationState". */
export type InstallationState_Order_By = {
  title?: InputMaybe<Order_By>;
  value?: InputMaybe<Order_By>;
};

/** primary key columns input for table: installationState */
export type InstallationState_Pk_Columns_Input = {
  value: Scalars['String'];
};

/** select columns of table "installationState" */
export enum InstallationState_Select_Column {
  /** column name */
  Title = 'title',
  /** column name */
  Value = 'value'
}

/** input type for updating data in table "installationState" */
export type InstallationState_Set_Input = {
  title?: InputMaybe<Scalars['String']>;
  value?: InputMaybe<Scalars['String']>;
};

/** update columns of table "installationState" */
export enum InstallationState_Update_Column {
  /** column name */
  Title = 'title',
  /** column name */
  Value = 'value'
}

/** aggregated selection of "installation" */
export type Installation_Aggregate = {
  __typename?: 'installation_aggregate';
  aggregate?: Maybe<Installation_Aggregate_Fields>;
  nodes: Array<Installation>;
};

/** aggregate fields of "installation" */
export type Installation_Aggregate_Fields = {
  __typename?: 'installation_aggregate_fields';
  count: Scalars['Int'];
  max?: Maybe<Installation_Max_Fields>;
  min?: Maybe<Installation_Min_Fields>;
};


/** aggregate fields of "installation" */
export type Installation_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<Installation_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "installation" */
export type Installation_Aggregate_Order_By = {
  count?: InputMaybe<Order_By>;
  max?: InputMaybe<Installation_Max_Order_By>;
  min?: InputMaybe<Installation_Min_Order_By>;
};

/** append existing jsonb value of filtered columns with new jsonb value */
export type Installation_Append_Input = {
  customer?: InputMaybe<Scalars['jsonb']>;
  deliveries?: InputMaybe<Scalars['jsonb']>;
  items?: InputMaybe<Scalars['jsonb']>;
  selectedDateTime?: InputMaybe<Scalars['jsonb']>;
  shippingAddress?: InputMaybe<Scalars['jsonb']>;
};

/** input type for inserting array relation for remote table "installation" */
export type Installation_Arr_Rel_Insert_Input = {
  data: Array<Installation_Insert_Input>;
  /** upsert condition */
  on_conflict?: InputMaybe<Installation_On_Conflict>;
};

/** Boolean expression to filter rows from the table "installation". All fields are combined with a logical 'AND'. */
export type Installation_Bool_Exp = {
  _and?: InputMaybe<Array<Installation_Bool_Exp>>;
  _not?: InputMaybe<Installation_Bool_Exp>;
  _or?: InputMaybe<Array<Installation_Bool_Exp>>;
  availabilities?: InputMaybe<Availability_Bool_Exp>;
  commentEntities?: InputMaybe<CommentEntity_Bool_Exp>;
  createdAt?: InputMaybe<Timestamptz_Comparison_Exp>;
  customer?: InputMaybe<Jsonb_Comparison_Exp>;
  date?: InputMaybe<Timestamptz_Comparison_Exp>;
  deliveries?: InputMaybe<Jsonb_Comparison_Exp>;
  eshop?: InputMaybe<Eshop_Bool_Exp>;
  eshopId?: InputMaybe<Uuid_Comparison_Exp>;
  id?: InputMaybe<Uuid_Comparison_Exp>;
  installationSessions?: InputMaybe<InstallationSession_Bool_Exp>;
  installationState?: InputMaybe<InstallationState_Bool_Exp>;
  items?: InputMaybe<Jsonb_Comparison_Exp>;
  note?: InputMaybe<String_Comparison_Exp>;
  orderId?: InputMaybe<String_Comparison_Exp>;
  pinCode?: InputMaybe<String_Comparison_Exp>;
  selectedDateTime?: InputMaybe<Jsonb_Comparison_Exp>;
  servicemanId?: InputMaybe<Uuid_Comparison_Exp>;
  shippingAddress?: InputMaybe<Jsonb_Comparison_Exp>;
  stateValue?: InputMaybe<InstallationState_Enum_Comparison_Exp>;
  updatedAt?: InputMaybe<Timestamptz_Comparison_Exp>;
  user?: InputMaybe<User_Bool_Exp>;
};

/** unique or primary key constraints on table "installation" */
export enum Installation_Constraint {
  /** unique or primary key constraint */
  InstallationsPkey = 'installations_pkey'
}

/** delete the field or element with specified path (for JSON arrays, negative integers count from the end) */
export type Installation_Delete_At_Path_Input = {
  customer?: InputMaybe<Array<Scalars['String']>>;
  deliveries?: InputMaybe<Array<Scalars['String']>>;
  items?: InputMaybe<Array<Scalars['String']>>;
  selectedDateTime?: InputMaybe<Array<Scalars['String']>>;
  shippingAddress?: InputMaybe<Array<Scalars['String']>>;
};

/** delete the array element with specified index (negative integers count from the end). throws an error if top level container is not an array */
export type Installation_Delete_Elem_Input = {
  customer?: InputMaybe<Scalars['Int']>;
  deliveries?: InputMaybe<Scalars['Int']>;
  items?: InputMaybe<Scalars['Int']>;
  selectedDateTime?: InputMaybe<Scalars['Int']>;
  shippingAddress?: InputMaybe<Scalars['Int']>;
};

/** delete key/value pair or string element. key/value pairs are matched based on their key value */
export type Installation_Delete_Key_Input = {
  customer?: InputMaybe<Scalars['String']>;
  deliveries?: InputMaybe<Scalars['String']>;
  items?: InputMaybe<Scalars['String']>;
  selectedDateTime?: InputMaybe<Scalars['String']>;
  shippingAddress?: InputMaybe<Scalars['String']>;
};

/** input type for inserting data into table "installation" */
export type Installation_Insert_Input = {
  availabilities?: InputMaybe<Availability_Arr_Rel_Insert_Input>;
  commentEntities?: InputMaybe<CommentEntity_Arr_Rel_Insert_Input>;
  createdAt?: InputMaybe<Scalars['timestamptz']>;
  customer?: InputMaybe<Scalars['jsonb']>;
  date?: InputMaybe<Scalars['timestamptz']>;
  deliveries?: InputMaybe<Scalars['jsonb']>;
  eshop?: InputMaybe<Eshop_Obj_Rel_Insert_Input>;
  eshopId?: InputMaybe<Scalars['uuid']>;
  id?: InputMaybe<Scalars['uuid']>;
  installationSessions?: InputMaybe<InstallationSession_Arr_Rel_Insert_Input>;
  installationState?: InputMaybe<InstallationState_Obj_Rel_Insert_Input>;
  items?: InputMaybe<Scalars['jsonb']>;
  note?: InputMaybe<Scalars['String']>;
  orderId?: InputMaybe<Scalars['String']>;
  pinCode?: InputMaybe<Scalars['String']>;
  selectedDateTime?: InputMaybe<Scalars['jsonb']>;
  servicemanId?: InputMaybe<Scalars['uuid']>;
  shippingAddress?: InputMaybe<Scalars['jsonb']>;
  stateValue?: InputMaybe<InstallationState_Enum>;
  updatedAt?: InputMaybe<Scalars['timestamptz']>;
  user?: InputMaybe<User_Obj_Rel_Insert_Input>;
};

/** aggregate max on columns */
export type Installation_Max_Fields = {
  __typename?: 'installation_max_fields';
  createdAt?: Maybe<Scalars['timestamptz']>;
  date?: Maybe<Scalars['timestamptz']>;
  eshopId?: Maybe<Scalars['uuid']>;
  id?: Maybe<Scalars['uuid']>;
  note?: Maybe<Scalars['String']>;
  orderId?: Maybe<Scalars['String']>;
  pinCode?: Maybe<Scalars['String']>;
  servicemanId?: Maybe<Scalars['uuid']>;
  updatedAt?: Maybe<Scalars['timestamptz']>;
};

/** order by max() on columns of table "installation" */
export type Installation_Max_Order_By = {
  createdAt?: InputMaybe<Order_By>;
  date?: InputMaybe<Order_By>;
  eshopId?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  note?: InputMaybe<Order_By>;
  orderId?: InputMaybe<Order_By>;
  pinCode?: InputMaybe<Order_By>;
  servicemanId?: InputMaybe<Order_By>;
  updatedAt?: InputMaybe<Order_By>;
};

/** aggregate min on columns */
export type Installation_Min_Fields = {
  __typename?: 'installation_min_fields';
  createdAt?: Maybe<Scalars['timestamptz']>;
  date?: Maybe<Scalars['timestamptz']>;
  eshopId?: Maybe<Scalars['uuid']>;
  id?: Maybe<Scalars['uuid']>;
  note?: Maybe<Scalars['String']>;
  orderId?: Maybe<Scalars['String']>;
  pinCode?: Maybe<Scalars['String']>;
  servicemanId?: Maybe<Scalars['uuid']>;
  updatedAt?: Maybe<Scalars['timestamptz']>;
};

/** order by min() on columns of table "installation" */
export type Installation_Min_Order_By = {
  createdAt?: InputMaybe<Order_By>;
  date?: InputMaybe<Order_By>;
  eshopId?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  note?: InputMaybe<Order_By>;
  orderId?: InputMaybe<Order_By>;
  pinCode?: InputMaybe<Order_By>;
  servicemanId?: InputMaybe<Order_By>;
  updatedAt?: InputMaybe<Order_By>;
};

/** response of any mutation on the table "installation" */
export type Installation_Mutation_Response = {
  __typename?: 'installation_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int'];
  /** data from the rows affected by the mutation */
  returning: Array<Installation>;
};

/** input type for inserting object relation for remote table "installation" */
export type Installation_Obj_Rel_Insert_Input = {
  data: Installation_Insert_Input;
  /** upsert condition */
  on_conflict?: InputMaybe<Installation_On_Conflict>;
};

/** on_conflict condition type for table "installation" */
export type Installation_On_Conflict = {
  constraint: Installation_Constraint;
  update_columns?: Array<Installation_Update_Column>;
  where?: InputMaybe<Installation_Bool_Exp>;
};

/** Ordering options when selecting data from "installation". */
export type Installation_Order_By = {
  availabilities_aggregate?: InputMaybe<Availability_Aggregate_Order_By>;
  commentEntities_aggregate?: InputMaybe<CommentEntity_Aggregate_Order_By>;
  createdAt?: InputMaybe<Order_By>;
  customer?: InputMaybe<Order_By>;
  date?: InputMaybe<Order_By>;
  deliveries?: InputMaybe<Order_By>;
  eshop?: InputMaybe<Eshop_Order_By>;
  eshopId?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  installationSessions_aggregate?: InputMaybe<InstallationSession_Aggregate_Order_By>;
  installationState?: InputMaybe<InstallationState_Order_By>;
  items?: InputMaybe<Order_By>;
  note?: InputMaybe<Order_By>;
  orderId?: InputMaybe<Order_By>;
  pinCode?: InputMaybe<Order_By>;
  selectedDateTime?: InputMaybe<Order_By>;
  servicemanId?: InputMaybe<Order_By>;
  shippingAddress?: InputMaybe<Order_By>;
  stateValue?: InputMaybe<Order_By>;
  updatedAt?: InputMaybe<Order_By>;
  user?: InputMaybe<User_Order_By>;
};

/** primary key columns input for table: installation */
export type Installation_Pk_Columns_Input = {
  id: Scalars['uuid'];
};

/** prepend existing jsonb value of filtered columns with new jsonb value */
export type Installation_Prepend_Input = {
  customer?: InputMaybe<Scalars['jsonb']>;
  deliveries?: InputMaybe<Scalars['jsonb']>;
  items?: InputMaybe<Scalars['jsonb']>;
  selectedDateTime?: InputMaybe<Scalars['jsonb']>;
  shippingAddress?: InputMaybe<Scalars['jsonb']>;
};

/** select columns of table "installation" */
export enum Installation_Select_Column {
  /** column name */
  CreatedAt = 'createdAt',
  /** column name */
  Customer = 'customer',
  /** column name */
  Date = 'date',
  /** column name */
  Deliveries = 'deliveries',
  /** column name */
  EshopId = 'eshopId',
  /** column name */
  Id = 'id',
  /** column name */
  Items = 'items',
  /** column name */
  Note = 'note',
  /** column name */
  OrderId = 'orderId',
  /** column name */
  PinCode = 'pinCode',
  /** column name */
  SelectedDateTime = 'selectedDateTime',
  /** column name */
  ServicemanId = 'servicemanId',
  /** column name */
  ShippingAddress = 'shippingAddress',
  /** column name */
  StateValue = 'stateValue',
  /** column name */
  UpdatedAt = 'updatedAt'
}

/** input type for updating data in table "installation" */
export type Installation_Set_Input = {
  createdAt?: InputMaybe<Scalars['timestamptz']>;
  customer?: InputMaybe<Scalars['jsonb']>;
  date?: InputMaybe<Scalars['timestamptz']>;
  deliveries?: InputMaybe<Scalars['jsonb']>;
  eshopId?: InputMaybe<Scalars['uuid']>;
  id?: InputMaybe<Scalars['uuid']>;
  items?: InputMaybe<Scalars['jsonb']>;
  note?: InputMaybe<Scalars['String']>;
  orderId?: InputMaybe<Scalars['String']>;
  pinCode?: InputMaybe<Scalars['String']>;
  selectedDateTime?: InputMaybe<Scalars['jsonb']>;
  servicemanId?: InputMaybe<Scalars['uuid']>;
  shippingAddress?: InputMaybe<Scalars['jsonb']>;
  stateValue?: InputMaybe<InstallationState_Enum>;
  updatedAt?: InputMaybe<Scalars['timestamptz']>;
};

/** update columns of table "installation" */
export enum Installation_Update_Column {
  /** column name */
  CreatedAt = 'createdAt',
  /** column name */
  Customer = 'customer',
  /** column name */
  Date = 'date',
  /** column name */
  Deliveries = 'deliveries',
  /** column name */
  EshopId = 'eshopId',
  /** column name */
  Id = 'id',
  /** column name */
  Items = 'items',
  /** column name */
  Note = 'note',
  /** column name */
  OrderId = 'orderId',
  /** column name */
  PinCode = 'pinCode',
  /** column name */
  SelectedDateTime = 'selectedDateTime',
  /** column name */
  ServicemanId = 'servicemanId',
  /** column name */
  ShippingAddress = 'shippingAddress',
  /** column name */
  StateValue = 'stateValue',
  /** column name */
  UpdatedAt = 'updatedAt'
}

export type Jsonb_Cast_Exp = {
  String?: InputMaybe<String_Comparison_Exp>;
};

/** Boolean expression to compare columns of type "jsonb". All fields are combined with logical 'AND'. */
export type Jsonb_Comparison_Exp = {
  _cast?: InputMaybe<Jsonb_Cast_Exp>;
  /** is the column contained in the given json value */
  _contained_in?: InputMaybe<Scalars['jsonb']>;
  /** does the column contain the given json value at the top level */
  _contains?: InputMaybe<Scalars['jsonb']>;
  _eq?: InputMaybe<Scalars['jsonb']>;
  _gt?: InputMaybe<Scalars['jsonb']>;
  _gte?: InputMaybe<Scalars['jsonb']>;
  /** does the string exist as a top-level key in the column */
  _has_key?: InputMaybe<Scalars['String']>;
  /** do all of these strings exist as top-level keys in the column */
  _has_keys_all?: InputMaybe<Array<Scalars['String']>>;
  /** do any of these strings exist as top-level keys in the column */
  _has_keys_any?: InputMaybe<Array<Scalars['String']>>;
  _in?: InputMaybe<Array<Scalars['jsonb']>>;
  _is_null?: InputMaybe<Scalars['Boolean']>;
  _lt?: InputMaybe<Scalars['jsonb']>;
  _lte?: InputMaybe<Scalars['jsonb']>;
  _neq?: InputMaybe<Scalars['jsonb']>;
  _nin?: InputMaybe<Array<Scalars['jsonb']>>;
};

/** mutation root */
export type Mutation_Root = {
  __typename?: 'mutation_root';
  /** delete data from the table: "availability" */
  delete_availability?: Maybe<Availability_Mutation_Response>;
  /** delete data from the table: "availabilityZipCodes" */
  delete_availabilityZipCodes?: Maybe<AvailabilityZipCodes_Mutation_Response>;
  /** delete single row from the table: "availabilityZipCodes" */
  delete_availabilityZipCodes_by_pk?: Maybe<AvailabilityZipCodes>;
  /** delete single row from the table: "availability" */
  delete_availability_by_pk?: Maybe<Availability>;
  /** delete data from the table: "comment" */
  delete_comment?: Maybe<Comment_Mutation_Response>;
  /** delete data from the table: "commentEntity" */
  delete_commentEntity?: Maybe<CommentEntity_Mutation_Response>;
  /** delete single row from the table: "commentEntity" */
  delete_commentEntity_by_pk?: Maybe<CommentEntity>;
  /** delete single row from the table: "comment" */
  delete_comment_by_pk?: Maybe<Comment>;
  /** delete data from the table: "eshop" */
  delete_eshop?: Maybe<Eshop_Mutation_Response>;
  /** delete single row from the table: "eshop" */
  delete_eshop_by_pk?: Maybe<Eshop>;
  /** delete data from the table: "installation" */
  delete_installation?: Maybe<Installation_Mutation_Response>;
  /** delete data from the table: "installationSession" */
  delete_installationSession?: Maybe<InstallationSession_Mutation_Response>;
  /** delete single row from the table: "installationSession" */
  delete_installationSession_by_pk?: Maybe<InstallationSession>;
  /** delete data from the table: "installationState" */
  delete_installationState?: Maybe<InstallationState_Mutation_Response>;
  /** delete single row from the table: "installationState" */
  delete_installationState_by_pk?: Maybe<InstallationState>;
  /** delete single row from the table: "installation" */
  delete_installation_by_pk?: Maybe<Installation>;
  /** delete data from the table: "pinSafeChars" */
  delete_pinSafeChars?: Maybe<PinSafeChars_Mutation_Response>;
  /** delete single row from the table: "pinSafeChars" */
  delete_pinSafeChars_by_pk?: Maybe<PinSafeChars>;
  /** delete data from the table: "role" */
  delete_role?: Maybe<Role_Mutation_Response>;
  /** delete single row from the table: "role" */
  delete_role_by_pk?: Maybe<Role>;
  /** delete data from the table: "servicePrice" */
  delete_servicePrice?: Maybe<ServicePrice_Mutation_Response>;
  /** delete single row from the table: "servicePrice" */
  delete_servicePrice_by_pk?: Maybe<ServicePrice>;
  /** delete data from the table: "serviceType" */
  delete_serviceType?: Maybe<ServiceType_Mutation_Response>;
  /** delete single row from the table: "serviceType" */
  delete_serviceType_by_pk?: Maybe<ServiceType>;
  /** delete data from the table: "servicedCategory" */
  delete_servicedCategory?: Maybe<ServicedCategory_Mutation_Response>;
  /** delete single row from the table: "servicedCategory" */
  delete_servicedCategory_by_pk?: Maybe<ServicedCategory>;
  /** delete data from the table: "servicedProduct" */
  delete_servicedProduct?: Maybe<ServicedProduct_Mutation_Response>;
  /** delete single row from the table: "servicedProduct" */
  delete_servicedProduct_by_pk?: Maybe<ServicedProduct>;
  /** delete data from the table: "user" */
  delete_user?: Maybe<User_Mutation_Response>;
  /** delete data from the table: "userServiceTypes" */
  delete_userServiceTypes?: Maybe<UserServiceTypes_Mutation_Response>;
  /** delete single row from the table: "userServiceTypes" */
  delete_userServiceTypes_by_pk?: Maybe<UserServiceTypes>;
  /** delete data from the table: "userZipCodes" */
  delete_userZipCodes?: Maybe<UserZipCodes_Mutation_Response>;
  /** delete single row from the table: "userZipCodes" */
  delete_userZipCodes_by_pk?: Maybe<UserZipCodes>;
  /** delete single row from the table: "user" */
  delete_user_by_pk?: Maybe<User>;
  /** delete data from the table: "zipCodes" */
  delete_zipCodes?: Maybe<ZipCodes_Mutation_Response>;
  /** delete single row from the table: "zipCodes" */
  delete_zipCodes_by_pk?: Maybe<ZipCodes>;
  /** insert data into the table: "availability" */
  insert_availability?: Maybe<Availability_Mutation_Response>;
  /** insert data into the table: "availabilityZipCodes" */
  insert_availabilityZipCodes?: Maybe<AvailabilityZipCodes_Mutation_Response>;
  /** insert a single row into the table: "availabilityZipCodes" */
  insert_availabilityZipCodes_one?: Maybe<AvailabilityZipCodes>;
  /** insert a single row into the table: "availability" */
  insert_availability_one?: Maybe<Availability>;
  /** insert data into the table: "comment" */
  insert_comment?: Maybe<Comment_Mutation_Response>;
  /** insert data into the table: "commentEntity" */
  insert_commentEntity?: Maybe<CommentEntity_Mutation_Response>;
  /** insert a single row into the table: "commentEntity" */
  insert_commentEntity_one?: Maybe<CommentEntity>;
  /** insert a single row into the table: "comment" */
  insert_comment_one?: Maybe<Comment>;
  /** insert data into the table: "eshop" */
  insert_eshop?: Maybe<Eshop_Mutation_Response>;
  /** insert a single row into the table: "eshop" */
  insert_eshop_one?: Maybe<Eshop>;
  /** insert data into the table: "installation" */
  insert_installation?: Maybe<Installation_Mutation_Response>;
  /** insert data into the table: "installationSession" */
  insert_installationSession?: Maybe<InstallationSession_Mutation_Response>;
  /** insert a single row into the table: "installationSession" */
  insert_installationSession_one?: Maybe<InstallationSession>;
  /** insert data into the table: "installationState" */
  insert_installationState?: Maybe<InstallationState_Mutation_Response>;
  /** insert a single row into the table: "installationState" */
  insert_installationState_one?: Maybe<InstallationState>;
  /** insert a single row into the table: "installation" */
  insert_installation_one?: Maybe<Installation>;
  /** insert data into the table: "pinSafeChars" */
  insert_pinSafeChars?: Maybe<PinSafeChars_Mutation_Response>;
  /** insert a single row into the table: "pinSafeChars" */
  insert_pinSafeChars_one?: Maybe<PinSafeChars>;
  /** insert data into the table: "role" */
  insert_role?: Maybe<Role_Mutation_Response>;
  /** insert a single row into the table: "role" */
  insert_role_one?: Maybe<Role>;
  /** insert data into the table: "servicePrice" */
  insert_servicePrice?: Maybe<ServicePrice_Mutation_Response>;
  /** insert a single row into the table: "servicePrice" */
  insert_servicePrice_one?: Maybe<ServicePrice>;
  /** insert data into the table: "serviceType" */
  insert_serviceType?: Maybe<ServiceType_Mutation_Response>;
  /** insert a single row into the table: "serviceType" */
  insert_serviceType_one?: Maybe<ServiceType>;
  /** insert data into the table: "servicedCategory" */
  insert_servicedCategory?: Maybe<ServicedCategory_Mutation_Response>;
  /** insert a single row into the table: "servicedCategory" */
  insert_servicedCategory_one?: Maybe<ServicedCategory>;
  /** insert data into the table: "servicedProduct" */
  insert_servicedProduct?: Maybe<ServicedProduct_Mutation_Response>;
  /** insert a single row into the table: "servicedProduct" */
  insert_servicedProduct_one?: Maybe<ServicedProduct>;
  /** insert data into the table: "user" */
  insert_user?: Maybe<User_Mutation_Response>;
  /** insert data into the table: "userServiceTypes" */
  insert_userServiceTypes?: Maybe<UserServiceTypes_Mutation_Response>;
  /** insert a single row into the table: "userServiceTypes" */
  insert_userServiceTypes_one?: Maybe<UserServiceTypes>;
  /** insert data into the table: "userZipCodes" */
  insert_userZipCodes?: Maybe<UserZipCodes_Mutation_Response>;
  /** insert a single row into the table: "userZipCodes" */
  insert_userZipCodes_one?: Maybe<UserZipCodes>;
  /** insert a single row into the table: "user" */
  insert_user_one?: Maybe<User>;
  /** insert data into the table: "zipCodes" */
  insert_zipCodes?: Maybe<ZipCodes_Mutation_Response>;
  /** insert a single row into the table: "zipCodes" */
  insert_zipCodes_one?: Maybe<ZipCodes>;
  /** update data of the table: "availability" */
  update_availability?: Maybe<Availability_Mutation_Response>;
  /** update data of the table: "availabilityZipCodes" */
  update_availabilityZipCodes?: Maybe<AvailabilityZipCodes_Mutation_Response>;
  /** update single row of the table: "availabilityZipCodes" */
  update_availabilityZipCodes_by_pk?: Maybe<AvailabilityZipCodes>;
  /** update single row of the table: "availability" */
  update_availability_by_pk?: Maybe<Availability>;
  /** update data of the table: "comment" */
  update_comment?: Maybe<Comment_Mutation_Response>;
  /** update data of the table: "commentEntity" */
  update_commentEntity?: Maybe<CommentEntity_Mutation_Response>;
  /** update single row of the table: "commentEntity" */
  update_commentEntity_by_pk?: Maybe<CommentEntity>;
  /** update single row of the table: "comment" */
  update_comment_by_pk?: Maybe<Comment>;
  /** update data of the table: "eshop" */
  update_eshop?: Maybe<Eshop_Mutation_Response>;
  /** update single row of the table: "eshop" */
  update_eshop_by_pk?: Maybe<Eshop>;
  /** update data of the table: "installation" */
  update_installation?: Maybe<Installation_Mutation_Response>;
  /** update data of the table: "installationSession" */
  update_installationSession?: Maybe<InstallationSession_Mutation_Response>;
  /** update single row of the table: "installationSession" */
  update_installationSession_by_pk?: Maybe<InstallationSession>;
  /** update data of the table: "installationState" */
  update_installationState?: Maybe<InstallationState_Mutation_Response>;
  /** update single row of the table: "installationState" */
  update_installationState_by_pk?: Maybe<InstallationState>;
  /** update single row of the table: "installation" */
  update_installation_by_pk?: Maybe<Installation>;
  /** update data of the table: "pinSafeChars" */
  update_pinSafeChars?: Maybe<PinSafeChars_Mutation_Response>;
  /** update single row of the table: "pinSafeChars" */
  update_pinSafeChars_by_pk?: Maybe<PinSafeChars>;
  /** update data of the table: "role" */
  update_role?: Maybe<Role_Mutation_Response>;
  /** update single row of the table: "role" */
  update_role_by_pk?: Maybe<Role>;
  /** update data of the table: "servicePrice" */
  update_servicePrice?: Maybe<ServicePrice_Mutation_Response>;
  /** update single row of the table: "servicePrice" */
  update_servicePrice_by_pk?: Maybe<ServicePrice>;
  /** update data of the table: "serviceType" */
  update_serviceType?: Maybe<ServiceType_Mutation_Response>;
  /** update single row of the table: "serviceType" */
  update_serviceType_by_pk?: Maybe<ServiceType>;
  /** update data of the table: "servicedCategory" */
  update_servicedCategory?: Maybe<ServicedCategory_Mutation_Response>;
  /** update single row of the table: "servicedCategory" */
  update_servicedCategory_by_pk?: Maybe<ServicedCategory>;
  /** update data of the table: "servicedProduct" */
  update_servicedProduct?: Maybe<ServicedProduct_Mutation_Response>;
  /** update single row of the table: "servicedProduct" */
  update_servicedProduct_by_pk?: Maybe<ServicedProduct>;
  /** update data of the table: "user" */
  update_user?: Maybe<User_Mutation_Response>;
  /** update data of the table: "userServiceTypes" */
  update_userServiceTypes?: Maybe<UserServiceTypes_Mutation_Response>;
  /** update single row of the table: "userServiceTypes" */
  update_userServiceTypes_by_pk?: Maybe<UserServiceTypes>;
  /** update data of the table: "userZipCodes" */
  update_userZipCodes?: Maybe<UserZipCodes_Mutation_Response>;
  /** update single row of the table: "userZipCodes" */
  update_userZipCodes_by_pk?: Maybe<UserZipCodes>;
  /** update single row of the table: "user" */
  update_user_by_pk?: Maybe<User>;
  /** update data of the table: "zipCodes" */
  update_zipCodes?: Maybe<ZipCodes_Mutation_Response>;
  /** update single row of the table: "zipCodes" */
  update_zipCodes_by_pk?: Maybe<ZipCodes>;
};


/** mutation root */
export type Mutation_RootDelete_AvailabilityArgs = {
  where: Availability_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_AvailabilityZipCodesArgs = {
  where: AvailabilityZipCodes_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_AvailabilityZipCodes_By_PkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type Mutation_RootDelete_Availability_By_PkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type Mutation_RootDelete_CommentArgs = {
  where: Comment_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_CommentEntityArgs = {
  where: CommentEntity_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_CommentEntity_By_PkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type Mutation_RootDelete_Comment_By_PkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type Mutation_RootDelete_EshopArgs = {
  where: Eshop_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Eshop_By_PkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type Mutation_RootDelete_InstallationArgs = {
  where: Installation_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_InstallationSessionArgs = {
  where: InstallationSession_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_InstallationSession_By_PkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type Mutation_RootDelete_InstallationStateArgs = {
  where: InstallationState_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_InstallationState_By_PkArgs = {
  value: Scalars['String'];
};


/** mutation root */
export type Mutation_RootDelete_Installation_By_PkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type Mutation_RootDelete_PinSafeCharsArgs = {
  where: PinSafeChars_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_PinSafeChars_By_PkArgs = {
  character: Scalars['bpchar'];
};


/** mutation root */
export type Mutation_RootDelete_RoleArgs = {
  where: Role_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Role_By_PkArgs = {
  value: Scalars['String'];
};


/** mutation root */
export type Mutation_RootDelete_ServicePriceArgs = {
  where: ServicePrice_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_ServicePrice_By_PkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type Mutation_RootDelete_ServiceTypeArgs = {
  where: ServiceType_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_ServiceType_By_PkArgs = {
  value: Scalars['String'];
};


/** mutation root */
export type Mutation_RootDelete_ServicedCategoryArgs = {
  where: ServicedCategory_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_ServicedCategory_By_PkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type Mutation_RootDelete_ServicedProductArgs = {
  where: ServicedProduct_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_ServicedProduct_By_PkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type Mutation_RootDelete_UserArgs = {
  where: User_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_UserServiceTypesArgs = {
  where: UserServiceTypes_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_UserServiceTypes_By_PkArgs = {
  serviceTypeValue: ServiceType_Enum;
  userId: Scalars['uuid'];
};


/** mutation root */
export type Mutation_RootDelete_UserZipCodesArgs = {
  where: UserZipCodes_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_UserZipCodes_By_PkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type Mutation_RootDelete_User_By_PkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type Mutation_RootDelete_ZipCodesArgs = {
  where: ZipCodes_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_ZipCodes_By_PkArgs = {
  id: Scalars['String'];
};


/** mutation root */
export type Mutation_RootInsert_AvailabilityArgs = {
  objects: Array<Availability_Insert_Input>;
  on_conflict?: InputMaybe<Availability_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_AvailabilityZipCodesArgs = {
  objects: Array<AvailabilityZipCodes_Insert_Input>;
  on_conflict?: InputMaybe<AvailabilityZipCodes_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_AvailabilityZipCodes_OneArgs = {
  object: AvailabilityZipCodes_Insert_Input;
  on_conflict?: InputMaybe<AvailabilityZipCodes_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Availability_OneArgs = {
  object: Availability_Insert_Input;
  on_conflict?: InputMaybe<Availability_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_CommentArgs = {
  objects: Array<Comment_Insert_Input>;
  on_conflict?: InputMaybe<Comment_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_CommentEntityArgs = {
  objects: Array<CommentEntity_Insert_Input>;
  on_conflict?: InputMaybe<CommentEntity_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_CommentEntity_OneArgs = {
  object: CommentEntity_Insert_Input;
  on_conflict?: InputMaybe<CommentEntity_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Comment_OneArgs = {
  object: Comment_Insert_Input;
  on_conflict?: InputMaybe<Comment_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_EshopArgs = {
  objects: Array<Eshop_Insert_Input>;
  on_conflict?: InputMaybe<Eshop_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Eshop_OneArgs = {
  object: Eshop_Insert_Input;
  on_conflict?: InputMaybe<Eshop_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_InstallationArgs = {
  objects: Array<Installation_Insert_Input>;
  on_conflict?: InputMaybe<Installation_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_InstallationSessionArgs = {
  objects: Array<InstallationSession_Insert_Input>;
  on_conflict?: InputMaybe<InstallationSession_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_InstallationSession_OneArgs = {
  object: InstallationSession_Insert_Input;
  on_conflict?: InputMaybe<InstallationSession_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_InstallationStateArgs = {
  objects: Array<InstallationState_Insert_Input>;
  on_conflict?: InputMaybe<InstallationState_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_InstallationState_OneArgs = {
  object: InstallationState_Insert_Input;
  on_conflict?: InputMaybe<InstallationState_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Installation_OneArgs = {
  object: Installation_Insert_Input;
  on_conflict?: InputMaybe<Installation_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_PinSafeCharsArgs = {
  objects: Array<PinSafeChars_Insert_Input>;
  on_conflict?: InputMaybe<PinSafeChars_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_PinSafeChars_OneArgs = {
  object: PinSafeChars_Insert_Input;
  on_conflict?: InputMaybe<PinSafeChars_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_RoleArgs = {
  objects: Array<Role_Insert_Input>;
  on_conflict?: InputMaybe<Role_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Role_OneArgs = {
  object: Role_Insert_Input;
  on_conflict?: InputMaybe<Role_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_ServicePriceArgs = {
  objects: Array<ServicePrice_Insert_Input>;
  on_conflict?: InputMaybe<ServicePrice_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_ServicePrice_OneArgs = {
  object: ServicePrice_Insert_Input;
  on_conflict?: InputMaybe<ServicePrice_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_ServiceTypeArgs = {
  objects: Array<ServiceType_Insert_Input>;
  on_conflict?: InputMaybe<ServiceType_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_ServiceType_OneArgs = {
  object: ServiceType_Insert_Input;
  on_conflict?: InputMaybe<ServiceType_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_ServicedCategoryArgs = {
  objects: Array<ServicedCategory_Insert_Input>;
  on_conflict?: InputMaybe<ServicedCategory_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_ServicedCategory_OneArgs = {
  object: ServicedCategory_Insert_Input;
  on_conflict?: InputMaybe<ServicedCategory_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_ServicedProductArgs = {
  objects: Array<ServicedProduct_Insert_Input>;
  on_conflict?: InputMaybe<ServicedProduct_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_ServicedProduct_OneArgs = {
  object: ServicedProduct_Insert_Input;
  on_conflict?: InputMaybe<ServicedProduct_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_UserArgs = {
  objects: Array<User_Insert_Input>;
  on_conflict?: InputMaybe<User_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_UserServiceTypesArgs = {
  objects: Array<UserServiceTypes_Insert_Input>;
  on_conflict?: InputMaybe<UserServiceTypes_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_UserServiceTypes_OneArgs = {
  object: UserServiceTypes_Insert_Input;
  on_conflict?: InputMaybe<UserServiceTypes_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_UserZipCodesArgs = {
  objects: Array<UserZipCodes_Insert_Input>;
  on_conflict?: InputMaybe<UserZipCodes_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_UserZipCodes_OneArgs = {
  object: UserZipCodes_Insert_Input;
  on_conflict?: InputMaybe<UserZipCodes_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_User_OneArgs = {
  object: User_Insert_Input;
  on_conflict?: InputMaybe<User_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_ZipCodesArgs = {
  objects: Array<ZipCodes_Insert_Input>;
  on_conflict?: InputMaybe<ZipCodes_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_ZipCodes_OneArgs = {
  object: ZipCodes_Insert_Input;
  on_conflict?: InputMaybe<ZipCodes_On_Conflict>;
};


/** mutation root */
export type Mutation_RootUpdate_AvailabilityArgs = {
  _set?: InputMaybe<Availability_Set_Input>;
  where: Availability_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_AvailabilityZipCodesArgs = {
  _set?: InputMaybe<AvailabilityZipCodes_Set_Input>;
  where: AvailabilityZipCodes_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_AvailabilityZipCodes_By_PkArgs = {
  _set?: InputMaybe<AvailabilityZipCodes_Set_Input>;
  pk_columns: AvailabilityZipCodes_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_Availability_By_PkArgs = {
  _set?: InputMaybe<Availability_Set_Input>;
  pk_columns: Availability_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_CommentArgs = {
  _set?: InputMaybe<Comment_Set_Input>;
  where: Comment_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_CommentEntityArgs = {
  _set?: InputMaybe<CommentEntity_Set_Input>;
  where: CommentEntity_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_CommentEntity_By_PkArgs = {
  _set?: InputMaybe<CommentEntity_Set_Input>;
  pk_columns: CommentEntity_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_Comment_By_PkArgs = {
  _set?: InputMaybe<Comment_Set_Input>;
  pk_columns: Comment_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_EshopArgs = {
  _set?: InputMaybe<Eshop_Set_Input>;
  where: Eshop_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Eshop_By_PkArgs = {
  _set?: InputMaybe<Eshop_Set_Input>;
  pk_columns: Eshop_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_InstallationArgs = {
  _append?: InputMaybe<Installation_Append_Input>;
  _delete_at_path?: InputMaybe<Installation_Delete_At_Path_Input>;
  _delete_elem?: InputMaybe<Installation_Delete_Elem_Input>;
  _delete_key?: InputMaybe<Installation_Delete_Key_Input>;
  _prepend?: InputMaybe<Installation_Prepend_Input>;
  _set?: InputMaybe<Installation_Set_Input>;
  where: Installation_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_InstallationSessionArgs = {
  _set?: InputMaybe<InstallationSession_Set_Input>;
  where: InstallationSession_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_InstallationSession_By_PkArgs = {
  _set?: InputMaybe<InstallationSession_Set_Input>;
  pk_columns: InstallationSession_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_InstallationStateArgs = {
  _set?: InputMaybe<InstallationState_Set_Input>;
  where: InstallationState_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_InstallationState_By_PkArgs = {
  _set?: InputMaybe<InstallationState_Set_Input>;
  pk_columns: InstallationState_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_Installation_By_PkArgs = {
  _append?: InputMaybe<Installation_Append_Input>;
  _delete_at_path?: InputMaybe<Installation_Delete_At_Path_Input>;
  _delete_elem?: InputMaybe<Installation_Delete_Elem_Input>;
  _delete_key?: InputMaybe<Installation_Delete_Key_Input>;
  _prepend?: InputMaybe<Installation_Prepend_Input>;
  _set?: InputMaybe<Installation_Set_Input>;
  pk_columns: Installation_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_PinSafeCharsArgs = {
  _set?: InputMaybe<PinSafeChars_Set_Input>;
  where: PinSafeChars_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_PinSafeChars_By_PkArgs = {
  _set?: InputMaybe<PinSafeChars_Set_Input>;
  pk_columns: PinSafeChars_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_RoleArgs = {
  _set?: InputMaybe<Role_Set_Input>;
  where: Role_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Role_By_PkArgs = {
  _set?: InputMaybe<Role_Set_Input>;
  pk_columns: Role_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_ServicePriceArgs = {
  _inc?: InputMaybe<ServicePrice_Inc_Input>;
  _set?: InputMaybe<ServicePrice_Set_Input>;
  where: ServicePrice_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_ServicePrice_By_PkArgs = {
  _inc?: InputMaybe<ServicePrice_Inc_Input>;
  _set?: InputMaybe<ServicePrice_Set_Input>;
  pk_columns: ServicePrice_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_ServiceTypeArgs = {
  _set?: InputMaybe<ServiceType_Set_Input>;
  where: ServiceType_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_ServiceType_By_PkArgs = {
  _set?: InputMaybe<ServiceType_Set_Input>;
  pk_columns: ServiceType_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_ServicedCategoryArgs = {
  _set?: InputMaybe<ServicedCategory_Set_Input>;
  where: ServicedCategory_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_ServicedCategory_By_PkArgs = {
  _set?: InputMaybe<ServicedCategory_Set_Input>;
  pk_columns: ServicedCategory_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_ServicedProductArgs = {
  _set?: InputMaybe<ServicedProduct_Set_Input>;
  where: ServicedProduct_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_ServicedProduct_By_PkArgs = {
  _set?: InputMaybe<ServicedProduct_Set_Input>;
  pk_columns: ServicedProduct_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_UserArgs = {
  _set?: InputMaybe<User_Set_Input>;
  where: User_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_UserServiceTypesArgs = {
  _set?: InputMaybe<UserServiceTypes_Set_Input>;
  where: UserServiceTypes_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_UserServiceTypes_By_PkArgs = {
  _set?: InputMaybe<UserServiceTypes_Set_Input>;
  pk_columns: UserServiceTypes_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_UserZipCodesArgs = {
  _set?: InputMaybe<UserZipCodes_Set_Input>;
  where: UserZipCodes_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_UserZipCodes_By_PkArgs = {
  _set?: InputMaybe<UserZipCodes_Set_Input>;
  pk_columns: UserZipCodes_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_User_By_PkArgs = {
  _set?: InputMaybe<User_Set_Input>;
  pk_columns: User_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_ZipCodesArgs = {
  _set?: InputMaybe<ZipCodes_Set_Input>;
  where: ZipCodes_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_ZipCodes_By_PkArgs = {
  _set?: InputMaybe<ZipCodes_Set_Input>;
  pk_columns: ZipCodes_Pk_Columns_Input;
};

/** column ordering options */
export enum Order_By {
  /** in ascending order, nulls last */
  Asc = 'asc',
  /** in ascending order, nulls first */
  AscNullsFirst = 'asc_nulls_first',
  /** in ascending order, nulls last */
  AscNullsLast = 'asc_nulls_last',
  /** in descending order, nulls first */
  Desc = 'desc',
  /** in descending order, nulls first */
  DescNullsFirst = 'desc_nulls_first',
  /** in descending order, nulls last */
  DescNullsLast = 'desc_nulls_last'
}

/** columns and relationships of "pinSafeChars" */
export type PinSafeChars = {
  __typename?: 'pinSafeChars';
  character: Scalars['bpchar'];
};

/** aggregated selection of "pinSafeChars" */
export type PinSafeChars_Aggregate = {
  __typename?: 'pinSafeChars_aggregate';
  aggregate?: Maybe<PinSafeChars_Aggregate_Fields>;
  nodes: Array<PinSafeChars>;
};

/** aggregate fields of "pinSafeChars" */
export type PinSafeChars_Aggregate_Fields = {
  __typename?: 'pinSafeChars_aggregate_fields';
  count: Scalars['Int'];
  max?: Maybe<PinSafeChars_Max_Fields>;
  min?: Maybe<PinSafeChars_Min_Fields>;
};


/** aggregate fields of "pinSafeChars" */
export type PinSafeChars_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<PinSafeChars_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']>;
};

/** Boolean expression to filter rows from the table "pinSafeChars". All fields are combined with a logical 'AND'. */
export type PinSafeChars_Bool_Exp = {
  _and?: InputMaybe<Array<PinSafeChars_Bool_Exp>>;
  _not?: InputMaybe<PinSafeChars_Bool_Exp>;
  _or?: InputMaybe<Array<PinSafeChars_Bool_Exp>>;
  character?: InputMaybe<Bpchar_Comparison_Exp>;
};

/** unique or primary key constraints on table "pinSafeChars" */
export enum PinSafeChars_Constraint {
  /** unique or primary key constraint */
  CharactersPkey = 'characters_pkey'
}

/** input type for inserting data into table "pinSafeChars" */
export type PinSafeChars_Insert_Input = {
  character?: InputMaybe<Scalars['bpchar']>;
};

/** aggregate max on columns */
export type PinSafeChars_Max_Fields = {
  __typename?: 'pinSafeChars_max_fields';
  character?: Maybe<Scalars['bpchar']>;
};

/** aggregate min on columns */
export type PinSafeChars_Min_Fields = {
  __typename?: 'pinSafeChars_min_fields';
  character?: Maybe<Scalars['bpchar']>;
};

/** response of any mutation on the table "pinSafeChars" */
export type PinSafeChars_Mutation_Response = {
  __typename?: 'pinSafeChars_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int'];
  /** data from the rows affected by the mutation */
  returning: Array<PinSafeChars>;
};

/** on_conflict condition type for table "pinSafeChars" */
export type PinSafeChars_On_Conflict = {
  constraint: PinSafeChars_Constraint;
  update_columns?: Array<PinSafeChars_Update_Column>;
  where?: InputMaybe<PinSafeChars_Bool_Exp>;
};

/** Ordering options when selecting data from "pinSafeChars". */
export type PinSafeChars_Order_By = {
  character?: InputMaybe<Order_By>;
};

/** primary key columns input for table: pinSafeChars */
export type PinSafeChars_Pk_Columns_Input = {
  character: Scalars['bpchar'];
};

/** select columns of table "pinSafeChars" */
export enum PinSafeChars_Select_Column {
  /** column name */
  Character = 'character'
}

/** input type for updating data in table "pinSafeChars" */
export type PinSafeChars_Set_Input = {
  character?: InputMaybe<Scalars['bpchar']>;
};

/** update columns of table "pinSafeChars" */
export enum PinSafeChars_Update_Column {
  /** column name */
  Character = 'character'
}

export type Query_Root = {
  __typename?: 'query_root';
  /** fetch data from the table: "availability" */
  availability: Array<Availability>;
  /** An array relationship */
  availabilityZipCodes: Array<AvailabilityZipCodes>;
  /** An aggregate relationship */
  availabilityZipCodes_aggregate: AvailabilityZipCodes_Aggregate;
  /** fetch data from the table: "availabilityZipCodes" using primary key columns */
  availabilityZipCodes_by_pk?: Maybe<AvailabilityZipCodes>;
  /** fetch aggregated fields from the table: "availability" */
  availability_aggregate: Availability_Aggregate;
  /** fetch data from the table: "availability" using primary key columns */
  availability_by_pk?: Maybe<Availability>;
  /** fetch data from the table: "comment" */
  comment: Array<Comment>;
  /** fetch data from the table: "commentEntity" */
  commentEntity: Array<CommentEntity>;
  /** fetch aggregated fields from the table: "commentEntity" */
  commentEntity_aggregate: CommentEntity_Aggregate;
  /** fetch data from the table: "commentEntity" using primary key columns */
  commentEntity_by_pk?: Maybe<CommentEntity>;
  /** fetch aggregated fields from the table: "comment" */
  comment_aggregate: Comment_Aggregate;
  /** fetch data from the table: "comment" using primary key columns */
  comment_by_pk?: Maybe<Comment>;
  /** fetch data from the table: "eshop" */
  eshop: Array<Eshop>;
  /** fetch aggregated fields from the table: "eshop" */
  eshop_aggregate: Eshop_Aggregate;
  /** fetch data from the table: "eshop" using primary key columns */
  eshop_by_pk?: Maybe<Eshop>;
  /** fetch data from the table: "installation" */
  installation: Array<Installation>;
  /** fetch data from the table: "installationSession" */
  installationSession: Array<InstallationSession>;
  /** fetch aggregated fields from the table: "installationSession" */
  installationSession_aggregate: InstallationSession_Aggregate;
  /** fetch data from the table: "installationSession" using primary key columns */
  installationSession_by_pk?: Maybe<InstallationSession>;
  /** fetch data from the table: "installationState" */
  installationState: Array<InstallationState>;
  /** fetch aggregated fields from the table: "installationState" */
  installationState_aggregate: InstallationState_Aggregate;
  /** fetch data from the table: "installationState" using primary key columns */
  installationState_by_pk?: Maybe<InstallationState>;
  /** fetch aggregated fields from the table: "installation" */
  installation_aggregate: Installation_Aggregate;
  /** fetch data from the table: "installation" using primary key columns */
  installation_by_pk?: Maybe<Installation>;
  /** fetch data from the table: "pinSafeChars" */
  pinSafeChars: Array<PinSafeChars>;
  /** fetch aggregated fields from the table: "pinSafeChars" */
  pinSafeChars_aggregate: PinSafeChars_Aggregate;
  /** fetch data from the table: "pinSafeChars" using primary key columns */
  pinSafeChars_by_pk?: Maybe<PinSafeChars>;
  /** fetch data from the table: "role" */
  role: Array<Role>;
  /** fetch aggregated fields from the table: "role" */
  role_aggregate: Role_Aggregate;
  /** fetch data from the table: "role" using primary key columns */
  role_by_pk?: Maybe<Role>;
  /** fetch data from the table: "servicePrice" */
  servicePrice: Array<ServicePrice>;
  /** fetch aggregated fields from the table: "servicePrice" */
  servicePrice_aggregate: ServicePrice_Aggregate;
  /** fetch data from the table: "servicePrice" using primary key columns */
  servicePrice_by_pk?: Maybe<ServicePrice>;
  /** fetch data from the table: "serviceType" */
  serviceType: Array<ServiceType>;
  /** fetch aggregated fields from the table: "serviceType" */
  serviceType_aggregate: ServiceType_Aggregate;
  /** fetch data from the table: "serviceType" using primary key columns */
  serviceType_by_pk?: Maybe<ServiceType>;
  /** fetch data from the table: "servicedCategory" */
  servicedCategory: Array<ServicedCategory>;
  /** fetch aggregated fields from the table: "servicedCategory" */
  servicedCategory_aggregate: ServicedCategory_Aggregate;
  /** fetch data from the table: "servicedCategory" using primary key columns */
  servicedCategory_by_pk?: Maybe<ServicedCategory>;
  /** fetch data from the table: "servicedProduct" */
  servicedProduct: Array<ServicedProduct>;
  /** fetch aggregated fields from the table: "servicedProduct" */
  servicedProduct_aggregate: ServicedProduct_Aggregate;
  /** fetch data from the table: "servicedProduct" using primary key columns */
  servicedProduct_by_pk?: Maybe<ServicedProduct>;
  /** fetch data from the table: "user" */
  user: Array<User>;
  /** An array relationship */
  userServiceTypes: Array<UserServiceTypes>;
  /** An aggregate relationship */
  userServiceTypes_aggregate: UserServiceTypes_Aggregate;
  /** fetch data from the table: "userServiceTypes" using primary key columns */
  userServiceTypes_by_pk?: Maybe<UserServiceTypes>;
  /** An array relationship */
  userZipCodes: Array<UserZipCodes>;
  /** An aggregate relationship */
  userZipCodes_aggregate: UserZipCodes_Aggregate;
  /** fetch data from the table: "userZipCodes" using primary key columns */
  userZipCodes_by_pk?: Maybe<UserZipCodes>;
  /** fetch aggregated fields from the table: "user" */
  user_aggregate: User_Aggregate;
  /** fetch data from the table: "user" using primary key columns */
  user_by_pk?: Maybe<User>;
  /** fetch data from the table: "zipCodes" */
  zipCodes: Array<ZipCodes>;
  /** fetch aggregated fields from the table: "zipCodes" */
  zipCodes_aggregate: ZipCodes_Aggregate;
  /** fetch data from the table: "zipCodes" using primary key columns */
  zipCodes_by_pk?: Maybe<ZipCodes>;
};


export type Query_RootAvailabilityArgs = {
  distinct_on?: InputMaybe<Array<Availability_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Availability_Order_By>>;
  where?: InputMaybe<Availability_Bool_Exp>;
};


export type Query_RootAvailabilityZipCodesArgs = {
  distinct_on?: InputMaybe<Array<AvailabilityZipCodes_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<AvailabilityZipCodes_Order_By>>;
  where?: InputMaybe<AvailabilityZipCodes_Bool_Exp>;
};


export type Query_RootAvailabilityZipCodes_AggregateArgs = {
  distinct_on?: InputMaybe<Array<AvailabilityZipCodes_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<AvailabilityZipCodes_Order_By>>;
  where?: InputMaybe<AvailabilityZipCodes_Bool_Exp>;
};


export type Query_RootAvailabilityZipCodes_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Query_RootAvailability_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Availability_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Availability_Order_By>>;
  where?: InputMaybe<Availability_Bool_Exp>;
};


export type Query_RootAvailability_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Query_RootCommentArgs = {
  distinct_on?: InputMaybe<Array<Comment_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Comment_Order_By>>;
  where?: InputMaybe<Comment_Bool_Exp>;
};


export type Query_RootCommentEntityArgs = {
  distinct_on?: InputMaybe<Array<CommentEntity_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<CommentEntity_Order_By>>;
  where?: InputMaybe<CommentEntity_Bool_Exp>;
};


export type Query_RootCommentEntity_AggregateArgs = {
  distinct_on?: InputMaybe<Array<CommentEntity_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<CommentEntity_Order_By>>;
  where?: InputMaybe<CommentEntity_Bool_Exp>;
};


export type Query_RootCommentEntity_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Query_RootComment_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Comment_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Comment_Order_By>>;
  where?: InputMaybe<Comment_Bool_Exp>;
};


export type Query_RootComment_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Query_RootEshopArgs = {
  distinct_on?: InputMaybe<Array<Eshop_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Eshop_Order_By>>;
  where?: InputMaybe<Eshop_Bool_Exp>;
};


export type Query_RootEshop_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Eshop_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Eshop_Order_By>>;
  where?: InputMaybe<Eshop_Bool_Exp>;
};


export type Query_RootEshop_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Query_RootInstallationArgs = {
  distinct_on?: InputMaybe<Array<Installation_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Installation_Order_By>>;
  where?: InputMaybe<Installation_Bool_Exp>;
};


export type Query_RootInstallationSessionArgs = {
  distinct_on?: InputMaybe<Array<InstallationSession_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<InstallationSession_Order_By>>;
  where?: InputMaybe<InstallationSession_Bool_Exp>;
};


export type Query_RootInstallationSession_AggregateArgs = {
  distinct_on?: InputMaybe<Array<InstallationSession_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<InstallationSession_Order_By>>;
  where?: InputMaybe<InstallationSession_Bool_Exp>;
};


export type Query_RootInstallationSession_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Query_RootInstallationStateArgs = {
  distinct_on?: InputMaybe<Array<InstallationState_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<InstallationState_Order_By>>;
  where?: InputMaybe<InstallationState_Bool_Exp>;
};


export type Query_RootInstallationState_AggregateArgs = {
  distinct_on?: InputMaybe<Array<InstallationState_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<InstallationState_Order_By>>;
  where?: InputMaybe<InstallationState_Bool_Exp>;
};


export type Query_RootInstallationState_By_PkArgs = {
  value: Scalars['String'];
};


export type Query_RootInstallation_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Installation_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Installation_Order_By>>;
  where?: InputMaybe<Installation_Bool_Exp>;
};


export type Query_RootInstallation_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Query_RootPinSafeCharsArgs = {
  distinct_on?: InputMaybe<Array<PinSafeChars_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<PinSafeChars_Order_By>>;
  where?: InputMaybe<PinSafeChars_Bool_Exp>;
};


export type Query_RootPinSafeChars_AggregateArgs = {
  distinct_on?: InputMaybe<Array<PinSafeChars_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<PinSafeChars_Order_By>>;
  where?: InputMaybe<PinSafeChars_Bool_Exp>;
};


export type Query_RootPinSafeChars_By_PkArgs = {
  character: Scalars['bpchar'];
};


export type Query_RootRoleArgs = {
  distinct_on?: InputMaybe<Array<Role_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Role_Order_By>>;
  where?: InputMaybe<Role_Bool_Exp>;
};


export type Query_RootRole_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Role_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Role_Order_By>>;
  where?: InputMaybe<Role_Bool_Exp>;
};


export type Query_RootRole_By_PkArgs = {
  value: Scalars['String'];
};


export type Query_RootServicePriceArgs = {
  distinct_on?: InputMaybe<Array<ServicePrice_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<ServicePrice_Order_By>>;
  where?: InputMaybe<ServicePrice_Bool_Exp>;
};


export type Query_RootServicePrice_AggregateArgs = {
  distinct_on?: InputMaybe<Array<ServicePrice_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<ServicePrice_Order_By>>;
  where?: InputMaybe<ServicePrice_Bool_Exp>;
};


export type Query_RootServicePrice_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Query_RootServiceTypeArgs = {
  distinct_on?: InputMaybe<Array<ServiceType_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<ServiceType_Order_By>>;
  where?: InputMaybe<ServiceType_Bool_Exp>;
};


export type Query_RootServiceType_AggregateArgs = {
  distinct_on?: InputMaybe<Array<ServiceType_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<ServiceType_Order_By>>;
  where?: InputMaybe<ServiceType_Bool_Exp>;
};


export type Query_RootServiceType_By_PkArgs = {
  value: Scalars['String'];
};


export type Query_RootServicedCategoryArgs = {
  distinct_on?: InputMaybe<Array<ServicedCategory_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<ServicedCategory_Order_By>>;
  where?: InputMaybe<ServicedCategory_Bool_Exp>;
};


export type Query_RootServicedCategory_AggregateArgs = {
  distinct_on?: InputMaybe<Array<ServicedCategory_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<ServicedCategory_Order_By>>;
  where?: InputMaybe<ServicedCategory_Bool_Exp>;
};


export type Query_RootServicedCategory_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Query_RootServicedProductArgs = {
  distinct_on?: InputMaybe<Array<ServicedProduct_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<ServicedProduct_Order_By>>;
  where?: InputMaybe<ServicedProduct_Bool_Exp>;
};


export type Query_RootServicedProduct_AggregateArgs = {
  distinct_on?: InputMaybe<Array<ServicedProduct_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<ServicedProduct_Order_By>>;
  where?: InputMaybe<ServicedProduct_Bool_Exp>;
};


export type Query_RootServicedProduct_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Query_RootUserArgs = {
  distinct_on?: InputMaybe<Array<User_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<User_Order_By>>;
  where?: InputMaybe<User_Bool_Exp>;
};


export type Query_RootUserServiceTypesArgs = {
  distinct_on?: InputMaybe<Array<UserServiceTypes_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<UserServiceTypes_Order_By>>;
  where?: InputMaybe<UserServiceTypes_Bool_Exp>;
};


export type Query_RootUserServiceTypes_AggregateArgs = {
  distinct_on?: InputMaybe<Array<UserServiceTypes_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<UserServiceTypes_Order_By>>;
  where?: InputMaybe<UserServiceTypes_Bool_Exp>;
};


export type Query_RootUserServiceTypes_By_PkArgs = {
  serviceTypeValue: ServiceType_Enum;
  userId: Scalars['uuid'];
};


export type Query_RootUserZipCodesArgs = {
  distinct_on?: InputMaybe<Array<UserZipCodes_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<UserZipCodes_Order_By>>;
  where?: InputMaybe<UserZipCodes_Bool_Exp>;
};


export type Query_RootUserZipCodes_AggregateArgs = {
  distinct_on?: InputMaybe<Array<UserZipCodes_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<UserZipCodes_Order_By>>;
  where?: InputMaybe<UserZipCodes_Bool_Exp>;
};


export type Query_RootUserZipCodes_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Query_RootUser_AggregateArgs = {
  distinct_on?: InputMaybe<Array<User_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<User_Order_By>>;
  where?: InputMaybe<User_Bool_Exp>;
};


export type Query_RootUser_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Query_RootZipCodesArgs = {
  distinct_on?: InputMaybe<Array<ZipCodes_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<ZipCodes_Order_By>>;
  where?: InputMaybe<ZipCodes_Bool_Exp>;
};


export type Query_RootZipCodes_AggregateArgs = {
  distinct_on?: InputMaybe<Array<ZipCodes_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<ZipCodes_Order_By>>;
  where?: InputMaybe<ZipCodes_Bool_Exp>;
};


export type Query_RootZipCodes_By_PkArgs = {
  id: Scalars['String'];
};

/** columns and relationships of "role" */
export type Role = {
  __typename?: 'role';
  description: Scalars['String'];
  /** An array relationship */
  users: Array<User>;
  /** An aggregate relationship */
  users_aggregate: User_Aggregate;
  value: Scalars['String'];
};


/** columns and relationships of "role" */
export type RoleUsersArgs = {
  distinct_on?: InputMaybe<Array<User_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<User_Order_By>>;
  where?: InputMaybe<User_Bool_Exp>;
};


/** columns and relationships of "role" */
export type RoleUsers_AggregateArgs = {
  distinct_on?: InputMaybe<Array<User_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<User_Order_By>>;
  where?: InputMaybe<User_Bool_Exp>;
};

/** aggregated selection of "role" */
export type Role_Aggregate = {
  __typename?: 'role_aggregate';
  aggregate?: Maybe<Role_Aggregate_Fields>;
  nodes: Array<Role>;
};

/** aggregate fields of "role" */
export type Role_Aggregate_Fields = {
  __typename?: 'role_aggregate_fields';
  count: Scalars['Int'];
  max?: Maybe<Role_Max_Fields>;
  min?: Maybe<Role_Min_Fields>;
};


/** aggregate fields of "role" */
export type Role_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<Role_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']>;
};

/** Boolean expression to filter rows from the table "role". All fields are combined with a logical 'AND'. */
export type Role_Bool_Exp = {
  _and?: InputMaybe<Array<Role_Bool_Exp>>;
  _not?: InputMaybe<Role_Bool_Exp>;
  _or?: InputMaybe<Array<Role_Bool_Exp>>;
  description?: InputMaybe<String_Comparison_Exp>;
  users?: InputMaybe<User_Bool_Exp>;
  value?: InputMaybe<String_Comparison_Exp>;
};

/** unique or primary key constraints on table "role" */
export enum Role_Constraint {
  /** unique or primary key constraint */
  RolesPkey = 'roles_pkey'
}

export enum Role_Enum {
  /** Administrátor */
  Admin = 'admin',
  /** Zákazník */
  Customer = 'customer',
  /** Řemeslník */
  Serviceman = 'serviceman',
  /** Superuser */
  Superuser = 'superuser'
}

/** Boolean expression to compare columns of type "role_enum". All fields are combined with logical 'AND'. */
export type Role_Enum_Comparison_Exp = {
  _eq?: InputMaybe<Role_Enum>;
  _in?: InputMaybe<Array<Role_Enum>>;
  _is_null?: InputMaybe<Scalars['Boolean']>;
  _neq?: InputMaybe<Role_Enum>;
  _nin?: InputMaybe<Array<Role_Enum>>;
};

/** input type for inserting data into table "role" */
export type Role_Insert_Input = {
  description?: InputMaybe<Scalars['String']>;
  users?: InputMaybe<User_Arr_Rel_Insert_Input>;
  value?: InputMaybe<Scalars['String']>;
};

/** aggregate max on columns */
export type Role_Max_Fields = {
  __typename?: 'role_max_fields';
  description?: Maybe<Scalars['String']>;
  value?: Maybe<Scalars['String']>;
};

/** aggregate min on columns */
export type Role_Min_Fields = {
  __typename?: 'role_min_fields';
  description?: Maybe<Scalars['String']>;
  value?: Maybe<Scalars['String']>;
};

/** response of any mutation on the table "role" */
export type Role_Mutation_Response = {
  __typename?: 'role_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int'];
  /** data from the rows affected by the mutation */
  returning: Array<Role>;
};

/** input type for inserting object relation for remote table "role" */
export type Role_Obj_Rel_Insert_Input = {
  data: Role_Insert_Input;
  /** upsert condition */
  on_conflict?: InputMaybe<Role_On_Conflict>;
};

/** on_conflict condition type for table "role" */
export type Role_On_Conflict = {
  constraint: Role_Constraint;
  update_columns?: Array<Role_Update_Column>;
  where?: InputMaybe<Role_Bool_Exp>;
};

/** Ordering options when selecting data from "role". */
export type Role_Order_By = {
  description?: InputMaybe<Order_By>;
  users_aggregate?: InputMaybe<User_Aggregate_Order_By>;
  value?: InputMaybe<Order_By>;
};

/** primary key columns input for table: role */
export type Role_Pk_Columns_Input = {
  value: Scalars['String'];
};

/** select columns of table "role" */
export enum Role_Select_Column {
  /** column name */
  Description = 'description',
  /** column name */
  Value = 'value'
}

/** input type for updating data in table "role" */
export type Role_Set_Input = {
  description?: InputMaybe<Scalars['String']>;
  value?: InputMaybe<Scalars['String']>;
};

/** update columns of table "role" */
export enum Role_Update_Column {
  /** column name */
  Description = 'description',
  /** column name */
  Value = 'value'
}

/** columns and relationships of "servicePrice" */
export type ServicePrice = {
  __typename?: 'servicePrice';
  eshopProductId?: Maybe<Scalars['String']>;
  id: Scalars['uuid'];
  price: Scalars['Float'];
  /** An object relationship */
  servicedCategory: ServicedCategory;
  servicedCategoryId: Scalars['uuid'];
  validFrom?: Maybe<Scalars['timestamptz']>;
  validTo?: Maybe<Scalars['timestamptz']>;
};

/** aggregated selection of "servicePrice" */
export type ServicePrice_Aggregate = {
  __typename?: 'servicePrice_aggregate';
  aggregate?: Maybe<ServicePrice_Aggregate_Fields>;
  nodes: Array<ServicePrice>;
};

/** aggregate fields of "servicePrice" */
export type ServicePrice_Aggregate_Fields = {
  __typename?: 'servicePrice_aggregate_fields';
  avg?: Maybe<ServicePrice_Avg_Fields>;
  count: Scalars['Int'];
  max?: Maybe<ServicePrice_Max_Fields>;
  min?: Maybe<ServicePrice_Min_Fields>;
  stddev?: Maybe<ServicePrice_Stddev_Fields>;
  stddev_pop?: Maybe<ServicePrice_Stddev_Pop_Fields>;
  stddev_samp?: Maybe<ServicePrice_Stddev_Samp_Fields>;
  sum?: Maybe<ServicePrice_Sum_Fields>;
  var_pop?: Maybe<ServicePrice_Var_Pop_Fields>;
  var_samp?: Maybe<ServicePrice_Var_Samp_Fields>;
  variance?: Maybe<ServicePrice_Variance_Fields>;
};


/** aggregate fields of "servicePrice" */
export type ServicePrice_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<ServicePrice_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "servicePrice" */
export type ServicePrice_Aggregate_Order_By = {
  avg?: InputMaybe<ServicePrice_Avg_Order_By>;
  count?: InputMaybe<Order_By>;
  max?: InputMaybe<ServicePrice_Max_Order_By>;
  min?: InputMaybe<ServicePrice_Min_Order_By>;
  stddev?: InputMaybe<ServicePrice_Stddev_Order_By>;
  stddev_pop?: InputMaybe<ServicePrice_Stddev_Pop_Order_By>;
  stddev_samp?: InputMaybe<ServicePrice_Stddev_Samp_Order_By>;
  sum?: InputMaybe<ServicePrice_Sum_Order_By>;
  var_pop?: InputMaybe<ServicePrice_Var_Pop_Order_By>;
  var_samp?: InputMaybe<ServicePrice_Var_Samp_Order_By>;
  variance?: InputMaybe<ServicePrice_Variance_Order_By>;
};

/** input type for inserting array relation for remote table "servicePrice" */
export type ServicePrice_Arr_Rel_Insert_Input = {
  data: Array<ServicePrice_Insert_Input>;
  /** upsert condition */
  on_conflict?: InputMaybe<ServicePrice_On_Conflict>;
};

/** aggregate avg on columns */
export type ServicePrice_Avg_Fields = {
  __typename?: 'servicePrice_avg_fields';
  price?: Maybe<Scalars['Float']>;
};

/** order by avg() on columns of table "servicePrice" */
export type ServicePrice_Avg_Order_By = {
  price?: InputMaybe<Order_By>;
};

/** Boolean expression to filter rows from the table "servicePrice". All fields are combined with a logical 'AND'. */
export type ServicePrice_Bool_Exp = {
  _and?: InputMaybe<Array<ServicePrice_Bool_Exp>>;
  _not?: InputMaybe<ServicePrice_Bool_Exp>;
  _or?: InputMaybe<Array<ServicePrice_Bool_Exp>>;
  eshopProductId?: InputMaybe<String_Comparison_Exp>;
  id?: InputMaybe<Uuid_Comparison_Exp>;
  price?: InputMaybe<Float_Comparison_Exp>;
  servicedCategory?: InputMaybe<ServicedCategory_Bool_Exp>;
  servicedCategoryId?: InputMaybe<Uuid_Comparison_Exp>;
  validFrom?: InputMaybe<Timestamptz_Comparison_Exp>;
  validTo?: InputMaybe<Timestamptz_Comparison_Exp>;
};

/** unique or primary key constraints on table "servicePrice" */
export enum ServicePrice_Constraint {
  /** unique or primary key constraint */
  ServicepricePkey = 'serviceprice_pkey'
}

/** input type for incrementing numeric columns in table "servicePrice" */
export type ServicePrice_Inc_Input = {
  price?: InputMaybe<Scalars['Float']>;
};

/** input type for inserting data into table "servicePrice" */
export type ServicePrice_Insert_Input = {
  eshopProductId?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['uuid']>;
  price?: InputMaybe<Scalars['Float']>;
  servicedCategory?: InputMaybe<ServicedCategory_Obj_Rel_Insert_Input>;
  servicedCategoryId?: InputMaybe<Scalars['uuid']>;
  validFrom?: InputMaybe<Scalars['timestamptz']>;
  validTo?: InputMaybe<Scalars['timestamptz']>;
};

/** aggregate max on columns */
export type ServicePrice_Max_Fields = {
  __typename?: 'servicePrice_max_fields';
  eshopProductId?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['uuid']>;
  price?: Maybe<Scalars['Float']>;
  servicedCategoryId?: Maybe<Scalars['uuid']>;
  validFrom?: Maybe<Scalars['timestamptz']>;
  validTo?: Maybe<Scalars['timestamptz']>;
};

/** order by max() on columns of table "servicePrice" */
export type ServicePrice_Max_Order_By = {
  eshopProductId?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  price?: InputMaybe<Order_By>;
  servicedCategoryId?: InputMaybe<Order_By>;
  validFrom?: InputMaybe<Order_By>;
  validTo?: InputMaybe<Order_By>;
};

/** aggregate min on columns */
export type ServicePrice_Min_Fields = {
  __typename?: 'servicePrice_min_fields';
  eshopProductId?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['uuid']>;
  price?: Maybe<Scalars['Float']>;
  servicedCategoryId?: Maybe<Scalars['uuid']>;
  validFrom?: Maybe<Scalars['timestamptz']>;
  validTo?: Maybe<Scalars['timestamptz']>;
};

/** order by min() on columns of table "servicePrice" */
export type ServicePrice_Min_Order_By = {
  eshopProductId?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  price?: InputMaybe<Order_By>;
  servicedCategoryId?: InputMaybe<Order_By>;
  validFrom?: InputMaybe<Order_By>;
  validTo?: InputMaybe<Order_By>;
};

/** response of any mutation on the table "servicePrice" */
export type ServicePrice_Mutation_Response = {
  __typename?: 'servicePrice_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int'];
  /** data from the rows affected by the mutation */
  returning: Array<ServicePrice>;
};

/** on_conflict condition type for table "servicePrice" */
export type ServicePrice_On_Conflict = {
  constraint: ServicePrice_Constraint;
  update_columns?: Array<ServicePrice_Update_Column>;
  where?: InputMaybe<ServicePrice_Bool_Exp>;
};

/** Ordering options when selecting data from "servicePrice". */
export type ServicePrice_Order_By = {
  eshopProductId?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  price?: InputMaybe<Order_By>;
  servicedCategory?: InputMaybe<ServicedCategory_Order_By>;
  servicedCategoryId?: InputMaybe<Order_By>;
  validFrom?: InputMaybe<Order_By>;
  validTo?: InputMaybe<Order_By>;
};

/** primary key columns input for table: servicePrice */
export type ServicePrice_Pk_Columns_Input = {
  id: Scalars['uuid'];
};

/** select columns of table "servicePrice" */
export enum ServicePrice_Select_Column {
  /** column name */
  EshopProductId = 'eshopProductId',
  /** column name */
  Id = 'id',
  /** column name */
  Price = 'price',
  /** column name */
  ServicedCategoryId = 'servicedCategoryId',
  /** column name */
  ValidFrom = 'validFrom',
  /** column name */
  ValidTo = 'validTo'
}

/** input type for updating data in table "servicePrice" */
export type ServicePrice_Set_Input = {
  eshopProductId?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['uuid']>;
  price?: InputMaybe<Scalars['Float']>;
  servicedCategoryId?: InputMaybe<Scalars['uuid']>;
  validFrom?: InputMaybe<Scalars['timestamptz']>;
  validTo?: InputMaybe<Scalars['timestamptz']>;
};

/** aggregate stddev on columns */
export type ServicePrice_Stddev_Fields = {
  __typename?: 'servicePrice_stddev_fields';
  price?: Maybe<Scalars['Float']>;
};

/** order by stddev() on columns of table "servicePrice" */
export type ServicePrice_Stddev_Order_By = {
  price?: InputMaybe<Order_By>;
};

/** aggregate stddev_pop on columns */
export type ServicePrice_Stddev_Pop_Fields = {
  __typename?: 'servicePrice_stddev_pop_fields';
  price?: Maybe<Scalars['Float']>;
};

/** order by stddev_pop() on columns of table "servicePrice" */
export type ServicePrice_Stddev_Pop_Order_By = {
  price?: InputMaybe<Order_By>;
};

/** aggregate stddev_samp on columns */
export type ServicePrice_Stddev_Samp_Fields = {
  __typename?: 'servicePrice_stddev_samp_fields';
  price?: Maybe<Scalars['Float']>;
};

/** order by stddev_samp() on columns of table "servicePrice" */
export type ServicePrice_Stddev_Samp_Order_By = {
  price?: InputMaybe<Order_By>;
};

/** aggregate sum on columns */
export type ServicePrice_Sum_Fields = {
  __typename?: 'servicePrice_sum_fields';
  price?: Maybe<Scalars['Float']>;
};

/** order by sum() on columns of table "servicePrice" */
export type ServicePrice_Sum_Order_By = {
  price?: InputMaybe<Order_By>;
};

/** update columns of table "servicePrice" */
export enum ServicePrice_Update_Column {
  /** column name */
  EshopProductId = 'eshopProductId',
  /** column name */
  Id = 'id',
  /** column name */
  Price = 'price',
  /** column name */
  ServicedCategoryId = 'servicedCategoryId',
  /** column name */
  ValidFrom = 'validFrom',
  /** column name */
  ValidTo = 'validTo'
}

/** aggregate var_pop on columns */
export type ServicePrice_Var_Pop_Fields = {
  __typename?: 'servicePrice_var_pop_fields';
  price?: Maybe<Scalars['Float']>;
};

/** order by var_pop() on columns of table "servicePrice" */
export type ServicePrice_Var_Pop_Order_By = {
  price?: InputMaybe<Order_By>;
};

/** aggregate var_samp on columns */
export type ServicePrice_Var_Samp_Fields = {
  __typename?: 'servicePrice_var_samp_fields';
  price?: Maybe<Scalars['Float']>;
};

/** order by var_samp() on columns of table "servicePrice" */
export type ServicePrice_Var_Samp_Order_By = {
  price?: InputMaybe<Order_By>;
};

/** aggregate variance on columns */
export type ServicePrice_Variance_Fields = {
  __typename?: 'servicePrice_variance_fields';
  price?: Maybe<Scalars['Float']>;
};

/** order by variance() on columns of table "servicePrice" */
export type ServicePrice_Variance_Order_By = {
  price?: InputMaybe<Order_By>;
};

/** columns and relationships of "serviceType" */
export type ServiceType = {
  __typename?: 'serviceType';
  description: Scalars['String'];
  /** An array relationship */
  servicedCategories: Array<ServicedCategory>;
  /** An aggregate relationship */
  servicedCategories_aggregate: ServicedCategory_Aggregate;
  /** An array relationship */
  userServiceTypes: Array<UserServiceTypes>;
  /** An aggregate relationship */
  userServiceTypes_aggregate: UserServiceTypes_Aggregate;
  value: Scalars['String'];
};


/** columns and relationships of "serviceType" */
export type ServiceTypeServicedCategoriesArgs = {
  distinct_on?: InputMaybe<Array<ServicedCategory_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<ServicedCategory_Order_By>>;
  where?: InputMaybe<ServicedCategory_Bool_Exp>;
};


/** columns and relationships of "serviceType" */
export type ServiceTypeServicedCategories_AggregateArgs = {
  distinct_on?: InputMaybe<Array<ServicedCategory_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<ServicedCategory_Order_By>>;
  where?: InputMaybe<ServicedCategory_Bool_Exp>;
};


/** columns and relationships of "serviceType" */
export type ServiceTypeUserServiceTypesArgs = {
  distinct_on?: InputMaybe<Array<UserServiceTypes_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<UserServiceTypes_Order_By>>;
  where?: InputMaybe<UserServiceTypes_Bool_Exp>;
};


/** columns and relationships of "serviceType" */
export type ServiceTypeUserServiceTypes_AggregateArgs = {
  distinct_on?: InputMaybe<Array<UserServiceTypes_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<UserServiceTypes_Order_By>>;
  where?: InputMaybe<UserServiceTypes_Bool_Exp>;
};

/** aggregated selection of "serviceType" */
export type ServiceType_Aggregate = {
  __typename?: 'serviceType_aggregate';
  aggregate?: Maybe<ServiceType_Aggregate_Fields>;
  nodes: Array<ServiceType>;
};

/** aggregate fields of "serviceType" */
export type ServiceType_Aggregate_Fields = {
  __typename?: 'serviceType_aggregate_fields';
  count: Scalars['Int'];
  max?: Maybe<ServiceType_Max_Fields>;
  min?: Maybe<ServiceType_Min_Fields>;
};


/** aggregate fields of "serviceType" */
export type ServiceType_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<ServiceType_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']>;
};

/** Boolean expression to filter rows from the table "serviceType". All fields are combined with a logical 'AND'. */
export type ServiceType_Bool_Exp = {
  _and?: InputMaybe<Array<ServiceType_Bool_Exp>>;
  _not?: InputMaybe<ServiceType_Bool_Exp>;
  _or?: InputMaybe<Array<ServiceType_Bool_Exp>>;
  description?: InputMaybe<String_Comparison_Exp>;
  servicedCategories?: InputMaybe<ServicedCategory_Bool_Exp>;
  userServiceTypes?: InputMaybe<UserServiceTypes_Bool_Exp>;
  value?: InputMaybe<String_Comparison_Exp>;
};

/** unique or primary key constraints on table "serviceType" */
export enum ServiceType_Constraint {
  /** unique or primary key constraint */
  ServiceTypePkey = 'serviceType_pkey'
}

export enum ServiceType_Enum {
  /** Elektroinstalace */
  Electricity = 'electricity',
  /** Plynařské práce */
  Gas = 'gas',
  /** Instalatérské práce */
  Water = 'water'
}

/** Boolean expression to compare columns of type "serviceType_enum". All fields are combined with logical 'AND'. */
export type ServiceType_Enum_Comparison_Exp = {
  _eq?: InputMaybe<ServiceType_Enum>;
  _in?: InputMaybe<Array<ServiceType_Enum>>;
  _is_null?: InputMaybe<Scalars['Boolean']>;
  _neq?: InputMaybe<ServiceType_Enum>;
  _nin?: InputMaybe<Array<ServiceType_Enum>>;
};

/** input type for inserting data into table "serviceType" */
export type ServiceType_Insert_Input = {
  description?: InputMaybe<Scalars['String']>;
  servicedCategories?: InputMaybe<ServicedCategory_Arr_Rel_Insert_Input>;
  userServiceTypes?: InputMaybe<UserServiceTypes_Arr_Rel_Insert_Input>;
  value?: InputMaybe<Scalars['String']>;
};

/** aggregate max on columns */
export type ServiceType_Max_Fields = {
  __typename?: 'serviceType_max_fields';
  description?: Maybe<Scalars['String']>;
  value?: Maybe<Scalars['String']>;
};

/** aggregate min on columns */
export type ServiceType_Min_Fields = {
  __typename?: 'serviceType_min_fields';
  description?: Maybe<Scalars['String']>;
  value?: Maybe<Scalars['String']>;
};

/** response of any mutation on the table "serviceType" */
export type ServiceType_Mutation_Response = {
  __typename?: 'serviceType_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int'];
  /** data from the rows affected by the mutation */
  returning: Array<ServiceType>;
};

/** input type for inserting object relation for remote table "serviceType" */
export type ServiceType_Obj_Rel_Insert_Input = {
  data: ServiceType_Insert_Input;
  /** upsert condition */
  on_conflict?: InputMaybe<ServiceType_On_Conflict>;
};

/** on_conflict condition type for table "serviceType" */
export type ServiceType_On_Conflict = {
  constraint: ServiceType_Constraint;
  update_columns?: Array<ServiceType_Update_Column>;
  where?: InputMaybe<ServiceType_Bool_Exp>;
};

/** Ordering options when selecting data from "serviceType". */
export type ServiceType_Order_By = {
  description?: InputMaybe<Order_By>;
  servicedCategories_aggregate?: InputMaybe<ServicedCategory_Aggregate_Order_By>;
  userServiceTypes_aggregate?: InputMaybe<UserServiceTypes_Aggregate_Order_By>;
  value?: InputMaybe<Order_By>;
};

/** primary key columns input for table: serviceType */
export type ServiceType_Pk_Columns_Input = {
  value: Scalars['String'];
};

/** select columns of table "serviceType" */
export enum ServiceType_Select_Column {
  /** column name */
  Description = 'description',
  /** column name */
  Value = 'value'
}

/** input type for updating data in table "serviceType" */
export type ServiceType_Set_Input = {
  description?: InputMaybe<Scalars['String']>;
  value?: InputMaybe<Scalars['String']>;
};

/** update columns of table "serviceType" */
export enum ServiceType_Update_Column {
  /** column name */
  Description = 'description',
  /** column name */
  Value = 'value'
}

/** columns and relationships of "servicedCategory" */
export type ServicedCategory = {
  __typename?: 'servicedCategory';
  categoryName: Scalars['String'];
  createdAt: Scalars['timestamptz'];
  /** An object relationship */
  eshop: Eshop;
  eshopCategoryId?: Maybe<Scalars['String']>;
  eshopId: Scalars['uuid'];
  id: Scalars['uuid'];
  /** An array relationship */
  servicePrices: Array<ServicePrice>;
  /** An aggregate relationship */
  servicePrices_aggregate: ServicePrice_Aggregate;
  /** An object relationship */
  serviceType: ServiceType;
  serviceTypeValue: ServiceType_Enum;
  updatedAt: Scalars['timestamptz'];
};


/** columns and relationships of "servicedCategory" */
export type ServicedCategoryServicePricesArgs = {
  distinct_on?: InputMaybe<Array<ServicePrice_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<ServicePrice_Order_By>>;
  where?: InputMaybe<ServicePrice_Bool_Exp>;
};


/** columns and relationships of "servicedCategory" */
export type ServicedCategoryServicePrices_AggregateArgs = {
  distinct_on?: InputMaybe<Array<ServicePrice_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<ServicePrice_Order_By>>;
  where?: InputMaybe<ServicePrice_Bool_Exp>;
};

/** aggregated selection of "servicedCategory" */
export type ServicedCategory_Aggregate = {
  __typename?: 'servicedCategory_aggregate';
  aggregate?: Maybe<ServicedCategory_Aggregate_Fields>;
  nodes: Array<ServicedCategory>;
};

/** aggregate fields of "servicedCategory" */
export type ServicedCategory_Aggregate_Fields = {
  __typename?: 'servicedCategory_aggregate_fields';
  count: Scalars['Int'];
  max?: Maybe<ServicedCategory_Max_Fields>;
  min?: Maybe<ServicedCategory_Min_Fields>;
};


/** aggregate fields of "servicedCategory" */
export type ServicedCategory_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<ServicedCategory_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "servicedCategory" */
export type ServicedCategory_Aggregate_Order_By = {
  count?: InputMaybe<Order_By>;
  max?: InputMaybe<ServicedCategory_Max_Order_By>;
  min?: InputMaybe<ServicedCategory_Min_Order_By>;
};

/** input type for inserting array relation for remote table "servicedCategory" */
export type ServicedCategory_Arr_Rel_Insert_Input = {
  data: Array<ServicedCategory_Insert_Input>;
  /** upsert condition */
  on_conflict?: InputMaybe<ServicedCategory_On_Conflict>;
};

/** Boolean expression to filter rows from the table "servicedCategory". All fields are combined with a logical 'AND'. */
export type ServicedCategory_Bool_Exp = {
  _and?: InputMaybe<Array<ServicedCategory_Bool_Exp>>;
  _not?: InputMaybe<ServicedCategory_Bool_Exp>;
  _or?: InputMaybe<Array<ServicedCategory_Bool_Exp>>;
  categoryName?: InputMaybe<String_Comparison_Exp>;
  createdAt?: InputMaybe<Timestamptz_Comparison_Exp>;
  eshop?: InputMaybe<Eshop_Bool_Exp>;
  eshopCategoryId?: InputMaybe<String_Comparison_Exp>;
  eshopId?: InputMaybe<Uuid_Comparison_Exp>;
  id?: InputMaybe<Uuid_Comparison_Exp>;
  servicePrices?: InputMaybe<ServicePrice_Bool_Exp>;
  serviceType?: InputMaybe<ServiceType_Bool_Exp>;
  serviceTypeValue?: InputMaybe<ServiceType_Enum_Comparison_Exp>;
  updatedAt?: InputMaybe<Timestamptz_Comparison_Exp>;
};

/** unique or primary key constraints on table "servicedCategory" */
export enum ServicedCategory_Constraint {
  /** unique or primary key constraint */
  ServicedCategoryIdKey = 'servicedCategory_id_key',
  /** unique or primary key constraint */
  ServicedCategoryPkey = 'servicedCategory_pkey'
}

/** input type for inserting data into table "servicedCategory" */
export type ServicedCategory_Insert_Input = {
  categoryName?: InputMaybe<Scalars['String']>;
  createdAt?: InputMaybe<Scalars['timestamptz']>;
  eshop?: InputMaybe<Eshop_Obj_Rel_Insert_Input>;
  eshopCategoryId?: InputMaybe<Scalars['String']>;
  eshopId?: InputMaybe<Scalars['uuid']>;
  id?: InputMaybe<Scalars['uuid']>;
  servicePrices?: InputMaybe<ServicePrice_Arr_Rel_Insert_Input>;
  serviceType?: InputMaybe<ServiceType_Obj_Rel_Insert_Input>;
  serviceTypeValue?: InputMaybe<ServiceType_Enum>;
  updatedAt?: InputMaybe<Scalars['timestamptz']>;
};

/** aggregate max on columns */
export type ServicedCategory_Max_Fields = {
  __typename?: 'servicedCategory_max_fields';
  categoryName?: Maybe<Scalars['String']>;
  createdAt?: Maybe<Scalars['timestamptz']>;
  eshopCategoryId?: Maybe<Scalars['String']>;
  eshopId?: Maybe<Scalars['uuid']>;
  id?: Maybe<Scalars['uuid']>;
  updatedAt?: Maybe<Scalars['timestamptz']>;
};

/** order by max() on columns of table "servicedCategory" */
export type ServicedCategory_Max_Order_By = {
  categoryName?: InputMaybe<Order_By>;
  createdAt?: InputMaybe<Order_By>;
  eshopCategoryId?: InputMaybe<Order_By>;
  eshopId?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  updatedAt?: InputMaybe<Order_By>;
};

/** aggregate min on columns */
export type ServicedCategory_Min_Fields = {
  __typename?: 'servicedCategory_min_fields';
  categoryName?: Maybe<Scalars['String']>;
  createdAt?: Maybe<Scalars['timestamptz']>;
  eshopCategoryId?: Maybe<Scalars['String']>;
  eshopId?: Maybe<Scalars['uuid']>;
  id?: Maybe<Scalars['uuid']>;
  updatedAt?: Maybe<Scalars['timestamptz']>;
};

/** order by min() on columns of table "servicedCategory" */
export type ServicedCategory_Min_Order_By = {
  categoryName?: InputMaybe<Order_By>;
  createdAt?: InputMaybe<Order_By>;
  eshopCategoryId?: InputMaybe<Order_By>;
  eshopId?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  updatedAt?: InputMaybe<Order_By>;
};

/** response of any mutation on the table "servicedCategory" */
export type ServicedCategory_Mutation_Response = {
  __typename?: 'servicedCategory_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int'];
  /** data from the rows affected by the mutation */
  returning: Array<ServicedCategory>;
};

/** input type for inserting object relation for remote table "servicedCategory" */
export type ServicedCategory_Obj_Rel_Insert_Input = {
  data: ServicedCategory_Insert_Input;
  /** upsert condition */
  on_conflict?: InputMaybe<ServicedCategory_On_Conflict>;
};

/** on_conflict condition type for table "servicedCategory" */
export type ServicedCategory_On_Conflict = {
  constraint: ServicedCategory_Constraint;
  update_columns?: Array<ServicedCategory_Update_Column>;
  where?: InputMaybe<ServicedCategory_Bool_Exp>;
};

/** Ordering options when selecting data from "servicedCategory". */
export type ServicedCategory_Order_By = {
  categoryName?: InputMaybe<Order_By>;
  createdAt?: InputMaybe<Order_By>;
  eshop?: InputMaybe<Eshop_Order_By>;
  eshopCategoryId?: InputMaybe<Order_By>;
  eshopId?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  servicePrices_aggregate?: InputMaybe<ServicePrice_Aggregate_Order_By>;
  serviceType?: InputMaybe<ServiceType_Order_By>;
  serviceTypeValue?: InputMaybe<Order_By>;
  updatedAt?: InputMaybe<Order_By>;
};

/** primary key columns input for table: servicedCategory */
export type ServicedCategory_Pk_Columns_Input = {
  id: Scalars['uuid'];
};

/** select columns of table "servicedCategory" */
export enum ServicedCategory_Select_Column {
  /** column name */
  CategoryName = 'categoryName',
  /** column name */
  CreatedAt = 'createdAt',
  /** column name */
  EshopCategoryId = 'eshopCategoryId',
  /** column name */
  EshopId = 'eshopId',
  /** column name */
  Id = 'id',
  /** column name */
  ServiceTypeValue = 'serviceTypeValue',
  /** column name */
  UpdatedAt = 'updatedAt'
}

/** input type for updating data in table "servicedCategory" */
export type ServicedCategory_Set_Input = {
  categoryName?: InputMaybe<Scalars['String']>;
  createdAt?: InputMaybe<Scalars['timestamptz']>;
  eshopCategoryId?: InputMaybe<Scalars['String']>;
  eshopId?: InputMaybe<Scalars['uuid']>;
  id?: InputMaybe<Scalars['uuid']>;
  serviceTypeValue?: InputMaybe<ServiceType_Enum>;
  updatedAt?: InputMaybe<Scalars['timestamptz']>;
};

/** update columns of table "servicedCategory" */
export enum ServicedCategory_Update_Column {
  /** column name */
  CategoryName = 'categoryName',
  /** column name */
  CreatedAt = 'createdAt',
  /** column name */
  EshopCategoryId = 'eshopCategoryId',
  /** column name */
  EshopId = 'eshopId',
  /** column name */
  Id = 'id',
  /** column name */
  ServiceTypeValue = 'serviceTypeValue',
  /** column name */
  UpdatedAt = 'updatedAt'
}

/** columns and relationships of "servicedProduct" */
export type ServicedProduct = {
  __typename?: 'servicedProduct';
  createdAt: Scalars['timestamptz'];
  eshopId: Scalars['uuid'];
  eshopProductId?: Maybe<Scalars['String']>;
  id: Scalars['uuid'];
  productName: Scalars['String'];
  updatedAt: Scalars['timestamptz'];
};

/** aggregated selection of "servicedProduct" */
export type ServicedProduct_Aggregate = {
  __typename?: 'servicedProduct_aggregate';
  aggregate?: Maybe<ServicedProduct_Aggregate_Fields>;
  nodes: Array<ServicedProduct>;
};

/** aggregate fields of "servicedProduct" */
export type ServicedProduct_Aggregate_Fields = {
  __typename?: 'servicedProduct_aggregate_fields';
  count: Scalars['Int'];
  max?: Maybe<ServicedProduct_Max_Fields>;
  min?: Maybe<ServicedProduct_Min_Fields>;
};


/** aggregate fields of "servicedProduct" */
export type ServicedProduct_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<ServicedProduct_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']>;
};

/** Boolean expression to filter rows from the table "servicedProduct". All fields are combined with a logical 'AND'. */
export type ServicedProduct_Bool_Exp = {
  _and?: InputMaybe<Array<ServicedProduct_Bool_Exp>>;
  _not?: InputMaybe<ServicedProduct_Bool_Exp>;
  _or?: InputMaybe<Array<ServicedProduct_Bool_Exp>>;
  createdAt?: InputMaybe<Timestamptz_Comparison_Exp>;
  eshopId?: InputMaybe<Uuid_Comparison_Exp>;
  eshopProductId?: InputMaybe<String_Comparison_Exp>;
  id?: InputMaybe<Uuid_Comparison_Exp>;
  productName?: InputMaybe<String_Comparison_Exp>;
  updatedAt?: InputMaybe<Timestamptz_Comparison_Exp>;
};

/** unique or primary key constraints on table "servicedProduct" */
export enum ServicedProduct_Constraint {
  /** unique or primary key constraint */
  ServicedProductPkey = 'servicedProduct_pkey'
}

/** input type for inserting data into table "servicedProduct" */
export type ServicedProduct_Insert_Input = {
  createdAt?: InputMaybe<Scalars['timestamptz']>;
  eshopId?: InputMaybe<Scalars['uuid']>;
  eshopProductId?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['uuid']>;
  productName?: InputMaybe<Scalars['String']>;
  updatedAt?: InputMaybe<Scalars['timestamptz']>;
};

/** aggregate max on columns */
export type ServicedProduct_Max_Fields = {
  __typename?: 'servicedProduct_max_fields';
  createdAt?: Maybe<Scalars['timestamptz']>;
  eshopId?: Maybe<Scalars['uuid']>;
  eshopProductId?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['uuid']>;
  productName?: Maybe<Scalars['String']>;
  updatedAt?: Maybe<Scalars['timestamptz']>;
};

/** aggregate min on columns */
export type ServicedProduct_Min_Fields = {
  __typename?: 'servicedProduct_min_fields';
  createdAt?: Maybe<Scalars['timestamptz']>;
  eshopId?: Maybe<Scalars['uuid']>;
  eshopProductId?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['uuid']>;
  productName?: Maybe<Scalars['String']>;
  updatedAt?: Maybe<Scalars['timestamptz']>;
};

/** response of any mutation on the table "servicedProduct" */
export type ServicedProduct_Mutation_Response = {
  __typename?: 'servicedProduct_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int'];
  /** data from the rows affected by the mutation */
  returning: Array<ServicedProduct>;
};

/** on_conflict condition type for table "servicedProduct" */
export type ServicedProduct_On_Conflict = {
  constraint: ServicedProduct_Constraint;
  update_columns?: Array<ServicedProduct_Update_Column>;
  where?: InputMaybe<ServicedProduct_Bool_Exp>;
};

/** Ordering options when selecting data from "servicedProduct". */
export type ServicedProduct_Order_By = {
  createdAt?: InputMaybe<Order_By>;
  eshopId?: InputMaybe<Order_By>;
  eshopProductId?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  productName?: InputMaybe<Order_By>;
  updatedAt?: InputMaybe<Order_By>;
};

/** primary key columns input for table: servicedProduct */
export type ServicedProduct_Pk_Columns_Input = {
  id: Scalars['uuid'];
};

/** select columns of table "servicedProduct" */
export enum ServicedProduct_Select_Column {
  /** column name */
  CreatedAt = 'createdAt',
  /** column name */
  EshopId = 'eshopId',
  /** column name */
  EshopProductId = 'eshopProductId',
  /** column name */
  Id = 'id',
  /** column name */
  ProductName = 'productName',
  /** column name */
  UpdatedAt = 'updatedAt'
}

/** input type for updating data in table "servicedProduct" */
export type ServicedProduct_Set_Input = {
  createdAt?: InputMaybe<Scalars['timestamptz']>;
  eshopId?: InputMaybe<Scalars['uuid']>;
  eshopProductId?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['uuid']>;
  productName?: InputMaybe<Scalars['String']>;
  updatedAt?: InputMaybe<Scalars['timestamptz']>;
};

/** update columns of table "servicedProduct" */
export enum ServicedProduct_Update_Column {
  /** column name */
  CreatedAt = 'createdAt',
  /** column name */
  EshopId = 'eshopId',
  /** column name */
  EshopProductId = 'eshopProductId',
  /** column name */
  Id = 'id',
  /** column name */
  ProductName = 'productName',
  /** column name */
  UpdatedAt = 'updatedAt'
}

export type Subscription_Root = {
  __typename?: 'subscription_root';
  /** fetch data from the table: "availability" */
  availability: Array<Availability>;
  /** An array relationship */
  availabilityZipCodes: Array<AvailabilityZipCodes>;
  /** An aggregate relationship */
  availabilityZipCodes_aggregate: AvailabilityZipCodes_Aggregate;
  /** fetch data from the table: "availabilityZipCodes" using primary key columns */
  availabilityZipCodes_by_pk?: Maybe<AvailabilityZipCodes>;
  /** fetch aggregated fields from the table: "availability" */
  availability_aggregate: Availability_Aggregate;
  /** fetch data from the table: "availability" using primary key columns */
  availability_by_pk?: Maybe<Availability>;
  /** fetch data from the table: "comment" */
  comment: Array<Comment>;
  /** fetch data from the table: "commentEntity" */
  commentEntity: Array<CommentEntity>;
  /** fetch aggregated fields from the table: "commentEntity" */
  commentEntity_aggregate: CommentEntity_Aggregate;
  /** fetch data from the table: "commentEntity" using primary key columns */
  commentEntity_by_pk?: Maybe<CommentEntity>;
  /** fetch aggregated fields from the table: "comment" */
  comment_aggregate: Comment_Aggregate;
  /** fetch data from the table: "comment" using primary key columns */
  comment_by_pk?: Maybe<Comment>;
  /** fetch data from the table: "eshop" */
  eshop: Array<Eshop>;
  /** fetch aggregated fields from the table: "eshop" */
  eshop_aggregate: Eshop_Aggregate;
  /** fetch data from the table: "eshop" using primary key columns */
  eshop_by_pk?: Maybe<Eshop>;
  /** fetch data from the table: "installation" */
  installation: Array<Installation>;
  /** fetch data from the table: "installationSession" */
  installationSession: Array<InstallationSession>;
  /** fetch aggregated fields from the table: "installationSession" */
  installationSession_aggregate: InstallationSession_Aggregate;
  /** fetch data from the table: "installationSession" using primary key columns */
  installationSession_by_pk?: Maybe<InstallationSession>;
  /** fetch data from the table: "installationState" */
  installationState: Array<InstallationState>;
  /** fetch aggregated fields from the table: "installationState" */
  installationState_aggregate: InstallationState_Aggregate;
  /** fetch data from the table: "installationState" using primary key columns */
  installationState_by_pk?: Maybe<InstallationState>;
  /** fetch aggregated fields from the table: "installation" */
  installation_aggregate: Installation_Aggregate;
  /** fetch data from the table: "installation" using primary key columns */
  installation_by_pk?: Maybe<Installation>;
  /** fetch data from the table: "pinSafeChars" */
  pinSafeChars: Array<PinSafeChars>;
  /** fetch aggregated fields from the table: "pinSafeChars" */
  pinSafeChars_aggregate: PinSafeChars_Aggregate;
  /** fetch data from the table: "pinSafeChars" using primary key columns */
  pinSafeChars_by_pk?: Maybe<PinSafeChars>;
  /** fetch data from the table: "role" */
  role: Array<Role>;
  /** fetch aggregated fields from the table: "role" */
  role_aggregate: Role_Aggregate;
  /** fetch data from the table: "role" using primary key columns */
  role_by_pk?: Maybe<Role>;
  /** fetch data from the table: "servicePrice" */
  servicePrice: Array<ServicePrice>;
  /** fetch aggregated fields from the table: "servicePrice" */
  servicePrice_aggregate: ServicePrice_Aggregate;
  /** fetch data from the table: "servicePrice" using primary key columns */
  servicePrice_by_pk?: Maybe<ServicePrice>;
  /** fetch data from the table: "serviceType" */
  serviceType: Array<ServiceType>;
  /** fetch aggregated fields from the table: "serviceType" */
  serviceType_aggregate: ServiceType_Aggregate;
  /** fetch data from the table: "serviceType" using primary key columns */
  serviceType_by_pk?: Maybe<ServiceType>;
  /** fetch data from the table: "servicedCategory" */
  servicedCategory: Array<ServicedCategory>;
  /** fetch aggregated fields from the table: "servicedCategory" */
  servicedCategory_aggregate: ServicedCategory_Aggregate;
  /** fetch data from the table: "servicedCategory" using primary key columns */
  servicedCategory_by_pk?: Maybe<ServicedCategory>;
  /** fetch data from the table: "servicedProduct" */
  servicedProduct: Array<ServicedProduct>;
  /** fetch aggregated fields from the table: "servicedProduct" */
  servicedProduct_aggregate: ServicedProduct_Aggregate;
  /** fetch data from the table: "servicedProduct" using primary key columns */
  servicedProduct_by_pk?: Maybe<ServicedProduct>;
  /** fetch data from the table: "user" */
  user: Array<User>;
  /** An array relationship */
  userServiceTypes: Array<UserServiceTypes>;
  /** An aggregate relationship */
  userServiceTypes_aggregate: UserServiceTypes_Aggregate;
  /** fetch data from the table: "userServiceTypes" using primary key columns */
  userServiceTypes_by_pk?: Maybe<UserServiceTypes>;
  /** An array relationship */
  userZipCodes: Array<UserZipCodes>;
  /** An aggregate relationship */
  userZipCodes_aggregate: UserZipCodes_Aggregate;
  /** fetch data from the table: "userZipCodes" using primary key columns */
  userZipCodes_by_pk?: Maybe<UserZipCodes>;
  /** fetch aggregated fields from the table: "user" */
  user_aggregate: User_Aggregate;
  /** fetch data from the table: "user" using primary key columns */
  user_by_pk?: Maybe<User>;
  /** fetch data from the table: "zipCodes" */
  zipCodes: Array<ZipCodes>;
  /** fetch aggregated fields from the table: "zipCodes" */
  zipCodes_aggregate: ZipCodes_Aggregate;
  /** fetch data from the table: "zipCodes" using primary key columns */
  zipCodes_by_pk?: Maybe<ZipCodes>;
};


export type Subscription_RootAvailabilityArgs = {
  distinct_on?: InputMaybe<Array<Availability_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Availability_Order_By>>;
  where?: InputMaybe<Availability_Bool_Exp>;
};


export type Subscription_RootAvailabilityZipCodesArgs = {
  distinct_on?: InputMaybe<Array<AvailabilityZipCodes_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<AvailabilityZipCodes_Order_By>>;
  where?: InputMaybe<AvailabilityZipCodes_Bool_Exp>;
};


export type Subscription_RootAvailabilityZipCodes_AggregateArgs = {
  distinct_on?: InputMaybe<Array<AvailabilityZipCodes_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<AvailabilityZipCodes_Order_By>>;
  where?: InputMaybe<AvailabilityZipCodes_Bool_Exp>;
};


export type Subscription_RootAvailabilityZipCodes_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Subscription_RootAvailability_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Availability_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Availability_Order_By>>;
  where?: InputMaybe<Availability_Bool_Exp>;
};


export type Subscription_RootAvailability_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Subscription_RootCommentArgs = {
  distinct_on?: InputMaybe<Array<Comment_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Comment_Order_By>>;
  where?: InputMaybe<Comment_Bool_Exp>;
};


export type Subscription_RootCommentEntityArgs = {
  distinct_on?: InputMaybe<Array<CommentEntity_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<CommentEntity_Order_By>>;
  where?: InputMaybe<CommentEntity_Bool_Exp>;
};


export type Subscription_RootCommentEntity_AggregateArgs = {
  distinct_on?: InputMaybe<Array<CommentEntity_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<CommentEntity_Order_By>>;
  where?: InputMaybe<CommentEntity_Bool_Exp>;
};


export type Subscription_RootCommentEntity_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Subscription_RootComment_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Comment_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Comment_Order_By>>;
  where?: InputMaybe<Comment_Bool_Exp>;
};


export type Subscription_RootComment_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Subscription_RootEshopArgs = {
  distinct_on?: InputMaybe<Array<Eshop_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Eshop_Order_By>>;
  where?: InputMaybe<Eshop_Bool_Exp>;
};


export type Subscription_RootEshop_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Eshop_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Eshop_Order_By>>;
  where?: InputMaybe<Eshop_Bool_Exp>;
};


export type Subscription_RootEshop_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Subscription_RootInstallationArgs = {
  distinct_on?: InputMaybe<Array<Installation_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Installation_Order_By>>;
  where?: InputMaybe<Installation_Bool_Exp>;
};


export type Subscription_RootInstallationSessionArgs = {
  distinct_on?: InputMaybe<Array<InstallationSession_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<InstallationSession_Order_By>>;
  where?: InputMaybe<InstallationSession_Bool_Exp>;
};


export type Subscription_RootInstallationSession_AggregateArgs = {
  distinct_on?: InputMaybe<Array<InstallationSession_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<InstallationSession_Order_By>>;
  where?: InputMaybe<InstallationSession_Bool_Exp>;
};


export type Subscription_RootInstallationSession_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Subscription_RootInstallationStateArgs = {
  distinct_on?: InputMaybe<Array<InstallationState_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<InstallationState_Order_By>>;
  where?: InputMaybe<InstallationState_Bool_Exp>;
};


export type Subscription_RootInstallationState_AggregateArgs = {
  distinct_on?: InputMaybe<Array<InstallationState_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<InstallationState_Order_By>>;
  where?: InputMaybe<InstallationState_Bool_Exp>;
};


export type Subscription_RootInstallationState_By_PkArgs = {
  value: Scalars['String'];
};


export type Subscription_RootInstallation_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Installation_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Installation_Order_By>>;
  where?: InputMaybe<Installation_Bool_Exp>;
};


export type Subscription_RootInstallation_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Subscription_RootPinSafeCharsArgs = {
  distinct_on?: InputMaybe<Array<PinSafeChars_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<PinSafeChars_Order_By>>;
  where?: InputMaybe<PinSafeChars_Bool_Exp>;
};


export type Subscription_RootPinSafeChars_AggregateArgs = {
  distinct_on?: InputMaybe<Array<PinSafeChars_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<PinSafeChars_Order_By>>;
  where?: InputMaybe<PinSafeChars_Bool_Exp>;
};


export type Subscription_RootPinSafeChars_By_PkArgs = {
  character: Scalars['bpchar'];
};


export type Subscription_RootRoleArgs = {
  distinct_on?: InputMaybe<Array<Role_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Role_Order_By>>;
  where?: InputMaybe<Role_Bool_Exp>;
};


export type Subscription_RootRole_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Role_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Role_Order_By>>;
  where?: InputMaybe<Role_Bool_Exp>;
};


export type Subscription_RootRole_By_PkArgs = {
  value: Scalars['String'];
};


export type Subscription_RootServicePriceArgs = {
  distinct_on?: InputMaybe<Array<ServicePrice_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<ServicePrice_Order_By>>;
  where?: InputMaybe<ServicePrice_Bool_Exp>;
};


export type Subscription_RootServicePrice_AggregateArgs = {
  distinct_on?: InputMaybe<Array<ServicePrice_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<ServicePrice_Order_By>>;
  where?: InputMaybe<ServicePrice_Bool_Exp>;
};


export type Subscription_RootServicePrice_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Subscription_RootServiceTypeArgs = {
  distinct_on?: InputMaybe<Array<ServiceType_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<ServiceType_Order_By>>;
  where?: InputMaybe<ServiceType_Bool_Exp>;
};


export type Subscription_RootServiceType_AggregateArgs = {
  distinct_on?: InputMaybe<Array<ServiceType_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<ServiceType_Order_By>>;
  where?: InputMaybe<ServiceType_Bool_Exp>;
};


export type Subscription_RootServiceType_By_PkArgs = {
  value: Scalars['String'];
};


export type Subscription_RootServicedCategoryArgs = {
  distinct_on?: InputMaybe<Array<ServicedCategory_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<ServicedCategory_Order_By>>;
  where?: InputMaybe<ServicedCategory_Bool_Exp>;
};


export type Subscription_RootServicedCategory_AggregateArgs = {
  distinct_on?: InputMaybe<Array<ServicedCategory_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<ServicedCategory_Order_By>>;
  where?: InputMaybe<ServicedCategory_Bool_Exp>;
};


export type Subscription_RootServicedCategory_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Subscription_RootServicedProductArgs = {
  distinct_on?: InputMaybe<Array<ServicedProduct_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<ServicedProduct_Order_By>>;
  where?: InputMaybe<ServicedProduct_Bool_Exp>;
};


export type Subscription_RootServicedProduct_AggregateArgs = {
  distinct_on?: InputMaybe<Array<ServicedProduct_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<ServicedProduct_Order_By>>;
  where?: InputMaybe<ServicedProduct_Bool_Exp>;
};


export type Subscription_RootServicedProduct_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Subscription_RootUserArgs = {
  distinct_on?: InputMaybe<Array<User_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<User_Order_By>>;
  where?: InputMaybe<User_Bool_Exp>;
};


export type Subscription_RootUserServiceTypesArgs = {
  distinct_on?: InputMaybe<Array<UserServiceTypes_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<UserServiceTypes_Order_By>>;
  where?: InputMaybe<UserServiceTypes_Bool_Exp>;
};


export type Subscription_RootUserServiceTypes_AggregateArgs = {
  distinct_on?: InputMaybe<Array<UserServiceTypes_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<UserServiceTypes_Order_By>>;
  where?: InputMaybe<UserServiceTypes_Bool_Exp>;
};


export type Subscription_RootUserServiceTypes_By_PkArgs = {
  serviceTypeValue: ServiceType_Enum;
  userId: Scalars['uuid'];
};


export type Subscription_RootUserZipCodesArgs = {
  distinct_on?: InputMaybe<Array<UserZipCodes_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<UserZipCodes_Order_By>>;
  where?: InputMaybe<UserZipCodes_Bool_Exp>;
};


export type Subscription_RootUserZipCodes_AggregateArgs = {
  distinct_on?: InputMaybe<Array<UserZipCodes_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<UserZipCodes_Order_By>>;
  where?: InputMaybe<UserZipCodes_Bool_Exp>;
};


export type Subscription_RootUserZipCodes_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Subscription_RootUser_AggregateArgs = {
  distinct_on?: InputMaybe<Array<User_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<User_Order_By>>;
  where?: InputMaybe<User_Bool_Exp>;
};


export type Subscription_RootUser_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Subscription_RootZipCodesArgs = {
  distinct_on?: InputMaybe<Array<ZipCodes_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<ZipCodes_Order_By>>;
  where?: InputMaybe<ZipCodes_Bool_Exp>;
};


export type Subscription_RootZipCodes_AggregateArgs = {
  distinct_on?: InputMaybe<Array<ZipCodes_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<ZipCodes_Order_By>>;
  where?: InputMaybe<ZipCodes_Bool_Exp>;
};


export type Subscription_RootZipCodes_By_PkArgs = {
  id: Scalars['String'];
};

/** Boolean expression to compare columns of type "timestamptz". All fields are combined with logical 'AND'. */
export type Timestamptz_Comparison_Exp = {
  _eq?: InputMaybe<Scalars['timestamptz']>;
  _gt?: InputMaybe<Scalars['timestamptz']>;
  _gte?: InputMaybe<Scalars['timestamptz']>;
  _in?: InputMaybe<Array<Scalars['timestamptz']>>;
  _is_null?: InputMaybe<Scalars['Boolean']>;
  _lt?: InputMaybe<Scalars['timestamptz']>;
  _lte?: InputMaybe<Scalars['timestamptz']>;
  _neq?: InputMaybe<Scalars['timestamptz']>;
  _nin?: InputMaybe<Array<Scalars['timestamptz']>>;
};

/** columns and relationships of "user" */
export type User = {
  __typename?: 'user';
  address?: Maybe<Scalars['String']>;
  /** An array relationship */
  availabilities: Array<Availability>;
  /** An aggregate relationship */
  availabilities_aggregate: Availability_Aggregate;
  comment?: Maybe<Scalars['String']>;
  /** An array relationship */
  comments: Array<Comment>;
  /** An aggregate relationship */
  comments_aggregate: Comment_Aggregate;
  companyName?: Maybe<Scalars['String']>;
  createdAt: Scalars['timestamptz'];
  email: Scalars['String'];
  firebaseId?: Maybe<Scalars['String']>;
  firstName: Scalars['String'];
  id: Scalars['uuid'];
  /** ICO - Identifikacni cislo osoby */
  in?: Maybe<Scalars['String']>;
  /** An array relationship */
  installations: Array<Installation>;
  /** An aggregate relationship */
  installations_aggregate: Installation_Aggregate;
  isActive: Scalars['Boolean'];
  lastName: Scalars['String'];
  login: Scalars['String'];
  password?: Maybe<Scalars['String']>;
  phone?: Maybe<Scalars['String']>;
  /** An object relationship */
  role: Role;
  roleValue: Role_Enum;
  /** DIC - Danove identifikacni cislo */
  tin?: Maybe<Scalars['String']>;
  updatedAt: Scalars['timestamptz'];
  /** An array relationship */
  userServiceTypes: Array<UserServiceTypes>;
  /** An aggregate relationship */
  userServiceTypes_aggregate: UserServiceTypes_Aggregate;
  /** An array relationship */
  userZipCodes: Array<UserZipCodes>;
  /** An aggregate relationship */
  userZipCodes_aggregate: UserZipCodes_Aggregate;
};


/** columns and relationships of "user" */
export type UserAvailabilitiesArgs = {
  distinct_on?: InputMaybe<Array<Availability_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Availability_Order_By>>;
  where?: InputMaybe<Availability_Bool_Exp>;
};


/** columns and relationships of "user" */
export type UserAvailabilities_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Availability_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Availability_Order_By>>;
  where?: InputMaybe<Availability_Bool_Exp>;
};


/** columns and relationships of "user" */
export type UserCommentsArgs = {
  distinct_on?: InputMaybe<Array<Comment_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Comment_Order_By>>;
  where?: InputMaybe<Comment_Bool_Exp>;
};


/** columns and relationships of "user" */
export type UserComments_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Comment_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Comment_Order_By>>;
  where?: InputMaybe<Comment_Bool_Exp>;
};


/** columns and relationships of "user" */
export type UserInstallationsArgs = {
  distinct_on?: InputMaybe<Array<Installation_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Installation_Order_By>>;
  where?: InputMaybe<Installation_Bool_Exp>;
};


/** columns and relationships of "user" */
export type UserInstallations_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Installation_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Installation_Order_By>>;
  where?: InputMaybe<Installation_Bool_Exp>;
};


/** columns and relationships of "user" */
export type UserUserServiceTypesArgs = {
  distinct_on?: InputMaybe<Array<UserServiceTypes_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<UserServiceTypes_Order_By>>;
  where?: InputMaybe<UserServiceTypes_Bool_Exp>;
};


/** columns and relationships of "user" */
export type UserUserServiceTypes_AggregateArgs = {
  distinct_on?: InputMaybe<Array<UserServiceTypes_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<UserServiceTypes_Order_By>>;
  where?: InputMaybe<UserServiceTypes_Bool_Exp>;
};


/** columns and relationships of "user" */
export type UserUserZipCodesArgs = {
  distinct_on?: InputMaybe<Array<UserZipCodes_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<UserZipCodes_Order_By>>;
  where?: InputMaybe<UserZipCodes_Bool_Exp>;
};


/** columns and relationships of "user" */
export type UserUserZipCodes_AggregateArgs = {
  distinct_on?: InputMaybe<Array<UserZipCodes_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<UserZipCodes_Order_By>>;
  where?: InputMaybe<UserZipCodes_Bool_Exp>;
};

/** columns and relationships of "userServiceTypes" */
export type UserServiceTypes = {
  __typename?: 'userServiceTypes';
  /** An object relationship */
  serviceType: ServiceType;
  serviceTypeValue: ServiceType_Enum;
  /** An object relationship */
  user: User;
  userId: Scalars['uuid'];
};

/** aggregated selection of "userServiceTypes" */
export type UserServiceTypes_Aggregate = {
  __typename?: 'userServiceTypes_aggregate';
  aggregate?: Maybe<UserServiceTypes_Aggregate_Fields>;
  nodes: Array<UserServiceTypes>;
};

/** aggregate fields of "userServiceTypes" */
export type UserServiceTypes_Aggregate_Fields = {
  __typename?: 'userServiceTypes_aggregate_fields';
  count: Scalars['Int'];
  max?: Maybe<UserServiceTypes_Max_Fields>;
  min?: Maybe<UserServiceTypes_Min_Fields>;
};


/** aggregate fields of "userServiceTypes" */
export type UserServiceTypes_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<UserServiceTypes_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "userServiceTypes" */
export type UserServiceTypes_Aggregate_Order_By = {
  count?: InputMaybe<Order_By>;
  max?: InputMaybe<UserServiceTypes_Max_Order_By>;
  min?: InputMaybe<UserServiceTypes_Min_Order_By>;
};

/** input type for inserting array relation for remote table "userServiceTypes" */
export type UserServiceTypes_Arr_Rel_Insert_Input = {
  data: Array<UserServiceTypes_Insert_Input>;
  /** upsert condition */
  on_conflict?: InputMaybe<UserServiceTypes_On_Conflict>;
};

/** Boolean expression to filter rows from the table "userServiceTypes". All fields are combined with a logical 'AND'. */
export type UserServiceTypes_Bool_Exp = {
  _and?: InputMaybe<Array<UserServiceTypes_Bool_Exp>>;
  _not?: InputMaybe<UserServiceTypes_Bool_Exp>;
  _or?: InputMaybe<Array<UserServiceTypes_Bool_Exp>>;
  serviceType?: InputMaybe<ServiceType_Bool_Exp>;
  serviceTypeValue?: InputMaybe<ServiceType_Enum_Comparison_Exp>;
  user?: InputMaybe<User_Bool_Exp>;
  userId?: InputMaybe<Uuid_Comparison_Exp>;
};

/** unique or primary key constraints on table "userServiceTypes" */
export enum UserServiceTypes_Constraint {
  /** unique or primary key constraint */
  UserServiceTypesPkey = 'userServiceTypes_pkey'
}

/** input type for inserting data into table "userServiceTypes" */
export type UserServiceTypes_Insert_Input = {
  serviceType?: InputMaybe<ServiceType_Obj_Rel_Insert_Input>;
  serviceTypeValue?: InputMaybe<ServiceType_Enum>;
  user?: InputMaybe<User_Obj_Rel_Insert_Input>;
  userId?: InputMaybe<Scalars['uuid']>;
};

/** aggregate max on columns */
export type UserServiceTypes_Max_Fields = {
  __typename?: 'userServiceTypes_max_fields';
  userId?: Maybe<Scalars['uuid']>;
};

/** order by max() on columns of table "userServiceTypes" */
export type UserServiceTypes_Max_Order_By = {
  userId?: InputMaybe<Order_By>;
};

/** aggregate min on columns */
export type UserServiceTypes_Min_Fields = {
  __typename?: 'userServiceTypes_min_fields';
  userId?: Maybe<Scalars['uuid']>;
};

/** order by min() on columns of table "userServiceTypes" */
export type UserServiceTypes_Min_Order_By = {
  userId?: InputMaybe<Order_By>;
};

/** response of any mutation on the table "userServiceTypes" */
export type UserServiceTypes_Mutation_Response = {
  __typename?: 'userServiceTypes_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int'];
  /** data from the rows affected by the mutation */
  returning: Array<UserServiceTypes>;
};

/** on_conflict condition type for table "userServiceTypes" */
export type UserServiceTypes_On_Conflict = {
  constraint: UserServiceTypes_Constraint;
  update_columns?: Array<UserServiceTypes_Update_Column>;
  where?: InputMaybe<UserServiceTypes_Bool_Exp>;
};

/** Ordering options when selecting data from "userServiceTypes". */
export type UserServiceTypes_Order_By = {
  serviceType?: InputMaybe<ServiceType_Order_By>;
  serviceTypeValue?: InputMaybe<Order_By>;
  user?: InputMaybe<User_Order_By>;
  userId?: InputMaybe<Order_By>;
};

/** primary key columns input for table: userServiceTypes */
export type UserServiceTypes_Pk_Columns_Input = {
  serviceTypeValue: ServiceType_Enum;
  userId: Scalars['uuid'];
};

/** select columns of table "userServiceTypes" */
export enum UserServiceTypes_Select_Column {
  /** column name */
  ServiceTypeValue = 'serviceTypeValue',
  /** column name */
  UserId = 'userId'
}

/** input type for updating data in table "userServiceTypes" */
export type UserServiceTypes_Set_Input = {
  serviceTypeValue?: InputMaybe<ServiceType_Enum>;
  userId?: InputMaybe<Scalars['uuid']>;
};

/** update columns of table "userServiceTypes" */
export enum UserServiceTypes_Update_Column {
  /** column name */
  ServiceTypeValue = 'serviceTypeValue',
  /** column name */
  UserId = 'userId'
}

/** connection table between user and zipcodes */
export type UserZipCodes = {
  __typename?: 'userZipCodes';
  id: Scalars['uuid'];
  /** An object relationship */
  user: User;
  userId: Scalars['uuid'];
  /** An object relationship */
  zipCode: ZipCodes;
  zipCodeId: Scalars['String'];
};

/** aggregated selection of "userZipCodes" */
export type UserZipCodes_Aggregate = {
  __typename?: 'userZipCodes_aggregate';
  aggregate?: Maybe<UserZipCodes_Aggregate_Fields>;
  nodes: Array<UserZipCodes>;
};

/** aggregate fields of "userZipCodes" */
export type UserZipCodes_Aggregate_Fields = {
  __typename?: 'userZipCodes_aggregate_fields';
  count: Scalars['Int'];
  max?: Maybe<UserZipCodes_Max_Fields>;
  min?: Maybe<UserZipCodes_Min_Fields>;
};


/** aggregate fields of "userZipCodes" */
export type UserZipCodes_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<UserZipCodes_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "userZipCodes" */
export type UserZipCodes_Aggregate_Order_By = {
  count?: InputMaybe<Order_By>;
  max?: InputMaybe<UserZipCodes_Max_Order_By>;
  min?: InputMaybe<UserZipCodes_Min_Order_By>;
};

/** input type for inserting array relation for remote table "userZipCodes" */
export type UserZipCodes_Arr_Rel_Insert_Input = {
  data: Array<UserZipCodes_Insert_Input>;
  /** upsert condition */
  on_conflict?: InputMaybe<UserZipCodes_On_Conflict>;
};

/** Boolean expression to filter rows from the table "userZipCodes". All fields are combined with a logical 'AND'. */
export type UserZipCodes_Bool_Exp = {
  _and?: InputMaybe<Array<UserZipCodes_Bool_Exp>>;
  _not?: InputMaybe<UserZipCodes_Bool_Exp>;
  _or?: InputMaybe<Array<UserZipCodes_Bool_Exp>>;
  id?: InputMaybe<Uuid_Comparison_Exp>;
  user?: InputMaybe<User_Bool_Exp>;
  userId?: InputMaybe<Uuid_Comparison_Exp>;
  zipCode?: InputMaybe<ZipCodes_Bool_Exp>;
  zipCodeId?: InputMaybe<String_Comparison_Exp>;
};

/** unique or primary key constraints on table "userZipCodes" */
export enum UserZipCodes_Constraint {
  /** unique or primary key constraint */
  UserZipCodesPkey = 'userZipCodes_pkey',
  /** unique or primary key constraint */
  UserZipCodesZipCodeIdUserIdKey = 'userZipCodes_zipCodeId_userId_key'
}

/** input type for inserting data into table "userZipCodes" */
export type UserZipCodes_Insert_Input = {
  id?: InputMaybe<Scalars['uuid']>;
  user?: InputMaybe<User_Obj_Rel_Insert_Input>;
  userId?: InputMaybe<Scalars['uuid']>;
  zipCode?: InputMaybe<ZipCodes_Obj_Rel_Insert_Input>;
  zipCodeId?: InputMaybe<Scalars['String']>;
};

/** aggregate max on columns */
export type UserZipCodes_Max_Fields = {
  __typename?: 'userZipCodes_max_fields';
  id?: Maybe<Scalars['uuid']>;
  userId?: Maybe<Scalars['uuid']>;
  zipCodeId?: Maybe<Scalars['String']>;
};

/** order by max() on columns of table "userZipCodes" */
export type UserZipCodes_Max_Order_By = {
  id?: InputMaybe<Order_By>;
  userId?: InputMaybe<Order_By>;
  zipCodeId?: InputMaybe<Order_By>;
};

/** aggregate min on columns */
export type UserZipCodes_Min_Fields = {
  __typename?: 'userZipCodes_min_fields';
  id?: Maybe<Scalars['uuid']>;
  userId?: Maybe<Scalars['uuid']>;
  zipCodeId?: Maybe<Scalars['String']>;
};

/** order by min() on columns of table "userZipCodes" */
export type UserZipCodes_Min_Order_By = {
  id?: InputMaybe<Order_By>;
  userId?: InputMaybe<Order_By>;
  zipCodeId?: InputMaybe<Order_By>;
};

/** response of any mutation on the table "userZipCodes" */
export type UserZipCodes_Mutation_Response = {
  __typename?: 'userZipCodes_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int'];
  /** data from the rows affected by the mutation */
  returning: Array<UserZipCodes>;
};

/** on_conflict condition type for table "userZipCodes" */
export type UserZipCodes_On_Conflict = {
  constraint: UserZipCodes_Constraint;
  update_columns?: Array<UserZipCodes_Update_Column>;
  where?: InputMaybe<UserZipCodes_Bool_Exp>;
};

/** Ordering options when selecting data from "userZipCodes". */
export type UserZipCodes_Order_By = {
  id?: InputMaybe<Order_By>;
  user?: InputMaybe<User_Order_By>;
  userId?: InputMaybe<Order_By>;
  zipCode?: InputMaybe<ZipCodes_Order_By>;
  zipCodeId?: InputMaybe<Order_By>;
};

/** primary key columns input for table: userZipCodes */
export type UserZipCodes_Pk_Columns_Input = {
  id: Scalars['uuid'];
};

/** select columns of table "userZipCodes" */
export enum UserZipCodes_Select_Column {
  /** column name */
  Id = 'id',
  /** column name */
  UserId = 'userId',
  /** column name */
  ZipCodeId = 'zipCodeId'
}

/** input type for updating data in table "userZipCodes" */
export type UserZipCodes_Set_Input = {
  id?: InputMaybe<Scalars['uuid']>;
  userId?: InputMaybe<Scalars['uuid']>;
  zipCodeId?: InputMaybe<Scalars['String']>;
};

/** update columns of table "userZipCodes" */
export enum UserZipCodes_Update_Column {
  /** column name */
  Id = 'id',
  /** column name */
  UserId = 'userId',
  /** column name */
  ZipCodeId = 'zipCodeId'
}

/** aggregated selection of "user" */
export type User_Aggregate = {
  __typename?: 'user_aggregate';
  aggregate?: Maybe<User_Aggregate_Fields>;
  nodes: Array<User>;
};

/** aggregate fields of "user" */
export type User_Aggregate_Fields = {
  __typename?: 'user_aggregate_fields';
  count: Scalars['Int'];
  max?: Maybe<User_Max_Fields>;
  min?: Maybe<User_Min_Fields>;
};


/** aggregate fields of "user" */
export type User_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<User_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "user" */
export type User_Aggregate_Order_By = {
  count?: InputMaybe<Order_By>;
  max?: InputMaybe<User_Max_Order_By>;
  min?: InputMaybe<User_Min_Order_By>;
};

/** input type for inserting array relation for remote table "user" */
export type User_Arr_Rel_Insert_Input = {
  data: Array<User_Insert_Input>;
  /** upsert condition */
  on_conflict?: InputMaybe<User_On_Conflict>;
};

/** Boolean expression to filter rows from the table "user". All fields are combined with a logical 'AND'. */
export type User_Bool_Exp = {
  _and?: InputMaybe<Array<User_Bool_Exp>>;
  _not?: InputMaybe<User_Bool_Exp>;
  _or?: InputMaybe<Array<User_Bool_Exp>>;
  address?: InputMaybe<String_Comparison_Exp>;
  availabilities?: InputMaybe<Availability_Bool_Exp>;
  comment?: InputMaybe<String_Comparison_Exp>;
  comments?: InputMaybe<Comment_Bool_Exp>;
  companyName?: InputMaybe<String_Comparison_Exp>;
  createdAt?: InputMaybe<Timestamptz_Comparison_Exp>;
  email?: InputMaybe<String_Comparison_Exp>;
  firebaseId?: InputMaybe<String_Comparison_Exp>;
  firstName?: InputMaybe<String_Comparison_Exp>;
  id?: InputMaybe<Uuid_Comparison_Exp>;
  in?: InputMaybe<String_Comparison_Exp>;
  installations?: InputMaybe<Installation_Bool_Exp>;
  isActive?: InputMaybe<Boolean_Comparison_Exp>;
  lastName?: InputMaybe<String_Comparison_Exp>;
  login?: InputMaybe<String_Comparison_Exp>;
  password?: InputMaybe<String_Comparison_Exp>;
  phone?: InputMaybe<String_Comparison_Exp>;
  role?: InputMaybe<Role_Bool_Exp>;
  roleValue?: InputMaybe<Role_Enum_Comparison_Exp>;
  tin?: InputMaybe<String_Comparison_Exp>;
  updatedAt?: InputMaybe<Timestamptz_Comparison_Exp>;
  userServiceTypes?: InputMaybe<UserServiceTypes_Bool_Exp>;
  userZipCodes?: InputMaybe<UserZipCodes_Bool_Exp>;
};

/** unique or primary key constraints on table "user" */
export enum User_Constraint {
  /** unique or primary key constraint */
  UserLoginKey = 'user_login_key',
  /** unique or primary key constraint */
  UserPkey = 'user_pkey'
}

/** input type for inserting data into table "user" */
export type User_Insert_Input = {
  address?: InputMaybe<Scalars['String']>;
  availabilities?: InputMaybe<Availability_Arr_Rel_Insert_Input>;
  comment?: InputMaybe<Scalars['String']>;
  comments?: InputMaybe<Comment_Arr_Rel_Insert_Input>;
  companyName?: InputMaybe<Scalars['String']>;
  createdAt?: InputMaybe<Scalars['timestamptz']>;
  email?: InputMaybe<Scalars['String']>;
  firebaseId?: InputMaybe<Scalars['String']>;
  firstName?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['uuid']>;
  /** ICO - Identifikacni cislo osoby */
  in?: InputMaybe<Scalars['String']>;
  installations?: InputMaybe<Installation_Arr_Rel_Insert_Input>;
  isActive?: InputMaybe<Scalars['Boolean']>;
  lastName?: InputMaybe<Scalars['String']>;
  login?: InputMaybe<Scalars['String']>;
  password?: InputMaybe<Scalars['String']>;
  phone?: InputMaybe<Scalars['String']>;
  role?: InputMaybe<Role_Obj_Rel_Insert_Input>;
  roleValue?: InputMaybe<Role_Enum>;
  /** DIC - Danove identifikacni cislo */
  tin?: InputMaybe<Scalars['String']>;
  updatedAt?: InputMaybe<Scalars['timestamptz']>;
  userServiceTypes?: InputMaybe<UserServiceTypes_Arr_Rel_Insert_Input>;
  userZipCodes?: InputMaybe<UserZipCodes_Arr_Rel_Insert_Input>;
};

/** aggregate max on columns */
export type User_Max_Fields = {
  __typename?: 'user_max_fields';
  address?: Maybe<Scalars['String']>;
  comment?: Maybe<Scalars['String']>;
  companyName?: Maybe<Scalars['String']>;
  createdAt?: Maybe<Scalars['timestamptz']>;
  email?: Maybe<Scalars['String']>;
  firebaseId?: Maybe<Scalars['String']>;
  firstName?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['uuid']>;
  /** ICO - Identifikacni cislo osoby */
  in?: Maybe<Scalars['String']>;
  lastName?: Maybe<Scalars['String']>;
  login?: Maybe<Scalars['String']>;
  password?: Maybe<Scalars['String']>;
  phone?: Maybe<Scalars['String']>;
  /** DIC - Danove identifikacni cislo */
  tin?: Maybe<Scalars['String']>;
  updatedAt?: Maybe<Scalars['timestamptz']>;
};

/** order by max() on columns of table "user" */
export type User_Max_Order_By = {
  address?: InputMaybe<Order_By>;
  comment?: InputMaybe<Order_By>;
  companyName?: InputMaybe<Order_By>;
  createdAt?: InputMaybe<Order_By>;
  email?: InputMaybe<Order_By>;
  firebaseId?: InputMaybe<Order_By>;
  firstName?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  /** ICO - Identifikacni cislo osoby */
  in?: InputMaybe<Order_By>;
  lastName?: InputMaybe<Order_By>;
  login?: InputMaybe<Order_By>;
  password?: InputMaybe<Order_By>;
  phone?: InputMaybe<Order_By>;
  /** DIC - Danove identifikacni cislo */
  tin?: InputMaybe<Order_By>;
  updatedAt?: InputMaybe<Order_By>;
};

/** aggregate min on columns */
export type User_Min_Fields = {
  __typename?: 'user_min_fields';
  address?: Maybe<Scalars['String']>;
  comment?: Maybe<Scalars['String']>;
  companyName?: Maybe<Scalars['String']>;
  createdAt?: Maybe<Scalars['timestamptz']>;
  email?: Maybe<Scalars['String']>;
  firebaseId?: Maybe<Scalars['String']>;
  firstName?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['uuid']>;
  /** ICO - Identifikacni cislo osoby */
  in?: Maybe<Scalars['String']>;
  lastName?: Maybe<Scalars['String']>;
  login?: Maybe<Scalars['String']>;
  password?: Maybe<Scalars['String']>;
  phone?: Maybe<Scalars['String']>;
  /** DIC - Danove identifikacni cislo */
  tin?: Maybe<Scalars['String']>;
  updatedAt?: Maybe<Scalars['timestamptz']>;
};

/** order by min() on columns of table "user" */
export type User_Min_Order_By = {
  address?: InputMaybe<Order_By>;
  comment?: InputMaybe<Order_By>;
  companyName?: InputMaybe<Order_By>;
  createdAt?: InputMaybe<Order_By>;
  email?: InputMaybe<Order_By>;
  firebaseId?: InputMaybe<Order_By>;
  firstName?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  /** ICO - Identifikacni cislo osoby */
  in?: InputMaybe<Order_By>;
  lastName?: InputMaybe<Order_By>;
  login?: InputMaybe<Order_By>;
  password?: InputMaybe<Order_By>;
  phone?: InputMaybe<Order_By>;
  /** DIC - Danove identifikacni cislo */
  tin?: InputMaybe<Order_By>;
  updatedAt?: InputMaybe<Order_By>;
};

/** response of any mutation on the table "user" */
export type User_Mutation_Response = {
  __typename?: 'user_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int'];
  /** data from the rows affected by the mutation */
  returning: Array<User>;
};

/** input type for inserting object relation for remote table "user" */
export type User_Obj_Rel_Insert_Input = {
  data: User_Insert_Input;
  /** upsert condition */
  on_conflict?: InputMaybe<User_On_Conflict>;
};

/** on_conflict condition type for table "user" */
export type User_On_Conflict = {
  constraint: User_Constraint;
  update_columns?: Array<User_Update_Column>;
  where?: InputMaybe<User_Bool_Exp>;
};

/** Ordering options when selecting data from "user". */
export type User_Order_By = {
  address?: InputMaybe<Order_By>;
  availabilities_aggregate?: InputMaybe<Availability_Aggregate_Order_By>;
  comment?: InputMaybe<Order_By>;
  comments_aggregate?: InputMaybe<Comment_Aggregate_Order_By>;
  companyName?: InputMaybe<Order_By>;
  createdAt?: InputMaybe<Order_By>;
  email?: InputMaybe<Order_By>;
  firebaseId?: InputMaybe<Order_By>;
  firstName?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  in?: InputMaybe<Order_By>;
  installations_aggregate?: InputMaybe<Installation_Aggregate_Order_By>;
  isActive?: InputMaybe<Order_By>;
  lastName?: InputMaybe<Order_By>;
  login?: InputMaybe<Order_By>;
  password?: InputMaybe<Order_By>;
  phone?: InputMaybe<Order_By>;
  role?: InputMaybe<Role_Order_By>;
  roleValue?: InputMaybe<Order_By>;
  tin?: InputMaybe<Order_By>;
  updatedAt?: InputMaybe<Order_By>;
  userServiceTypes_aggregate?: InputMaybe<UserServiceTypes_Aggregate_Order_By>;
  userZipCodes_aggregate?: InputMaybe<UserZipCodes_Aggregate_Order_By>;
};

/** primary key columns input for table: user */
export type User_Pk_Columns_Input = {
  id: Scalars['uuid'];
};

/** select columns of table "user" */
export enum User_Select_Column {
  /** column name */
  Address = 'address',
  /** column name */
  Comment = 'comment',
  /** column name */
  CompanyName = 'companyName',
  /** column name */
  CreatedAt = 'createdAt',
  /** column name */
  Email = 'email',
  /** column name */
  FirebaseId = 'firebaseId',
  /** column name */
  FirstName = 'firstName',
  /** column name */
  Id = 'id',
  /** column name */
  In = 'in',
  /** column name */
  IsActive = 'isActive',
  /** column name */
  LastName = 'lastName',
  /** column name */
  Login = 'login',
  /** column name */
  Password = 'password',
  /** column name */
  Phone = 'phone',
  /** column name */
  RoleValue = 'roleValue',
  /** column name */
  Tin = 'tin',
  /** column name */
  UpdatedAt = 'updatedAt'
}

/** input type for updating data in table "user" */
export type User_Set_Input = {
  address?: InputMaybe<Scalars['String']>;
  comment?: InputMaybe<Scalars['String']>;
  companyName?: InputMaybe<Scalars['String']>;
  createdAt?: InputMaybe<Scalars['timestamptz']>;
  email?: InputMaybe<Scalars['String']>;
  firebaseId?: InputMaybe<Scalars['String']>;
  firstName?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['uuid']>;
  /** ICO - Identifikacni cislo osoby */
  in?: InputMaybe<Scalars['String']>;
  isActive?: InputMaybe<Scalars['Boolean']>;
  lastName?: InputMaybe<Scalars['String']>;
  login?: InputMaybe<Scalars['String']>;
  password?: InputMaybe<Scalars['String']>;
  phone?: InputMaybe<Scalars['String']>;
  roleValue?: InputMaybe<Role_Enum>;
  /** DIC - Danove identifikacni cislo */
  tin?: InputMaybe<Scalars['String']>;
  updatedAt?: InputMaybe<Scalars['timestamptz']>;
};

/** update columns of table "user" */
export enum User_Update_Column {
  /** column name */
  Address = 'address',
  /** column name */
  Comment = 'comment',
  /** column name */
  CompanyName = 'companyName',
  /** column name */
  CreatedAt = 'createdAt',
  /** column name */
  Email = 'email',
  /** column name */
  FirebaseId = 'firebaseId',
  /** column name */
  FirstName = 'firstName',
  /** column name */
  Id = 'id',
  /** column name */
  In = 'in',
  /** column name */
  IsActive = 'isActive',
  /** column name */
  LastName = 'lastName',
  /** column name */
  Login = 'login',
  /** column name */
  Password = 'password',
  /** column name */
  Phone = 'phone',
  /** column name */
  RoleValue = 'roleValue',
  /** column name */
  Tin = 'tin',
  /** column name */
  UpdatedAt = 'updatedAt'
}

/** Boolean expression to compare columns of type "uuid". All fields are combined with logical 'AND'. */
export type Uuid_Comparison_Exp = {
  _eq?: InputMaybe<Scalars['uuid']>;
  _gt?: InputMaybe<Scalars['uuid']>;
  _gte?: InputMaybe<Scalars['uuid']>;
  _in?: InputMaybe<Array<Scalars['uuid']>>;
  _is_null?: InputMaybe<Scalars['Boolean']>;
  _lt?: InputMaybe<Scalars['uuid']>;
  _lte?: InputMaybe<Scalars['uuid']>;
  _neq?: InputMaybe<Scalars['uuid']>;
  _nin?: InputMaybe<Array<Scalars['uuid']>>;
};

/** columns and relationships of "zipCodes" */
export type ZipCodes = {
  __typename?: 'zipCodes';
  /** An array relationship */
  availabilityZipCodes: Array<AvailabilityZipCodes>;
  /** An aggregate relationship */
  availabilityZipCodes_aggregate: AvailabilityZipCodes_Aggregate;
  city: Scalars['String'];
  district: Scalars['String'];
  id: Scalars['String'];
  region: Scalars['String'];
  /** An array relationship */
  userZipCodes: Array<UserZipCodes>;
  /** An aggregate relationship */
  userZipCodes_aggregate: UserZipCodes_Aggregate;
};


/** columns and relationships of "zipCodes" */
export type ZipCodesAvailabilityZipCodesArgs = {
  distinct_on?: InputMaybe<Array<AvailabilityZipCodes_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<AvailabilityZipCodes_Order_By>>;
  where?: InputMaybe<AvailabilityZipCodes_Bool_Exp>;
};


/** columns and relationships of "zipCodes" */
export type ZipCodesAvailabilityZipCodes_AggregateArgs = {
  distinct_on?: InputMaybe<Array<AvailabilityZipCodes_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<AvailabilityZipCodes_Order_By>>;
  where?: InputMaybe<AvailabilityZipCodes_Bool_Exp>;
};


/** columns and relationships of "zipCodes" */
export type ZipCodesUserZipCodesArgs = {
  distinct_on?: InputMaybe<Array<UserZipCodes_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<UserZipCodes_Order_By>>;
  where?: InputMaybe<UserZipCodes_Bool_Exp>;
};


/** columns and relationships of "zipCodes" */
export type ZipCodesUserZipCodes_AggregateArgs = {
  distinct_on?: InputMaybe<Array<UserZipCodes_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<UserZipCodes_Order_By>>;
  where?: InputMaybe<UserZipCodes_Bool_Exp>;
};

/** aggregated selection of "zipCodes" */
export type ZipCodes_Aggregate = {
  __typename?: 'zipCodes_aggregate';
  aggregate?: Maybe<ZipCodes_Aggregate_Fields>;
  nodes: Array<ZipCodes>;
};

/** aggregate fields of "zipCodes" */
export type ZipCodes_Aggregate_Fields = {
  __typename?: 'zipCodes_aggregate_fields';
  count: Scalars['Int'];
  max?: Maybe<ZipCodes_Max_Fields>;
  min?: Maybe<ZipCodes_Min_Fields>;
};


/** aggregate fields of "zipCodes" */
export type ZipCodes_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<ZipCodes_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']>;
};

/** Boolean expression to filter rows from the table "zipCodes". All fields are combined with a logical 'AND'. */
export type ZipCodes_Bool_Exp = {
  _and?: InputMaybe<Array<ZipCodes_Bool_Exp>>;
  _not?: InputMaybe<ZipCodes_Bool_Exp>;
  _or?: InputMaybe<Array<ZipCodes_Bool_Exp>>;
  availabilityZipCodes?: InputMaybe<AvailabilityZipCodes_Bool_Exp>;
  city?: InputMaybe<String_Comparison_Exp>;
  district?: InputMaybe<String_Comparison_Exp>;
  id?: InputMaybe<String_Comparison_Exp>;
  region?: InputMaybe<String_Comparison_Exp>;
  userZipCodes?: InputMaybe<UserZipCodes_Bool_Exp>;
};

/** unique or primary key constraints on table "zipCodes" */
export enum ZipCodes_Constraint {
  /** unique or primary key constraint */
  ZipCodesPkey = 'zipCodes_pkey'
}

/** input type for inserting data into table "zipCodes" */
export type ZipCodes_Insert_Input = {
  availabilityZipCodes?: InputMaybe<AvailabilityZipCodes_Arr_Rel_Insert_Input>;
  city?: InputMaybe<Scalars['String']>;
  district?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['String']>;
  region?: InputMaybe<Scalars['String']>;
  userZipCodes?: InputMaybe<UserZipCodes_Arr_Rel_Insert_Input>;
};

/** aggregate max on columns */
export type ZipCodes_Max_Fields = {
  __typename?: 'zipCodes_max_fields';
  city?: Maybe<Scalars['String']>;
  district?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['String']>;
  region?: Maybe<Scalars['String']>;
};

/** aggregate min on columns */
export type ZipCodes_Min_Fields = {
  __typename?: 'zipCodes_min_fields';
  city?: Maybe<Scalars['String']>;
  district?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['String']>;
  region?: Maybe<Scalars['String']>;
};

/** response of any mutation on the table "zipCodes" */
export type ZipCodes_Mutation_Response = {
  __typename?: 'zipCodes_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int'];
  /** data from the rows affected by the mutation */
  returning: Array<ZipCodes>;
};

/** input type for inserting object relation for remote table "zipCodes" */
export type ZipCodes_Obj_Rel_Insert_Input = {
  data: ZipCodes_Insert_Input;
  /** upsert condition */
  on_conflict?: InputMaybe<ZipCodes_On_Conflict>;
};

/** on_conflict condition type for table "zipCodes" */
export type ZipCodes_On_Conflict = {
  constraint: ZipCodes_Constraint;
  update_columns?: Array<ZipCodes_Update_Column>;
  where?: InputMaybe<ZipCodes_Bool_Exp>;
};

/** Ordering options when selecting data from "zipCodes". */
export type ZipCodes_Order_By = {
  availabilityZipCodes_aggregate?: InputMaybe<AvailabilityZipCodes_Aggregate_Order_By>;
  city?: InputMaybe<Order_By>;
  district?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  region?: InputMaybe<Order_By>;
  userZipCodes_aggregate?: InputMaybe<UserZipCodes_Aggregate_Order_By>;
};

/** primary key columns input for table: zipCodes */
export type ZipCodes_Pk_Columns_Input = {
  id: Scalars['String'];
};

/** select columns of table "zipCodes" */
export enum ZipCodes_Select_Column {
  /** column name */
  City = 'city',
  /** column name */
  District = 'district',
  /** column name */
  Id = 'id',
  /** column name */
  Region = 'region'
}

/** input type for updating data in table "zipCodes" */
export type ZipCodes_Set_Input = {
  city?: InputMaybe<Scalars['String']>;
  district?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['String']>;
  region?: InputMaybe<Scalars['String']>;
};

/** update columns of table "zipCodes" */
export enum ZipCodes_Update_Column {
  /** column name */
  City = 'city',
  /** column name */
  District = 'district',
  /** column name */
  Id = 'id',
  /** column name */
  Region = 'region'
}

export type UpdateAvailabilityMutationVariables = Exact<{
  installId: Scalars['uuid'];
  availabilityIds: Array<Scalars['uuid']> | Scalars['uuid'];
}>;


export type UpdateAvailabilityMutation = { __typename?: 'mutation_root', resetIds?: { __typename?: 'availability_mutation_response', returning: Array<{ __typename?: 'availability', id: any }> } | null, update_availability?: { __typename?: 'availability_mutation_response', returning: Array<{ __typename?: 'availability', id: any }> } | null };

export type CreateAvailabilityMutationVariables = Exact<{
  availabilityInput: Array<Availability_Insert_Input> | Availability_Insert_Input;
}>;


export type CreateAvailabilityMutation = { __typename?: 'mutation_root', insert_availability?: { __typename?: 'availability_mutation_response', returning: Array<{ __typename?: 'availability', id: any }> } | null };

export type CreateCommentMutationVariables = Exact<{
  commentInput: Comment_Insert_Input;
}>;


export type CreateCommentMutation = { __typename?: 'mutation_root', comment?: { __typename?: 'comment', id: any } | null };

export type UpdateCommentMutationVariables = Exact<{
  id?: InputMaybe<Scalars['uuid']>;
  commentInput?: InputMaybe<Comment_Set_Input>;
}>;


export type UpdateCommentMutation = { __typename?: 'mutation_root', comment?: { __typename?: 'comment_mutation_response', returning: Array<{ __typename?: 'comment', id: any }> } | null };

export type DeleteCommentMutationVariables = Exact<{
  id: Scalars['uuid'];
}>;


export type DeleteCommentMutation = { __typename?: 'mutation_root', comment?: { __typename?: 'comment', id: any } | null };

export type UpdateInstallationMutationVariables = Exact<{
  id?: InputMaybe<Scalars['uuid']>;
  installationInput?: InputMaybe<Installation_Set_Input>;
}>;


export type UpdateInstallationMutation = { __typename?: 'mutation_root', installation?: { __typename?: 'installation_mutation_response', returning: Array<{ __typename?: 'installation', id: any }> } | null };

export type CreateInstallationMutationVariables = Exact<{
  installationInput: Installation_Insert_Input;
}>;


export type CreateInstallationMutation = { __typename?: 'mutation_root', installation?: { __typename?: 'installation', id: any } | null };

export type DeleteInstallationMutationVariables = Exact<{
  id: Scalars['uuid'];
}>;


export type DeleteInstallationMutation = { __typename?: 'mutation_root', installation?: { __typename?: 'installation', id: any } | null };

export type CreateUserMutationVariables = Exact<{
  userInput: User_Insert_Input;
}>;


export type CreateUserMutation = { __typename?: 'mutation_root', user?: { __typename?: 'user', id: any } | null };

export type UpdateUserMutationVariables = Exact<{
  id: Scalars['uuid'];
  userInput?: InputMaybe<User_Set_Input>;
  userServiceTypesInput: Array<UserServiceTypes_Insert_Input> | UserServiceTypes_Insert_Input;
}>;


export type UpdateUserMutation = { __typename?: 'mutation_root', delete_userServiceTypes?: { __typename?: 'userServiceTypes_mutation_response', returning: Array<{ __typename?: 'userServiceTypes', serviceTypeValue: ServiceType_Enum }> } | null, insert_userServiceTypes?: { __typename?: 'userServiceTypes_mutation_response', returning: Array<{ __typename?: 'userServiceTypes', serviceTypeValue: ServiceType_Enum }> } | null, user?: { __typename?: 'user', id: any } | null };

export type DeleteUserMutationVariables = Exact<{
  id: Scalars['uuid'];
}>;


export type DeleteUserMutation = { __typename?: 'mutation_root', user?: { __typename?: 'user', id: any } | null };

export type UpdateUserByPkMutationVariables = Exact<{
  id: Scalars['uuid'];
  userInput: User_Set_Input;
}>;


export type UpdateUserByPkMutation = { __typename?: 'mutation_root', update_user_by_pk?: { __typename?: 'user', firebaseId?: string | null, id: any } | null };

export type CreateUserZipCodesMutationVariables = Exact<{
  userId: Scalars['uuid'];
  zipCodeInput: Array<UserZipCodes_Insert_Input> | UserZipCodes_Insert_Input;
}>;


export type CreateUserZipCodesMutation = { __typename?: 'mutation_root', delete_userZipCodes?: { __typename?: 'userZipCodes_mutation_response', returning: Array<{ __typename?: 'userZipCodes', id: any }> } | null, insert_userZipCodes?: { __typename?: 'userZipCodes_mutation_response', returning: Array<{ __typename?: 'userZipCodes', id: any }> } | null };

export type AvailabilityFragment = { __typename?: 'availability', availableFrom?: any | null, availableTo?: any | null, id: any, installationId?: any | null, user: { __typename?: 'user', firstName: string, lastName: string, email: string, address?: string | null, comment?: string | null, companyName?: string | null, id: any, in?: string | null, phone?: string | null } };

export type GetAvailabilityQueryVariables = Exact<{
  id: Scalars['uuid'];
}>;


export type GetAvailabilityQuery = { __typename?: 'query_root', availability?: { __typename?: 'availability', availableFrom?: any | null, availableTo?: any | null, id: any, installationId?: any | null, user: { __typename?: 'user', firstName: string, lastName: string, email: string, address?: string | null, comment?: string | null, companyName?: string | null, id: any, in?: string | null, phone?: string | null } } | null };

export type GetInstallationAvailabilityQueryVariables = Exact<{
  where?: InputMaybe<Availability_Bool_Exp>;
}>;


export type GetInstallationAvailabilityQuery = { __typename?: 'query_root', availability: Array<{ __typename?: 'availability', availableFrom?: any | null, availableTo?: any | null, id: any, installationId?: any | null, user: { __typename?: 'user', firstName: string, lastName: string, email: string, address?: string | null, comment?: string | null, companyName?: string | null, id: any, in?: string | null, phone?: string | null } }> };

export type GetServiceTypeQueryVariables = Exact<{
  where?: InputMaybe<ServiceType_Bool_Exp>;
}>;


export type GetServiceTypeQuery = { __typename?: 'query_root', serviceType: Array<{ __typename?: 'serviceType', value: string, description: string }> };

export type CommentFragment = { __typename?: 'comment', id: any, text: string, createdAt?: any | null, updatedAt?: any | null, userId?: any | null, user?: { __typename?: 'user', lastName: string, firstName: string, email: string } | null };

export type GetCommentsQueryVariables = Exact<{
  installationId: Scalars['uuid'];
}>;


export type GetCommentsQuery = { __typename?: 'query_root', comment: Array<{ __typename?: 'comment', text: string, updatedAt?: any | null, createdAt?: any | null, userId?: any | null, user?: { __typename?: 'user', lastName: string, firstName: string, email: string, id: any } | null }> };

export type EshopSelectFragment = { __typename?: 'eshop', id: any, name: string, servicedCategories: Array<{ __typename?: 'servicedCategory', categoryName: string, eshopCategoryId?: string | null, serviceType: { __typename?: 'serviceType', value: string, description: string } }> };

export type GetEshopSelectQueryVariables = Exact<{ [key: string]: never; }>;


export type GetEshopSelectQuery = { __typename?: 'query_root', eshops: Array<{ __typename?: 'eshop', id: any, name: string, servicedCategories: Array<{ __typename?: 'servicedCategory', categoryName: string, eshopCategoryId?: string | null, serviceType: { __typename?: 'serviceType', value: string, description: string } }> }> };

export type InstallationFragment = { __typename?: 'installation', id: any, customer: any, deliveries: any, eshopId: any, items: any, orderId: string, pinCode?: string | null, shippingAddress: any, servicemanId?: any | null, updatedAt?: any | null, createdAt?: any | null, stateValue: InstallationState_Enum, selectedDateTime?: any | null, date?: any | null, eshop: { __typename?: 'eshop', id: any, name: string }, availabilities: Array<{ __typename?: 'availability', availableFrom?: any | null, availableTo?: any | null, userId: any, id: any, installationId?: any | null }>, installationComments: Array<{ __typename?: 'commentEntity', comment: { __typename?: 'comment', text: string, updatedAt?: any | null, id: any, createdAt?: any | null, user?: { __typename?: 'user', firstName: string, email: string, lastName: string } | null } }> };

export type GetInstallationQueryVariables = Exact<{
  id: Scalars['uuid'];
}>;


export type GetInstallationQuery = { __typename?: 'query_root', installation?: { __typename?: 'installation', id: any, customer: any, deliveries: any, eshopId: any, items: any, orderId: string, pinCode?: string | null, shippingAddress: any, servicemanId?: any | null, updatedAt?: any | null, createdAt?: any | null, stateValue: InstallationState_Enum, selectedDateTime?: any | null, date?: any | null, eshop: { __typename?: 'eshop', name: string, id: any }, availabilities: Array<{ __typename?: 'availability', availableFrom?: any | null, availableTo?: any | null, userId: any, id: any, installationId?: any | null }>, installationComments: Array<{ __typename?: 'commentEntity', comment: { __typename?: 'comment', text: string, updatedAt?: any | null, id: any, createdAt?: any | null, user?: { __typename?: 'user', firstName: string, email: string, lastName: string } | null } }> } | null };

export type FilterInstallationsQueryVariables = Exact<{
  orderBy?: InputMaybe<Array<Installation_Order_By> | Installation_Order_By>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<Installation_Bool_Exp>;
}>;


export type FilterInstallationsQuery = { __typename?: 'query_root', installations: Array<{ __typename?: 'installation', id: any, customer: any, deliveries: any, eshopId: any, items: any, orderId: string, pinCode?: string | null, shippingAddress: any, servicemanId?: any | null, updatedAt?: any | null, createdAt?: any | null, stateValue: InstallationState_Enum, selectedDateTime?: any | null, date?: any | null, installationState: { __typename?: 'installationState', value: string, title: string }, user?: { __typename?: 'user', id: any } | null, eshop: { __typename?: 'eshop', name: string, id: any }, availabilities: Array<{ __typename?: 'availability', availableFrom?: any | null, availableTo?: any | null, userId: any, id: any, installationId?: any | null }>, installationComments: Array<{ __typename?: 'commentEntity', comment: { __typename?: 'comment', text: string, updatedAt?: any | null, id: any, createdAt?: any | null, user?: { __typename?: 'user', firstName: string, email: string, lastName: string } | null } }> }>, installation_aggregate: { __typename?: 'installation_aggregate', aggregate?: { __typename?: 'installation_aggregate_fields', count: number } | null } };

export type InstallationsQueryVariables = Exact<{
  where?: InputMaybe<Installation_Bool_Exp>;
  orderBy?: InputMaybe<Array<Installation_Order_By> | Installation_Order_By>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
}>;


export type InstallationsQuery = { __typename?: 'query_root', installations: Array<{ __typename?: 'installation', id: any, customer: any, deliveries: any, eshopId: any, items: any, orderId: string, pinCode?: string | null, shippingAddress: any, servicemanId?: any | null, updatedAt?: any | null, createdAt?: any | null, stateValue: InstallationState_Enum, selectedDateTime?: any | null, date?: any | null, eshop: { __typename?: 'eshop', name: string, id: any }, installationState: { __typename?: 'installationState', value: string, title: string }, availabilities: Array<{ __typename?: 'availability', availableFrom?: any | null, availableTo?: any | null, userId: any, id: any, installationId?: any | null }>, installationComments: Array<{ __typename?: 'commentEntity', comment: { __typename?: 'comment', text: string, updatedAt?: any | null, id: any, createdAt?: any | null, user?: { __typename?: 'user', firstName: string, email: string, lastName: string } | null } }> }>, installationsAggregate: { __typename?: 'installation_aggregate', aggregate?: { __typename?: 'installation_aggregate_fields', count: number } | null } };

export type InstallationStateSelectFragment = { __typename?: 'installationState', value: string, title: string };

export type GetInstallationStateSelectQueryVariables = Exact<{ [key: string]: never; }>;


export type GetInstallationStateSelectQuery = { __typename?: 'query_root', installationStates: Array<{ __typename?: 'installationState', value: string, title: string }> };

export type RoleSelectFragment = { __typename?: 'role', value: string, description: string };

export type GetRoleSelectQueryVariables = Exact<{ [key: string]: never; }>;


export type GetRoleSelectQuery = { __typename?: 'query_root', roles: Array<{ __typename?: 'role', value: string, description: string }> };

export type ServiceTypeSelectFragment = { __typename?: 'serviceType', value: string, description: string };

export type GetServiceTypeSelectQueryVariables = Exact<{ [key: string]: never; }>;


export type GetServiceTypeSelectQuery = { __typename?: 'query_root', serviceTypes: Array<{ __typename?: 'serviceType', value: string, description: string }> };

export type UserSelectFragment = { __typename?: 'user', id: any, firstName: string, lastName: string, companyName?: string | null };

export type GetUserSelectQueryVariables = Exact<{ [key: string]: never; }>;


export type GetUserSelectQuery = { __typename?: 'query_root', users: Array<{ __typename?: 'user', id: any, firstName: string, lastName: string, companyName?: string | null }> };

export type GetUserCompanySelectQueryVariables = Exact<{ [key: string]: never; }>;


export type GetUserCompanySelectQuery = { __typename?: 'query_root', user: Array<{ __typename?: 'user', companyName?: string | null }> };

export type UserFragment = { __typename?: 'user', id: any, firebaseId?: string | null, firstName: string, lastName: string, login: string, password?: string | null, address?: string | null, email: string, companyName?: string | null, phone?: string | null, comment?: string | null, roleValue: Role_Enum, isActive: boolean, in?: string | null, tin?: string | null, userServiceTypes: Array<{ __typename?: 'userServiceTypes', userId: any, serviceTypeValue: ServiceType_Enum, serviceType: { __typename?: 'serviceType', value: string, description: string } }>, role: { __typename?: 'role', value: string, description: string }, availabilities: Array<{ __typename?: 'availability', availableFrom?: any | null, availableTo?: any | null }>, userZipCodes: Array<{ __typename?: 'userZipCodes', zipCodeId: string }> };

export type GetUserQueryVariables = Exact<{
  id: Scalars['uuid'];
}>;


export type GetUserQuery = { __typename?: 'query_root', user?: { __typename?: 'user', id: any, firebaseId?: string | null, firstName: string, lastName: string, login: string, password?: string | null, address?: string | null, email: string, companyName?: string | null, phone?: string | null, comment?: string | null, roleValue: Role_Enum, isActive: boolean, in?: string | null, tin?: string | null, userServiceTypes: Array<{ __typename?: 'userServiceTypes', userId: any, serviceTypeValue: ServiceType_Enum, serviceType: { __typename?: 'serviceType', value: string, description: string } }>, role: { __typename?: 'role', value: string, description: string }, availabilities: Array<{ __typename?: 'availability', availableFrom?: any | null, availableTo?: any | null }>, userZipCodes: Array<{ __typename?: 'userZipCodes', zipCodeId: string }> } | null };

export type UsersQueryVariables = Exact<{
  where?: InputMaybe<User_Bool_Exp>;
  orderBy?: InputMaybe<Array<User_Order_By> | User_Order_By>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
}>;


export type UsersQuery = { __typename?: 'query_root', users: Array<{ __typename?: 'user', id: any, firebaseId?: string | null, firstName: string, lastName: string, login: string, password?: string | null, address?: string | null, email: string, companyName?: string | null, phone?: string | null, comment?: string | null, roleValue: Role_Enum, isActive: boolean, in?: string | null, tin?: string | null, userServiceTypes: Array<{ __typename?: 'userServiceTypes', userId: any, serviceTypeValue: ServiceType_Enum, serviceType: { __typename?: 'serviceType', value: string, description: string } }>, role: { __typename?: 'role', value: string, description: string }, availabilities: Array<{ __typename?: 'availability', availableFrom?: any | null, availableTo?: any | null }>, userZipCodes: Array<{ __typename?: 'userZipCodes', zipCodeId: string }> }>, usersAgregate: { __typename?: 'user_aggregate', aggregate?: { __typename?: 'user_aggregate_fields', count: number } | null } };

export type ZipcodeFragment = { __typename?: 'zipCodes', city: string, district: string, id: string, region: string };

export type GetAllZipCodesQueryVariables = Exact<{ [key: string]: never; }>;


export type GetAllZipCodesQuery = { __typename?: 'query_root', zipCodes: Array<{ __typename?: 'zipCodes', city: string, district: string, id: string, region: string }> };

export type GetAssignedZipCodesQueryVariables = Exact<{
  userId: Scalars['uuid'];
}>;


export type GetAssignedZipCodesQuery = { __typename?: 'query_root', zipCodes: Array<{ __typename?: 'zipCodes', city: string, district: string, id: string, region: string }> };

export type SubscribeCommentSubscriptionVariables = Exact<{
  installationId: Scalars['uuid'];
}>;


export type SubscribeCommentSubscription = { __typename?: 'subscription_root', comment: Array<{ __typename?: 'comment', id: any, text: string, createdAt?: any | null, updatedAt?: any | null, user?: { __typename?: 'user', firstName: string, lastName: string } | null }> };

export const AvailabilityFragmentDoc = gql`
    fragment Availability on availability {
  availableFrom
  availableTo
  user {
    firstName
    lastName
    email
    address
    comment
    companyName
    id
    in
    phone
  }
  id
  installationId
}
    `;
export const CommentFragmentDoc = gql`
    fragment Comment on comment {
  id
  text
  createdAt
  updatedAt
  userId
  user {
    lastName
    firstName
    email
  }
}
    `;
export const EshopSelectFragmentDoc = gql`
    fragment EshopSelect on eshop {
  id
  name
  servicedCategories {
    categoryName
    eshopCategoryId
    serviceType {
      value
      description
    }
  }
}
    `;
export const InstallationFragmentDoc = gql`
    fragment Installation on installation {
  id
  customer
  deliveries
  eshopId
  eshop {
    id
    name
  }
  items
  orderId
  pinCode
  shippingAddress
  servicemanId
  updatedAt
  createdAt
  stateValue
  selectedDateTime
  date
  availabilities {
    availableFrom
    availableTo
    userId
    id
    installationId
  }
  installationComments: commentEntities {
    comment {
      text
      updatedAt
      id
      createdAt
      user {
        firstName
        email
        lastName
      }
    }
  }
}
    `;
export const InstallationStateSelectFragmentDoc = gql`
    fragment InstallationStateSelect on installationState {
  value
  title
}
    `;
export const RoleSelectFragmentDoc = gql`
    fragment RoleSelect on role {
  value
  description
}
    `;
export const ServiceTypeSelectFragmentDoc = gql`
    fragment ServiceTypeSelect on serviceType {
  value
  description
}
    `;
export const UserSelectFragmentDoc = gql`
    fragment UserSelect on user {
  id
  firstName
  lastName
  companyName
}
    `;
export const UserFragmentDoc = gql`
    fragment User on user {
  id
  firebaseId
  firstName
  lastName
  login
  password
  address
  email
  companyName
  email
  phone
  comment
  roleValue
  isActive
  in
  tin
  userServiceTypes {
    userId
    serviceTypeValue
    serviceType {
      value
      description
    }
  }
  role {
    value
    description
  }
  availabilities {
    availableFrom
    availableTo
  }
  userZipCodes {
    zipCodeId
  }
}
    `;
export const ZipcodeFragmentDoc = gql`
    fragment Zipcode on zipCodes {
  city
  district
  id
  region
}
    `;
export const UpdateAvailabilityDocument = gql`
    mutation UpdateAvailability($installId: uuid!, $availabilityIds: [uuid!]!) {
  resetIds: update_availability(
    where: {installationId: {_eq: $installId}}
    _set: {installationId: null}
  ) {
    returning {
      id
    }
  }
  update_availability(
    where: {id: {_in: $availabilityIds}}
    _set: {installationId: $installId}
  ) {
    returning {
      id
    }
  }
}
    `;

export function useUpdateAvailabilityMutation() {
  return Urql.useMutation<UpdateAvailabilityMutation, UpdateAvailabilityMutationVariables>(UpdateAvailabilityDocument);
};
export const CreateAvailabilityDocument = gql`
    mutation CreateAvailability($availabilityInput: [availability_insert_input!]!) {
  insert_availability(objects: $availabilityInput) {
    returning {
      id
    }
  }
}
    `;

export function useCreateAvailabilityMutation() {
  return Urql.useMutation<CreateAvailabilityMutation, CreateAvailabilityMutationVariables>(CreateAvailabilityDocument);
};
export const CreateCommentDocument = gql`
    mutation CreateComment($commentInput: comment_insert_input!) {
  comment: insert_comment_one(object: $commentInput) {
    id
  }
}
    `;

export function useCreateCommentMutation() {
  return Urql.useMutation<CreateCommentMutation, CreateCommentMutationVariables>(CreateCommentDocument);
};
export const UpdateCommentDocument = gql`
    mutation UpdateComment($id: uuid, $commentInput: comment_set_input) {
  comment: update_comment(where: {id: {_eq: $id}}, _set: $commentInput) {
    returning {
      id
    }
  }
}
    `;

export function useUpdateCommentMutation() {
  return Urql.useMutation<UpdateCommentMutation, UpdateCommentMutationVariables>(UpdateCommentDocument);
};
export const DeleteCommentDocument = gql`
    mutation DeleteComment($id: uuid!) {
  comment: delete_comment_by_pk(id: $id) {
    id
  }
}
    `;

export function useDeleteCommentMutation() {
  return Urql.useMutation<DeleteCommentMutation, DeleteCommentMutationVariables>(DeleteCommentDocument);
};
export const UpdateInstallationDocument = gql`
    mutation UpdateInstallation($id: uuid, $installationInput: installation_set_input) {
  installation: update_installation(
    where: {id: {_eq: $id}}
    _set: $installationInput
  ) {
    returning {
      id
    }
  }
}
    `;

export function useUpdateInstallationMutation() {
  return Urql.useMutation<UpdateInstallationMutation, UpdateInstallationMutationVariables>(UpdateInstallationDocument);
};
export const CreateInstallationDocument = gql`
    mutation CreateInstallation($installationInput: installation_insert_input!) {
  installation: insert_installation_one(object: $installationInput) {
    id
  }
}
    `;

export function useCreateInstallationMutation() {
  return Urql.useMutation<CreateInstallationMutation, CreateInstallationMutationVariables>(CreateInstallationDocument);
};
export const DeleteInstallationDocument = gql`
    mutation DeleteInstallation($id: uuid!) {
  installation: delete_installation_by_pk(id: $id) {
    id
  }
}
    `;

export function useDeleteInstallationMutation() {
  return Urql.useMutation<DeleteInstallationMutation, DeleteInstallationMutationVariables>(DeleteInstallationDocument);
};
export const CreateUserDocument = gql`
    mutation CreateUser($userInput: user_insert_input!) {
  user: insert_user_one(object: $userInput) {
    id
  }
}
    `;

export function useCreateUserMutation() {
  return Urql.useMutation<CreateUserMutation, CreateUserMutationVariables>(CreateUserDocument);
};
export const UpdateUserDocument = gql`
    mutation UpdateUser($id: uuid!, $userInput: user_set_input, $userServiceTypesInput: [userServiceTypes_insert_input!]!) {
  delete_userServiceTypes(where: {userId: {_eq: $id}}) {
    returning {
      serviceTypeValue
    }
  }
  insert_userServiceTypes(
    objects: $userServiceTypesInput
    on_conflict: {constraint: userServiceTypes_pkey, update_columns: serviceTypeValue}
  ) {
    returning {
      serviceTypeValue
    }
  }
  user: update_user_by_pk(pk_columns: {id: $id}, _set: $userInput) {
    id
  }
}
    `;

export function useUpdateUserMutation() {
  return Urql.useMutation<UpdateUserMutation, UpdateUserMutationVariables>(UpdateUserDocument);
};
export const DeleteUserDocument = gql`
    mutation DeleteUser($id: uuid!) {
  user: delete_user_by_pk(id: $id) {
    id
  }
}
    `;

export function useDeleteUserMutation() {
  return Urql.useMutation<DeleteUserMutation, DeleteUserMutationVariables>(DeleteUserDocument);
};
export const UpdateUserByPkDocument = gql`
    mutation updateUserByPK($id: uuid!, $userInput: user_set_input!) {
  update_user_by_pk(pk_columns: {id: $id}, _set: $userInput) {
    firebaseId
    id
  }
}
    `;

export function useUpdateUserByPkMutation() {
  return Urql.useMutation<UpdateUserByPkMutation, UpdateUserByPkMutationVariables>(UpdateUserByPkDocument);
};
export const CreateUserZipCodesDocument = gql`
    mutation CreateUserZipCodes($userId: uuid!, $zipCodeInput: [userZipCodes_insert_input!]!) {
  delete_userZipCodes(where: {userId: {_eq: $userId}}) {
    returning {
      id
    }
  }
  insert_userZipCodes(objects: $zipCodeInput) {
    returning {
      id
    }
  }
}
    `;

export function useCreateUserZipCodesMutation() {
  return Urql.useMutation<CreateUserZipCodesMutation, CreateUserZipCodesMutationVariables>(CreateUserZipCodesDocument);
};
export const GetAvailabilityDocument = gql`
    query GetAvailability($id: uuid!) {
  availability: availability_by_pk(id: $id) {
    ...Availability
  }
}
    ${AvailabilityFragmentDoc}`;

export function useGetAvailabilityQuery(options: Omit<Urql.UseQueryArgs<never, GetAvailabilityQueryVariables>, 'query'> = {}) {
  return Urql.useQuery<GetAvailabilityQuery>({ query: GetAvailabilityDocument, ...options });
};
export const GetInstallationAvailabilityDocument = gql`
    query GetInstallationAvailability($where: availability_bool_exp = {}) {
  availability(order_by: {availableFrom: asc}, where: $where) {
    availableFrom
    availableTo
    user {
      firstName
      lastName
      email
      address
      comment
      companyName
      id
      in
      phone
    }
    id
    installationId
  }
}
    `;

export function useGetInstallationAvailabilityQuery(options: Omit<Urql.UseQueryArgs<never, GetInstallationAvailabilityQueryVariables>, 'query'> = {}) {
  return Urql.useQuery<GetInstallationAvailabilityQuery>({ query: GetInstallationAvailabilityDocument, ...options });
};
export const GetServiceTypeDocument = gql`
    query GetServiceType($where: serviceType_bool_exp = {}) {
  serviceType(where: $where) {
    value
    description
  }
}
    `;

export function useGetServiceTypeQuery(options: Omit<Urql.UseQueryArgs<never, GetServiceTypeQueryVariables>, 'query'> = {}) {
  return Urql.useQuery<GetServiceTypeQuery>({ query: GetServiceTypeDocument, ...options });
};
export const GetCommentsDocument = gql`
    query GetComments($installationId: uuid!) {
  comment(where: {commentEntities: {installationId: {_eq: $installationId}}}) {
    text
    user {
      lastName
      firstName
      email
      id
    }
    updatedAt
    createdAt
    userId
  }
}
    `;

export function useGetCommentsQuery(options: Omit<Urql.UseQueryArgs<never, GetCommentsQueryVariables>, 'query'> = {}) {
  return Urql.useQuery<GetCommentsQuery>({ query: GetCommentsDocument, ...options });
};
export const GetEshopSelectDocument = gql`
    query GetEshopSelect {
  eshops: eshop {
    ...EshopSelect
  }
}
    ${EshopSelectFragmentDoc}`;

export function useGetEshopSelectQuery(options: Omit<Urql.UseQueryArgs<never, GetEshopSelectQueryVariables>, 'query'> = {}) {
  return Urql.useQuery<GetEshopSelectQuery>({ query: GetEshopSelectDocument, ...options });
};
export const GetInstallationDocument = gql`
    query GetInstallation($id: uuid!) {
  installation: installation_by_pk(id: $id) {
    ...Installation
    eshop {
      name
    }
  }
}
    ${InstallationFragmentDoc}`;

export function useGetInstallationQuery(options: Omit<Urql.UseQueryArgs<never, GetInstallationQueryVariables>, 'query'> = {}) {
  return Urql.useQuery<GetInstallationQuery>({ query: GetInstallationDocument, ...options });
};
export const FilterInstallationsDocument = gql`
    query FilterInstallations($orderBy: [installation_order_by!], $limit: Int = 10, $offset: Int = 0, $where: installation_bool_exp) {
  installations: installation(
    limit: $limit
    offset: $offset
    order_by: $orderBy
    where: $where
  ) {
    ...Installation
    installationState {
      value
      title
    }
    user {
      id
    }
    eshop {
      name
    }
  }
  installation_aggregate {
    aggregate {
      count
    }
  }
}
    ${InstallationFragmentDoc}`;

export function useFilterInstallationsQuery(options: Omit<Urql.UseQueryArgs<never, FilterInstallationsQueryVariables>, 'query'> = {}) {
  return Urql.useQuery<FilterInstallationsQuery>({ query: FilterInstallationsDocument, ...options });
};
export const InstallationsDocument = gql`
    query Installations($where: installation_bool_exp, $orderBy: [installation_order_by!], $limit: Int = 10, $offset: Int = 0) {
  installations: installation(
    where: $where
    limit: $limit
    offset: $offset
    order_by: $orderBy
  ) {
    ...Installation
    eshop {
      name
    }
    installationState {
      value
      title
    }
  }
  installationsAggregate: installation_aggregate(where: $where) {
    aggregate {
      count
    }
  }
}
    ${InstallationFragmentDoc}`;

export function useInstallationsQuery(options: Omit<Urql.UseQueryArgs<never, InstallationsQueryVariables>, 'query'> = {}) {
  return Urql.useQuery<InstallationsQuery>({ query: InstallationsDocument, ...options });
};
export const GetInstallationStateSelectDocument = gql`
    query GetInstallationStateSelect {
  installationStates: installationState {
    ...InstallationStateSelect
  }
}
    ${InstallationStateSelectFragmentDoc}`;

export function useGetInstallationStateSelectQuery(options: Omit<Urql.UseQueryArgs<never, GetInstallationStateSelectQueryVariables>, 'query'> = {}) {
  return Urql.useQuery<GetInstallationStateSelectQuery>({ query: GetInstallationStateSelectDocument, ...options });
};
export const GetRoleSelectDocument = gql`
    query GetRoleSelect {
  roles: role {
    ...RoleSelect
  }
}
    ${RoleSelectFragmentDoc}`;

export function useGetRoleSelectQuery(options: Omit<Urql.UseQueryArgs<never, GetRoleSelectQueryVariables>, 'query'> = {}) {
  return Urql.useQuery<GetRoleSelectQuery>({ query: GetRoleSelectDocument, ...options });
};
export const GetServiceTypeSelectDocument = gql`
    query GetServiceTypeSelect {
  serviceTypes: serviceType {
    ...ServiceTypeSelect
  }
}
    ${ServiceTypeSelectFragmentDoc}`;

export function useGetServiceTypeSelectQuery(options: Omit<Urql.UseQueryArgs<never, GetServiceTypeSelectQueryVariables>, 'query'> = {}) {
  return Urql.useQuery<GetServiceTypeSelectQuery>({ query: GetServiceTypeSelectDocument, ...options });
};
export const GetUserSelectDocument = gql`
    query GetUserSelect {
  users: user {
    ...UserSelect
  }
}
    ${UserSelectFragmentDoc}`;

export function useGetUserSelectQuery(options: Omit<Urql.UseQueryArgs<never, GetUserSelectQueryVariables>, 'query'> = {}) {
  return Urql.useQuery<GetUserSelectQuery>({ query: GetUserSelectDocument, ...options });
};
export const GetUserCompanySelectDocument = gql`
    query GetUserCompanySelect {
  user(distinct_on: companyName) {
    companyName
  }
}
    `;

export function useGetUserCompanySelectQuery(options: Omit<Urql.UseQueryArgs<never, GetUserCompanySelectQueryVariables>, 'query'> = {}) {
  return Urql.useQuery<GetUserCompanySelectQuery>({ query: GetUserCompanySelectDocument, ...options });
};
export const GetUserDocument = gql`
    query GetUser($id: uuid!) {
  user: user_by_pk(id: $id) {
    ...User
  }
}
    ${UserFragmentDoc}`;

export function useGetUserQuery(options: Omit<Urql.UseQueryArgs<never, GetUserQueryVariables>, 'query'> = {}) {
  return Urql.useQuery<GetUserQuery>({ query: GetUserDocument, ...options });
};
export const UsersDocument = gql`
    query Users($where: user_bool_exp, $orderBy: [user_order_by!], $limit: Int = 10, $offset: Int = 0) {
  users: user(where: $where, order_by: $orderBy, limit: $limit, offset: $offset) {
    ...User
  }
  usersAgregate: user_aggregate(where: $where) {
    aggregate {
      count
    }
  }
}
    ${UserFragmentDoc}`;

export function useUsersQuery(options: Omit<Urql.UseQueryArgs<never, UsersQueryVariables>, 'query'> = {}) {
  return Urql.useQuery<UsersQuery>({ query: UsersDocument, ...options });
};
export const GetAllZipCodesDocument = gql`
    query GetAllZipCodes {
  zipCodes {
    city
    district
    id
    region
  }
}
    `;

export function useGetAllZipCodesQuery(options: Omit<Urql.UseQueryArgs<never, GetAllZipCodesQueryVariables>, 'query'> = {}) {
  return Urql.useQuery<GetAllZipCodesQuery>({ query: GetAllZipCodesDocument, ...options });
};
export const GetAssignedZipCodesDocument = gql`
    query GetAssignedZipCodes($userId: uuid!) {
  zipCodes(where: {userZipCodes: {userId: {_eq: $userId}}}) {
    city
    district
    id
    region
  }
}
    `;

export function useGetAssignedZipCodesQuery(options: Omit<Urql.UseQueryArgs<never, GetAssignedZipCodesQueryVariables>, 'query'> = {}) {
  return Urql.useQuery<GetAssignedZipCodesQuery>({ query: GetAssignedZipCodesDocument, ...options });
};
export const SubscribeCommentDocument = gql`
    subscription subscribeComment($installationId: uuid!) {
  comment(where: {commentEntities: {installationId: {_eq: $installationId}}}) {
    id
    text
    createdAt
    updatedAt
    user {
      firstName
      lastName
    }
  }
}
    `;

export function useSubscribeCommentSubscription<R = SubscribeCommentSubscription>(options: Omit<Urql.UseSubscriptionArgs<never, SubscribeCommentSubscriptionVariables>, 'query'> = {}, handler?: Urql.SubscriptionHandlerArg<SubscribeCommentSubscription, R>) {
  return Urql.useSubscription<SubscribeCommentSubscription, R, SubscribeCommentSubscriptionVariables>({ query: SubscribeCommentDocument, ...options }, handler);
};