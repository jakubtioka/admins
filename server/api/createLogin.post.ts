import { getAuth, UserRecord } from 'firebase-admin/auth'

export default defineEventHandler(async (event) => {
	const body = await readBody(event)

	const adminEntity = {
		email: body.email,
		emailVerified: false,
		password: body.password,
		displayName: `${body.firstName} ${body.lastName}`,
		disabled: false,
	}

	const response: UserRecord = await getAuth().createUser(adminEntity)
		.then((record) => {
			return record
		})
		.catch((error) => {
			return error
		}) 

	return {
		response
	}
})
