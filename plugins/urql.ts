import NodeWebSocket from 'ws'
import { createClient as createWSClient } from 'graphql-ws'
import { Client, defaultExchanges, subscriptionExchange } from '@urql/vue'

export default defineNuxtPlugin(nuxtApp => {
	const { vueApp } = nuxtApp
	const { public: { graphql } } = useRuntimeConfig()

	const wsClient = createWSClient({
		url: `${ graphql.websocket }`,
		webSocketImpl: typeof window === 'undefined' ? NodeWebSocket : WebSocket
	  })

	const client = new Client({
		url: `${ graphql.endpoint }`,
		maskTypename: true,
		fetchOptions: {
			headers: {
				'x-hasura-admin-secret': `${ graphql.secret }`
			}
		},
		exchanges: [
			...defaultExchanges,
			subscriptionExchange({
				forwardSubscription: (operation) => ({
					subscribe: (sink) => ({
						unsubscribe: wsClient.subscribe(operation, sink)
					})
				})
			})
		]
	})

	nuxtApp.provide('urql', client)
	vueApp.provide('$urql', ref(client))
})
