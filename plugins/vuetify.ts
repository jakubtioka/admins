// plugins/vuetify.js
import { createVuetify, ThemeDefinition } from 'vuetify'
import * as components from 'vuetify/components'
import * as directives from 'vuetify/directives'


const adminTheme: ThemeDefinition = {
	colors: {
		primary: '#7B1FA2',
		'primary-lighten-2': '#bd8fd1',
		'primary-lighten-1': '#8835ab',
	}
}


export default defineNuxtPlugin(nuxtApp => {
	const vuetify = createVuetify({
		components,
		directives,
		theme: {
			defaultTheme: 'adminTheme',
			themes: {
				adminTheme,
			}
		}
	})

	nuxtApp.vueApp.use(vuetify)
})
