import { initializeApp, cert, getApps } from 'firebase-admin/app'

export default defineNuxtPlugin((nuxtApp) => {
	if (getApps().length > 0) return

	const { firebase } = useRuntimeConfig()

	initializeApp({ credential: cert(firebase) })
})
