import { defineRule } from 'vee-validate'
import dayjs from 'dayjs'
import customParseFormat from 'dayjs/plugin/customParseFormat'
dayjs.extend(customParseFormat)
export const trimSpaces = (value: string) => value.split(' ').join('')
const { format } = useTranslates()

export default defineNuxtPlugin(nuxtApp => { 
    
	// GLOBAL VALIDACE --> https://vee-validate.logaretm.com/v4/guide/global-validators
	defineRule('notEmptyTextbox', value => {
		if (!value || !value.trim() || !value.length) {
			return 'Pole musí byt vyplněno'
		}
		return true
	})
	defineRule('maxLengthTextbox', (value, [allowedLength]) => {
		return (value ? value.length : 0) <= allowedLength || `Maximalní povolená délka je ${allowedLength} znaků`
	})
	defineRule('validEmail', value => {
		const regex = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i
		if (!regex.test(value)) {
			return 'E-mail musí být validní'
		}
		return true
	})
	defineRule('includesPhoneCode', (value) => {
		return value.startsWith('+420') ? true : 'Telefon musí začínat mezinárodní předvolbou +420'
	})
	defineRule('validPhone', (value, [min, max]) => {
		return !value
		// eslint-disable-next-line max-len
			? true : /^(\+420)?(2[0-9]{2}|3[0-9]{2}|4[0-9]{2}|5[0-9]{2}|72[0-9]|73[0-9]|77[0-9]|60[1-8]|56[0-9]|70[2-5]|79[0-9])[0-9]{3}[0-9]{3}$/.test(trimSpaces(value)) || 'Telefon musí být validní'
	})
	defineRule('validZipcode', value => {
		return  /^\d{3}[ ]?\d{2}$/.test(trimSpaces(value)) || 'Zip kod musí být ve správném tvaru'
	})
	defineRule('validIn', value => {
		return (value ? value.length : 0) == 8 || (value ? value.length : 0) == 0 || 'IČO musí obsahovat 8 číslic'
	})
	defineRule('validTinPrefix', value => {
		return !value ?  true : value.startsWith('CZ') ?  true : 'Daňové identifikační číslo musí začínat CZ'
	})
	defineRule('validTinLength', value => {
		// eslint-disable-next-line max-len
		return !value ?  true : (value.length > 12 || value.length < 10) ? 'Daňové identifikační číslo musí obsahovat 8 až 10 číslic' : true
	})
	defineRule('isValidDateFormat', value => {
		// eslint-disable-next-line max-len
		return !value.trim() ? true : dayjs(value, format.date, true).isValid() ? true : 'Datum musí být ve správném formátu DD/MM/RRRR' 
	})
	defineRule('isValidTimeFormat', value => {
		// eslint-disable-next-line max-len
		return !value.trim() ? true : /^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/.test(trimSpaces(value)) ? true : 'Čas musí být ve správném formátu HH:MM nebo H:MM' 
	})
	defineRule('dateIsBeforeToday', value => {
		// eslint-disable-next-line max-len
		return !value.trim() ? true : dayjs(value, format.date).isBefore(dayjs(), 'day') ? 'Datum nemůže být v minulosti' : true
	})
	defineRule('isBeforeDateFrom', (value, [target]) => {
		if(dayjs(value, format.date).isAfter(dayjs(target, format.date), 'day') || !value.trim() ) {
		  return true	
		}
		return 'Datum Do nemůže být dříve než Datum Od'
	})	
	defineRule('isBeforeTimeFrom', (value, [target]) => {
		if(dayjs(value, ['HH:mm', 'H:mm']).isAfter(dayjs(target, ['HH:mm', 'H:mm'])) || !value.trim() ) {
		  return true	
		}
		return 'Čas Do nemůže být dříve než Čas Od'
	})	
	defineRule('isBeforeNow', (value, [target]) => {
		// Check if the target date is after the current date
		if (dayjs(target, format.date).endOf('day').isAfter(dayjs().endOf('day')) || !value.trim() || 
		!target.trim() )  {
			return true
		}
		  // Check if the value time is before the current time
		return dayjs(value, ['HH:mm', 'H:mm']).isBefore(dayjs()) ? 'Čas Od nemůžou být dříve než je aktuální čas' : true
	})	
})

