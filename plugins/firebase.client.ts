import { initializeApp } from 'firebase/app'

export default defineNuxtPlugin((nuxtApp) => {
	const { initUser } = useFirebaseAuth()
	const { public: { firebase } } = useRuntimeConfig()

	initializeApp(firebase)
	initUser()
})
