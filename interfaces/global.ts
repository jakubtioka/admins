import { AvailabilityFragment } from '@gql/generated'
import { Dayjs } from 'dayjs'

export interface Message {
  text: string
  timeout?: number
}

export interface Address {
  city: string
  street: string
  zipCode: string
  state: string
}

export interface Customer {
  email: string
  firstName: string
  lastName: string
  phone: string
}

export interface AvailableTimeSlot  {
  [key: string]: {
    fromTo: string
    availabilityStart: Dayjs
    availabilityEnd: Dayjs
    availability: Array<AvailabilityFragment>
  }
}
