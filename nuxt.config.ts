import { resolve } from 'path'

// https://v3.nuxtjs.org/api/configuration/nuxt.config
export default {
	css: [
		'vuetify/lib/styles/main.sass',
		'@mdi/font/css/materialdesignicons.min.css',
	],
	build: {
		transpile: [
			'vuetify',
			'@urql/vue',
		],
	},
	alias: {
		'@components': resolve(__dirname, './components'),
		'@interfaces': resolve(__dirname, './interfaces'),
		'@composables': resolve(__dirname, './composables'),
		'@gql': resolve(__dirname, './graphql'),
	},
	typescript: {
		strict: true,
	},
	runtimeConfig: {
		// Values are automatically loaded from .env (or environment variables)
		firebase: {
			privateKey:	'',
			projectId:	'',
			clientEmail:	'',
		},
		public: {
			graphql: {
				endpoint: '',
				websocket: '',
				secret: '',
			},
			firebase: {
				apiKey: '',
				storageBucket: '',
			},
		},
	},
}
